<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<title>Select Report</title>

<style type="text/css">


#sample h3 {text-align:center; 
				font-style: italic; }
#sample h1 {text-align:center;
			font-style: italic;
			color: brown;}

body {
/* background-image: url("pfizerback.png"); */
background-size: cover;
background-image: url("suggestion1.jpg");
}


hr {
width: 58vw;
border: 0; border-top: 2px solid white;
}

p{
    font-size:15px;
    font-weight: bold;
}



.sub-btn {
background: orange;
color: white;
height: 30px;
margin-left:5px;
width: 200px;
text-align:center;
font: bold 15px arial, sans-serif;
border-radius:4px;
cursor: pointer;
text-shadow:none;
}

.sub-btn:Hover
{
background-color: #e5e5e5;
color: black;
}


#button {
    line-height: 15px;
    width: 60px;
    font-size: 12px;
    background-color: white;
    border-radius: 10px;
    margin-top: 0px;
    margin-right: 0px;
    position: fixed;
    top: 0;
    right: 0;
}
</style>


</head>
<body>

<% 
	response.setHeader("Cache-Control", "no-Cache, no-store, must-revalidate");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");

	if(session.getAttribute("username")== null)
	{
		response.sendRedirect("login.jsp");
	}
	%>


<div id="sample">

<form method="post" action="LogOut">
<p> <input type="submit" name="name"  value="Logout" id="button">  </p>
</form>
<h1><u style="color:#e5e5e5"> Jira&Confluence </u></h1>

<!--  <hr>  -->
<br>
<br>
<form action="Controller" method="post">


<center><h3>SELECT A REQUIRED REPORT</h3> </center><br>

<table align="center">
<tr>
<td>
<p> <input type="radio" name="name" value="jira"><font color="black">  Jira Organization Report</font> </p></td>
</tr>
<tr>
<td>
<p> <input type="radio" name="name" value="conf" checked><font color="black" >  Confluence Organization Report</font> </p></td>
</tr>

<tr>
<td>
<p> <input type="radio" name="name" value="confgroup" > <font color="black" >  Confluence User Group Report</font> </p></td>
</tr>

<tr>
<td>
<p> <input type="radio" name="name" value="group"><font color="black" >  Jira User Group Report</font> </p> </td>
</tr>

<tr>
<td>
<p> <input type="radio" name="name" value="JSD"><font color="black" >  Jira Service-Desk Report</font> </p> </td>
</tr>

<tr>
<td>
<p> <input type="radio" name="name" value="jiraAdmin" id="JAdmin"><font color="black" >  Jira Admin Report</font> </p> </td>
</tr>

<tr>
<td>
<p> <input type="radio" name="name" value="confAdmin" id="CAdmin"><font color="black" >  Confluence Admin Report</font> </p> </td>
</tr>


<tr>

<td> <input type="submit" class="sub-btn" id="sbmt" value="SUBMIT" data-toggle="modal" data-target="#myModal" /><br /></td>
</tr>

</table>


</form>

<%-- Popup Modal for Confluence--%>
 
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="disableBtnSubmit()" data-dismiss="modal">&times;</button>
          <p class="modal-title">This process will take some time to generate Report.<p>
        </div>
        <div class="modal-body">
          <h6>Be patience, Report generation is started.<br><br> Please wait on this page until <b>Download </b>button appear. </h6>
                 
        </div>
        <div class="modal-footer">
		  <span class="form-control-static pull-left"><b>Pro tip: </b> Keep this tab open here as it is and continue to work.</span>
          <button type="button" class="btn btn-default" onclick="disableBtnSubmit()" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<%-- Popup Modal for Confluence end here --%>


</div>
</body>

<script type="text/javascript">

//function disableBtnSubmit() {
	
//    document.getElementById("sbmt").disabled = true; 
	//						}


</script>
</html>


