<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html>
<head>
	<title>Login Page</title>
  
   
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="styles.css">
	
	
	<style type="text/css">
	
	/* Made with love by Mutiullah Samim   background-image: url("background.png");
	*/



html,body{
opacity: 5;
background-size:   cover;
background-color: #0093D0;
background-repeat: no-repeat;
height: 100%;
font-family: 'Numans', sans-serif;
}

.container{
height: 100%;
align-content: center;
}

.card{
height: 300px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: white !important;
}


h3{
color: white;
text-align: center;
}



input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;
}

.btn{
color: black;
background-color: #F26649;
width: 300px;
}

.sub-btn:hover{
color: white;
background-color: #0093D0;
}

.sub-btn {
background:  #0093D0;      /* #7DBA00; */
color: white;
height: 30px;
margin-left:5px;
width: 200px;
text-align:center;
font: bold 15px arial, sans-serif;
border-radius:4px;
cursor: pointer;
text-shadow:none;

}
	
	.inputbtn
	{
		width: 200px;
	}
	</style>
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			
			<div class="card-body">
				
			
		<form action="LoginCheck" method="post" onsubmit="return validateForm()" name="loginform">
		<div align="center">
			<img alt="" src="logo1.png" width="180px" height="auto">
		</div>
		<br>
			
					
					<div>
					<center>	<input type="text" placeholder="username" name="username" class="inputbtn"></center>	
					</div>
				
					<div>	
					<center><input type="password" placeholder="password" name="password" class="inputbtn"></center>	
					</div>
						<br>
					<div  align="center">
						<input type="submit" value="Login" class="sub-btn">
					</div>
				</form>
			</div>
			</div>
			</div>
		
		</div>
	

</body>

<script type="text/javascript">

function validateForm() {
	  var x = document.forms["loginform"]["username"].value;
	  var y = document.forms["loginform"]["password"].value;
	  if (x == "") {
	    alert("User Name must be filled out");
	    return false;
	  }
	  
	  if(y == "")
		  {
		  alert("Password must be filled out");
		  return false;
		  }
	}
</script>
</html>