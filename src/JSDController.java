

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Servlet implementation class JSDController
 */
@WebServlet("/JSDController")
public class JSDController extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public JSDController() {
        super();
    }
    
    String login= "bc8c2e2b59c44e8497a279fa85848fb3";
	String password= "a03f507332514bddBf02288759961A1c";

	String authString = login + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	int maxResults = 50;
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	String Researchlogin = "SRVAMR-ATLRPT";
	String Researchpass = "Pfe63478";
	

	String userNameDB = "JIRADB_P_User";
	String passwordDB = "2wsxcde@2019";
	String url = "jdbc:sqlserver://AMRDRMW638:2023;databaseName=JIRADB_P";
	
	
	String filename = "Jira_service-desk_org_report.xlsx";
	String outputFile= "/usr/local/Enhanced/reports/"+filename;
 	//String outputFile = "D:\\Users\\"+filename;

	XSSFWorkbook newWorkbook = new XSSFWorkbook();
	XSSFSheet ServiceDeskSheet = null;
	String PGSTEXT = "JSD-User List";
	
	
	XSSFSheet FULLSHEET= null;
	String FULLTEXT= "All Jira-ServiceDesk-User List";
	// Parent List used to bind list inside method
	ArrayList<ArrayList<String>> parentList = new ArrayList<>();
	
	ArrayList<String> UserName = new ArrayList<>();
	ArrayList<String> DisplayName = new ArrayList<>();
	ArrayList<String> EmailAddress= new ArrayList<>();
	ArrayList<Boolean> ActiveFlag = new ArrayList<>();
	 ArrayList<String> Login = new ArrayList<>(); 
	ArrayList<ArrayList<String>> PGSContainer = new ArrayList<>();
	ArrayList<ArrayList<String>> PGSParent = new ArrayList<>();
	
	ArrayList<ArrayList<Boolean>> PGSFlagContainer = new ArrayList<>();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int totalCount = 0;
		JSDController jsd = new JSDController();
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			totalCount = jsd.getUserCount("jira-servicedesk-users");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Total number of users present in service desk is "+totalCount);
		
		int servicedeskcount = 0;
		
		
		while(totalCount > servicedeskcount)
		{
			try {
				PGSContainer = jsd.Run(servicedeskcount, "jira-servicedesk-users", UserName, DisplayName, EmailAddress, PGSContainer);
				PGSFlagContainer = jsd.getUserFLAG(servicedeskcount, "jira-servicedesk-users", ActiveFlag);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			servicedeskcount = servicedeskcount + 50;	
		}
		
		UserName = PGSContainer.get(0);
		DisplayName = PGSContainer.get(1);
		EmailAddress = PGSContainer.get(2);
		System.out.println(UserName.toString());
    	
    	ActiveFlag = PGSFlagContainer.get(0);
  		System.out.println(ActiveFlag.toString());

 		
  		 ArrayList<ArrayList<String>> callForAll = new ArrayList<>();
		 Connection con =null;
  		 try {
  			con = DriverManager.getConnection(url, userNameDB, passwordDB);

	    	    callForAll = jsd.getLoginFromDB(con, UserName);
	    	} catch (SQLException e2) {
	    		// TODO Auto-generated catch block
	    		e2.printStackTrace();
	    	}
  		 	Login = callForAll.get(0);
  		 
  		System.out.println("Size of username is "+UserName.size());
  		System.out.println("Size of DisplayName is "+DisplayName.size());
  		System.out.println("Size of EmailAddress is "+EmailAddress.size());
  		System.out.println("Size of ActiveFlag is "+ActiveFlag.size());
  		System.out.println("Size of Login is "+Login.size());

  		try {
			jsd.WriteData(FULLSHEET, UserName, DisplayName, EmailAddress, ActiveFlag, Login, FULLTEXT);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
  		
    	
	     	
	  		File file = new File(outputFile);

				if(file.exists())
				{
					//			Desktop desktop = Desktop.getDesktop();
					//			desktop.open(file);
					//			InputStream in = new URL(f).openStream();
					//			Files.copy(in, Paths.get(f));


					response.setContentType("application/vnd.ms-excel");   
					response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
					System.out.println("filename new  "+filename);
					
					BufferedInputStream in=null;
					ServletOutputStream outs=null;
					byte[] buffer = new byte[1024]; 
				
					
					try {
						int g = 0;
						in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
						outs=response.getOutputStream();
						while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
							outs.write(buffer, 0, g);  
						}  
						outs.flush();  

					} catch (IOException ioe) {
						ioe.printStackTrace(System.out);
					}

					System.out.println("Done !!");

					outs.close();
					in.close();
					
				}
				else
				{
					System.out.println("not found");
				}


	     	
	}
	
		
		public int getUserCount(String groupName) throws JSONException, IOException
		{
			String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName; 		
			  
	    	URL url = new URL(api);
	      	HttpURLConnection conn = null;
	    	conn = (HttpURLConnection) url.openConnection();
	    	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	    	String inputLine = "";
	    	BufferedReader in = null;
	    	System.out.println("Authentication process");
	    	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    	StringBuffer response = new StringBuffer();
	    	while((inputLine = in.readLine()) != null)
	    	{
	    		response.append(inputLine);
	    	}
				in.close();
			  
	    	JSONObject json  = new JSONObject(response.toString());
	    	System.out.println("From User Count Method "+json.getInt("total"));
			
	    	int Count= 0;
			try {
				Count = json.getInt("total");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			System.out.println("Total count is "+Count);
			return Count;	
		}
		
		
		public ArrayList<ArrayList<String>> Run(int startAt, String groupName, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<ArrayList<String>> Container) throws IOException, JSONException  {
			
			System.out.println("Start At value si "+startAt);
			String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName+"&startAt="+startAt; 				    	  
			URL url = new URL(api);
		  	TrustAllCertificates.install();
			HttpURLConnection conn = null;
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
			String inputLine = "";
			BufferedReader in = null;
			System.out.println("Authentication process");
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuffer response = new StringBuffer();
			while((inputLine = in.readLine()) != null)
			{
				response.append(inputLine);
			}
			in.close();
				    	  
			JSONObject json  = new JSONObject(response.toString());
			JSONArray innerJsonArray = json.getJSONArray("values");
		     	    	  
			for(int i=0; i<innerJsonArray.length(); i++)
			{ 
		  	one.add(innerJsonArray.getJSONObject(i).getString("name"));
			two.add(innerJsonArray.getJSONObject(i).getString("displayName"));
			three.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
		  	
			}
			
			
			Container.add(one);
			Container.add(two);
			Container.add(three);
			
			return Container;	
		}
		
		
		public ArrayList<ArrayList<Boolean>> getUserFLAG(int startAt, String groupName, ArrayList<Boolean> four) throws IOException, JSONException  {
			
			String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName+"&startAt="+startAt; 				    	  
			URL url = new URL(api);
		  	TrustAllCertificates.install();
		  	HttpURLConnection conn = null;
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
			String inputLine = "";
			BufferedReader in = null;
			System.out.println("Authentication process");
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			StringBuffer response = new StringBuffer();
			while((inputLine = in.readLine()) != null)
			{
				response.append(inputLine);
			}
			in.close();
				    	  
		    JSONObject json  = new JSONObject(response.toString());
		    JSONArray innerJsonArray = json.getJSONArray("values");
		       	    	  
		    for(int i=0; i<innerJsonArray.length(); i++)
		    { 
		    	    		  
		    	four.add(innerJsonArray.getJSONObject(i).getBoolean("active"));    	
		    }   	    	 
		    
			ArrayList<ArrayList<Boolean>> FlagList = new ArrayList<>();
			FlagList.add(four);
			
			return FlagList;
		}
		    


		public ArrayList<ArrayList<String>> getLoginFromDB(Connection con, ArrayList<String> one) throws SQLException 
		{
			ArrayList<String> LoginList = new ArrayList<>();
			
			String query = "'"+one.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			System.out.println(" Inside DB "+one.toString());
			Statement s1 = con.createStatement();

			ResultSet rs = null;
			try {
				rs = s1.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+query.toString()+") GROUP BY u.lower_user_name;");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			while(rs.next()){
				LoginList.add(rs.getString(2));
			}
			
			ArrayList<ArrayList<String>> al = new ArrayList<>();
			al.add(LoginList);

			return al;
		}

		
		public void WriteData(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<Boolean> four, ArrayList<String> five, String PGSTEXT) throws IOException, ParserConfigurationException
	 	{
	 		
	 		newSheet = newWorkbook.createSheet(PGSTEXT);
	 		DocumentBuilder db = dbf.newDocumentBuilder();

	 		String  costCenter1=null, costCenterDescription2 = null, supervisorId3 = null, supervisorName4 = null, departmentLevel1Id5 = null, departmentLevel1Description6= null, departmentLevel2Id7 = null, departmentLevel2Description8 = null;
	 		String departmentLevel3Id9 = null, departmentLevel3Description10 = null, departmentLevel4Id11 = null, departmentLevel4Description12 = null, departmentLevel5Id13 = null, departmentLevel5Description14 = null, departmentLevel6Id15 = null, departmentLevel6Description16 = null, activePfizer17 = null;

	 		System.out.println("Sheet Created");
			String Columns[] = new String[] {"Username", "Full Name", "Email Address", "Cost Center", "Cost Center Description", "Supervisor Id", "Supervisor Name", "Department Level1 Id","Department Level1 Description", "Department Level2 Id", "Department Level2 Description", "Department Level3 Id", "Department Level3 Description", "Department Level4 Id", "Department Level4 Description", "Department Level5 Id", "Department Level5 Description", "Department Level6 Id", "Department Level6 Description", "Active in Pfizer", "Active in Jira", "Last Login"};     
			Font headerFont = newWorkbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 15);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			CellStyle headerCellStyle = newWorkbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
				
			Row headerRow = newSheet.createRow(0);
				
			for(int columnI=0; columnI<Columns.length; columnI++)
			{ 
				Cell cell = headerRow.createCell(columnI);	
				cell.setCellValue(Columns[columnI]);	
				cell.setCellStyle(headerCellStyle); 
				newSheet.autoSizeColumn(columnI);	
			}
	 		String input, url;
			URL urli;
			URLConnection conn; 
			Document doc = null;
			int fork = 1;
			 for(int ntid =0 ; ntid<one.size();ntid++)
			 {   
				input = one.get(ntid).toString();
				url = "http://researchservice.pfizer.com/ResearchServicePeople/person/ntid/";
				url = url.concat(input);
			    
			    System.out.println(ntid+" Running");
			    System.out.println(input);
				Authenticator.setDefault(new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {          
					return new PasswordAuthentication(Researchlogin, Researchpass.toCharArray());
				}
					});
				
				 try {
			    	urli = new URL(url);
			    	conn = urli.openConnection();
			    	conn.setReadTimeout(50000);
			    	conn.setRequestProperty("Accept-Encoding", "gzip");
			    	conn.setRequestProperty("Accept", "application/xml");
			        conn.setRequestProperty("Content-Type", "application/xml");
			        conn.setUseCaches(true);
			        
						try {
							doc = db.parse(conn.getInputStream());
						} catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        
			        activePfizer17 = doc.getElementsByTagName("activeIndicator").item(0).getTextContent();

			        Node n = doc.getElementsByTagName("organization").item(0);
			        NodeList nl = n.getChildNodes();
			        Node an = null;    	
			   	         
			        for (int i=1; i < nl.getLength(); i++) {
			        	an = nl.item(i);
			        	
			            if(Objects.equals("costCenter", an.getNodeName())) 
			            {
			            	
			            	costCenter1 = an.getChildNodes().item(0).getTextContent();
			            	//System.out.println(costCenter1);
			            }
			            
			            if(Objects.equals("costCenterDescription", an.getNodeName())) 
			            {        	
			            	costCenterDescription2 = an.getChildNodes().item(0).getTextContent();			
			            }
			            
			            if(Objects.equals("supervisorId", an.getNodeName())) 
			            {            		
			            	supervisorId3 = an.getChildNodes().item(0).getTextContent(); 					
			            }
			            
			            if(Objects.equals("supervisorName", an.getNodeName())) 
			            {            	
			            	supervisorName4 = an.getChildNodes().item(0).getTextContent();					
			            }
			            
			            if(Objects.equals("departmentLevel1Id", an.getNodeName())) 
			            {			
			            	departmentLevel1Id5 = an.getChildNodes().item(0).getTextContent();				
			            }            
			            
			            if(Objects.equals("departmentLevel1Description", an.getNodeName())) 
			            {	
			            	departmentLevel1Description6 = an.getChildNodes().item(0).getTextContent();		
			            }
			            
			            if(Objects.equals("departmentLevel2Id", an.getNodeName())) 
			            { 
			            	departmentLevel2Id7 = an.getChildNodes().item(0).getTextContent();				
			            }
			            
			            if(Objects.equals("departmentLevel2Description", an.getNodeName())) 
			            {
			            	departmentLevel2Description8 = an.getChildNodes().item(0).getTextContent();		
			            }
			            
			            if(Objects.equals("departmentLevel3Id", an.getNodeName()))
			            {			
			            	departmentLevel3Id9 = an.getChildNodes().item(0).getTextContent();
			            }    
			            
			            if(Objects.equals("departmentLevel3Description", an.getNodeName())) 
			            {
			            	departmentLevel3Description10 = an.getChildNodes().item(0).getTextContent();	
			            }
			            
			            if(Objects.equals("departmentLevel4Id", an.getNodeName()))
			            {            
			            	departmentLevel4Id11 = an.getChildNodes().item(0).getTextContent();				
			            }
			            
			            if(Objects.equals("departmentLevel4Description", an.getNodeName())) 
			            {	
			            	departmentLevel4Description12 = an.getChildNodes().item(0).getTextContent();	
			            }
			            
			            if(Objects.equals("departmentLevel5Id", an.getNodeName())) 
			            {        
			            	departmentLevel5Id13 = an.getChildNodes().item(0).getTextContent();				
			            }

			            if(Objects.equals("departmentLevel5Description", an.getNodeName())) 
			            {
			            	departmentLevel5Description14 = an.getChildNodes().item(0).getTextContent();	
			            }
			            if(Objects.equals("departmentLevel6Id", an.getNodeName())) 
			            {
			            	departmentLevel6Id15 = an.getChildNodes().item(0).getTextContent();				
			            }
			            
			            if(Objects.equals("departmentLevel6Description", an.getNodeName())) 
			            {
			            	departmentLevel6Description16 = an.getChildNodes().item(0).getTextContent();				
			            }
			            	     	
			    }
			        Row row = newSheet.createRow(fork);
			        fork++;
	            	row.createCell(0).setCellValue(input);
	            	row.createCell(1).setCellValue(two.get(ntid).toString());
	            	row.createCell(2).setCellValue(three.get(ntid).toString());	
	         		row.createCell(3).setCellValue(costCenter1);
	         		row.createCell(4).setCellValue(costCenterDescription2);
	         		row.createCell(5).setCellValue(supervisorId3);
	         		row.createCell(6).setCellValue(supervisorName4);
	         		row.createCell(7).setCellValue(departmentLevel1Id5);
	         		row.createCell(8).setCellValue(departmentLevel1Description6);
	         		row.createCell(9).setCellValue(departmentLevel2Id7);
	         		row.createCell(10).setCellValue(departmentLevel2Description8);
	         		row.createCell(11).setCellValue(departmentLevel3Id9);
	         		row.createCell(12).setCellValue(departmentLevel3Description10);
	         		row.createCell(13).setCellValue(departmentLevel4Id11);
	         		row.createCell(14).setCellValue(departmentLevel4Description12);
	         		row.createCell(15).setCellValue(departmentLevel5Id13);
	          		row.createCell(16).setCellValue(departmentLevel5Description14);
	          		row.createCell(17).setCellValue(departmentLevel6Id15);
	         		row.createCell(18).setCellValue(departmentLevel6Description16);
	         		row.createCell(19).setCellValue(activePfizer17);
	         		row.createCell(20).setCellValue(four.get(ntid));
	         		row.createCell(21).setCellValue(five.get(ntid));
	                 	   
	         		FileOutputStream oFile = new FileOutputStream(outputFile); 
	         		newWorkbook.write(oFile);
	         		
				 	
			 	costCenter1 = "";costCenterDescription2= ""; supervisorId3= ""; supervisorName4= ""; departmentLevel1Id5 = ""; departmentLevel1Description6 = "";  departmentLevel2Id7 = ""; departmentLevel2Description8 = ""; departmentLevel3Id9= "";  departmentLevel3Description10 = ""; departmentLevel4Id11= ""; departmentLevel4Description12=""; departmentLevel5Id13= ""; departmentLevel5Description14=""; departmentLevel6Id15 = ""; departmentLevel6Description16 = ""; activePfizer17="";
				 }
				 catch (Exception e) {

					  String blank = "N/A";
					    if(newSheet.getRow(fork) == null)
					    {
							System.out.println("null row at "+ntid);
							Row row = newSheet.createRow(fork);
							  fork++;
							
							row.createCell(0).setCellValue(input);
			         		row.createCell(1).setCellValue(two.get(ntid).toString());
			         		row.createCell(2).setCellValue(three.get(ntid).toString());
			         		row.createCell(3).setCellValue(blank);
			         		row.createCell(4).setCellValue(blank);
			         		row.createCell(5).setCellValue(blank);
			         		row.createCell(6).setCellValue(blank);
			         		row.createCell(7).setCellValue(blank);
			         		row.createCell(8).setCellValue(blank);
			         		row.createCell(9).setCellValue(blank);
			         		row.createCell(10).setCellValue(blank);
			         		row.createCell(11).setCellValue(blank);
			         		row.createCell(12).setCellValue(blank);
			         		row.createCell(13).setCellValue(blank);
			         		row.createCell(14).setCellValue(blank);
			         		row.createCell(15).setCellValue(blank);
			         		row.createCell(16).setCellValue(blank);
			         		row.createCell(17).setCellValue(blank);
			         		row.createCell(18).setCellValue(blank);
			         		row.createCell(19).setCellValue(blank);
			         		row.createCell(20).setCellValue(blank);
			         		row.createCell(21).setCellValue(blank);

					    }
					  
				}	
				 
			 }
				    
		} 	
	 	
	

}
