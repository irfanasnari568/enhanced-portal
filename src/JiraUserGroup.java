

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet("/JiraUserGroup")
public class JiraUserGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public JiraUserGroup() {
        super();
        // TODO Auto-generated constructor stub
    }

String userhome = System.getProperty("user.home");
	
	
String login= "bc8c2e2b59c44e8497a279fa85848fb3";
String password= "a03f507332514bddBf02288759961A1c";

	
	String authString = login + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	XSSFWorkbook newWorkbook = new XSSFWorkbook();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JiraUserGroup jira = new JiraUserGroup();
		
		
		
		XSSFSheet PGSSheet = null, RNDSheet = null, GISSheet = null, RREMSSheet = null, FBOSheet = null, CPPMSheet = null, AIBISheet = null, PROCUREMENTSheet = null, DSESheet = null, ARCHITECTURESheet = null, SUMMARYSHEET = null , HRCFSSheet = null, GPDSheet = null, ECSSheet = null;
		String PGSTEXT = "PGS-User List" , RNDTEXT ="RND-User List", GISTEXT = "GIS-User List", RREMSTEXT = "RREMS-User List", FBOTEXT = "FBO-User List";
	    String CPPMTEXT = "CPPM-User List", AIBITEXT = "AIBI-User List", PROCUREMENTTEXT = "PROCUREMENT-USER List", DSETEXT = "DSE-COMMERCIAL-USER List", SUMMARYTEXT = "SUMMARY";
	    String ARCHITECTURETEXT = "ARCHITECTURE-User List", HRCFSTEXT = "HRCFS-User List", GPDTEXT = "GPD-User List", ECSTEXT = "ECS-User List";
	    
		String PGSGROUP = "jiraprodpgs", RNDGROUP = "jiraprodrnd", GISGROUP="jiraprod", FBOGROUP="jiraprodfbo", RREMSGROUP="jiraprodrrems", CPPMGROUP="jiraprodcppm", AIBIGROUP="jiraprodaibi", PROCUREMENTGROUP="jiraprodprocurement", DSEGROUP="jiraproddse-commercial";
		String ARCHITECTUREGROUP = "jiraprodarchitecture", HRCFSGROUP = "jiraprodhrcfs", GPDGROUP = "jiraprodgpd", ECSGROUP = "jiraprodecs";
		
		int i = 0;
		int PGSCount = 0;
		
		 ArrayList<String> PGSDataListUserName = new ArrayList<String>();
		 ArrayList<String> PGSDataListDisplayName = new ArrayList<String>();
		 ArrayList<String> PGSDataListEmailAddress = new ArrayList<String>();
		 ArrayList<Boolean> PGSDataListFlag= new ArrayList<Boolean>();
		 ArrayList<ArrayList<String>> PGSContainer = new ArrayList<>();
		 ArrayList<ArrayList<Boolean>> PGSFlagContainer = new ArrayList<>();
	    	
	    	try {
				PGSCount = jira.passCount(PGSGROUP);
				}
			catch (JSONException e1) {
				e1.printStackTrace();
			}
			
	    	while(i < PGSCount)
	    	{
	    		try {
	    			PGSContainer = jira.Run(i, PGSGROUP, PGSDataListUserName, PGSDataListDisplayName, PGSDataListEmailAddress);
	    			PGSFlagContainer = jira.getUserFLAG(i, PGSGROUP, PGSDataListFlag);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		i = i+50;
	    	}
	    	
	    	PGSDataListUserName = PGSContainer.get(0);
	    	PGSDataListDisplayName = PGSContainer.get(1);
	    	PGSDataListEmailAddress = PGSContainer.get(2);	
	    	
	    	PGSDataListFlag = PGSFlagContainer.get(0);
	  		System.out.println("PGS Group Count is "+PGSCount);
	  		
	  		System.out.println(PGSDataListUserName.toString());
	  		System.out.println(PGSDataListDisplayName.toString());
	  		System.out.println(PGSDataListEmailAddress.toString());
	  		System.out.println(PGSDataListFlag.toString());
	  		
	  		// PGS END HERE	
	  		
	  		int i1 = 0;
			int RNDCount = 0;
			
			 ArrayList<String> RNDDataListUserName = new ArrayList<String>();
			 ArrayList<String> RNDDataListDisplayName = new ArrayList<String>();
			 ArrayList<String> RNDDataListEmailAddress = new ArrayList<String>();
			 ArrayList<Boolean> RNDDataListFlag= new ArrayList<Boolean>();
			 ArrayList<ArrayList<String>> RNDContainer = new ArrayList<>();
			 ArrayList<ArrayList<Boolean>> RNDFlagContainer = new ArrayList<>();
		    	
		    	try {
		    		RNDCount = jira.passCount(RNDGROUP);
					}
				catch (JSONException e1) {
					e1.printStackTrace();
				}
				
		    	while(i1 < RNDCount)
		    	{
		    		try {
		    			RNDContainer = jira.Run(i1, RNDGROUP, RNDDataListUserName, RNDDataListDisplayName, RNDDataListEmailAddress);
		    			RNDFlagContainer = jira.getUserFLAG(i1, RNDGROUP, RNDDataListFlag);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		
		    		i1 = i1+50;
		    	}
		    	
		    	RNDDataListUserName = RNDContainer.get(0);
		    	RNDDataListDisplayName = RNDContainer.get(1);
		    	RNDDataListEmailAddress = RNDContainer.get(2);	
		    	
		    	RNDDataListFlag = RNDFlagContainer.get(0);
		  		System.out.println("RND Group Count is "+RNDCount);
		  		
		  		System.out.println(RNDDataListUserName.toString());
		  		System.out.println(RNDDataListDisplayName.toString());
		  		System.out.println(RNDDataListEmailAddress.toString());
		  		System.out.println(RNDDataListFlag.toString());
		  		
		  		// RND END HERE	
	  		
	  		int j = 0;
			int GISCount = 0;
			
			 ArrayList<String> GISDataListUserName = new ArrayList<String>();
			 ArrayList<String> GISDataListDisplayName = new ArrayList<String>();
			 ArrayList<String> GISDataListEmailAddress = new ArrayList<String>();
			 ArrayList<Boolean> GISDataListFlag= new ArrayList<Boolean>();
			 ArrayList<ArrayList<String>> GISContainer = new ArrayList<>();
			 ArrayList<ArrayList<Boolean>> GISFlagContainer = new ArrayList<>();
		    	
		    	try {
		    		GISCount = jira.passCount(GISGROUP);
					}
				catch (JSONException e1) {
					e1.printStackTrace();
				}
				
		    	while(j < GISCount)
		    	{
		    		try {
		    			GISContainer = jira.Run(j, GISGROUP, GISDataListUserName, GISDataListDisplayName, GISDataListEmailAddress);
		    			GISFlagContainer = jira.getUserFLAG(j, GISGROUP, GISDataListFlag);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		
		    		j = j+50;
		    	}
		    	
		    	GISDataListUserName = GISContainer.get(0);
		    	GISDataListDisplayName = GISContainer.get(1);
		    	GISDataListEmailAddress = GISContainer.get(2);	
		    	
		    	GISDataListFlag = GISFlagContainer.get(0);
		  		System.out.println("GIS Group Count is "+GISCount);
		  		
		  		System.out.println(GISDataListUserName.toString());
		  		System.out.println(GISDataListDisplayName.toString());
		  		System.out.println(GISDataListEmailAddress.toString());
		  		System.out.println(GISDataListFlag.toString());
		  		
		  		// GIS END HERE	
		  		
		  		int k = 0;
				int RREMSCount = 0;
				
				 ArrayList<String> RREMSDataListUserName = new ArrayList<String>();
				 ArrayList<String> RREMSDataListDisplayName = new ArrayList<String>();
				 ArrayList<String> RREMSDataListEmailAddress = new ArrayList<String>();
				 ArrayList<Boolean> RREMSDataListFlag= new ArrayList<Boolean>();
				 ArrayList<ArrayList<String>> RREMSContainer = new ArrayList<>();
				 ArrayList<ArrayList<Boolean>> RREMSFlagContainer = new ArrayList<>();
			    	
			    	try {
			    		RREMSCount = jira.passCount(RREMSGROUP);
						}
					catch (JSONException e1) {
						e1.printStackTrace();
					}
					
			    	while(k < RREMSCount)
			    	{
			    		try {
			    			RREMSContainer = jira.Run(k, RREMSGROUP, RREMSDataListUserName, RREMSDataListDisplayName, RREMSDataListEmailAddress);
			    			RREMSFlagContainer = jira.getUserFLAG(k, RREMSGROUP, RREMSDataListFlag);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    		
			    		k = k+50;
			    	}
			    	
			    	RREMSDataListUserName = RREMSContainer.get(0);
			    	RREMSDataListDisplayName = RREMSContainer.get(1);
			    	RREMSDataListEmailAddress = RREMSContainer.get(2);	
			    	
			    	RREMSDataListFlag = RREMSFlagContainer.get(0);
			  		System.out.println("RREMS Group Count is "+RREMSCount);
			  		
			  		//RREMS END HERE
			  		
			  		int l = 0;
					int FBOCount = 0;
					
					 ArrayList<String> FBODataListUserName = new ArrayList<String>();
					 ArrayList<String> FBODataListDisplayName = new ArrayList<String>();
					 ArrayList<String> FBODataListEmailAddress = new ArrayList<String>();
					 ArrayList<Boolean> FBODataListFlag= new ArrayList<Boolean>();
					 ArrayList<ArrayList<String>> FBOContainer = new ArrayList<>();
					 ArrayList<ArrayList<Boolean>> FBOFlagContainer = new ArrayList<>();
				    	
				    	try {
				    		FBOCount = jira.passCount(FBOGROUP);
							}
						catch (JSONException e1) {
							e1.printStackTrace();
						}
						
				    	while(l < FBOCount)
				    	{
				    		try {
				    			FBOContainer = jira.Run(l, FBOGROUP, FBODataListUserName, FBODataListDisplayName, FBODataListEmailAddress);
				    			FBOFlagContainer = jira.getUserFLAG(l, FBOGROUP, FBODataListFlag);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				    		
				    		l = l+50;
				    	}
				    	
				    	FBODataListUserName = FBOContainer.get(0);
				    	FBODataListDisplayName = FBOContainer.get(1);
				    	FBODataListEmailAddress = FBOContainer.get(2);	
				    	
				    	FBODataListFlag = FBOFlagContainer.get(0);
				  		System.out.println("FBO Group Count is "+FBOCount);
				  		
				  		//FBO END HERE
	  		
				  		int m = 0;
						int CPPMCount = 0;
						
						 ArrayList<String> CPPMDataListUserName = new ArrayList<String>();
						 ArrayList<String> CPPMDataListDisplayName = new ArrayList<String>();
						 ArrayList<String> CPPMDataListEmailAddress = new ArrayList<String>();
						 ArrayList<Boolean> CPPMDataListFlag= new ArrayList<Boolean>();
						 ArrayList<ArrayList<String>> CPPMContainer = new ArrayList<>();
						 ArrayList<ArrayList<Boolean>> CPPMFlagContainer = new ArrayList<>();
					    	
					    	try {
					    		CPPMCount = jira.passCount(CPPMGROUP);
								}
							catch (JSONException e1) {
								e1.printStackTrace();
							}
							
					    	while(m < CPPMCount)
					    	{
					    		try {
					    			CPPMContainer = jira.Run(m, CPPMGROUP, CPPMDataListUserName, CPPMDataListDisplayName, CPPMDataListEmailAddress);
					    			CPPMFlagContainer = jira.getUserFLAG(m, CPPMGROUP, CPPMDataListFlag);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
					    		
					    		m = m+50;
					    	}
					    	
					    	CPPMDataListUserName = CPPMContainer.get(0);
					    	CPPMDataListDisplayName = CPPMContainer.get(1);
					    	CPPMDataListEmailAddress = CPPMContainer.get(2);	
					    	
					    	CPPMDataListFlag = CPPMFlagContainer.get(0);
					  		System.out.println("CPPM Group Count is "+CPPMCount);
					  		
					  		//CPPM END HERE	
					  		
					  		int n = 0;
							int AIBICount = 0;
							
							 ArrayList<String> AIBIDataListUserName = new ArrayList<String>();
							 ArrayList<String> AIBIDataListDisplayName = new ArrayList<String>();
							 ArrayList<String> AIBIDataListEmailAddress = new ArrayList<String>();
							 ArrayList<Boolean> AIBIDataListFlag= new ArrayList<Boolean>();
							 ArrayList<ArrayList<String>> AIBIContainer = new ArrayList<>();
							 ArrayList<ArrayList<Boolean>> AIBIFlagContainer = new ArrayList<>();
						    	
						    	try {
						    		AIBICount = jira.passCount(AIBIGROUP);
									}
								catch (JSONException e1) {
									e1.printStackTrace();
								}
								
						    	while(n < AIBICount)
						    	{
						    		try {
						    			AIBIContainer = jira.Run(n, AIBIGROUP, AIBIDataListUserName, AIBIDataListDisplayName, AIBIDataListEmailAddress);
						    			AIBIFlagContainer = jira.getUserFLAG(n, AIBIGROUP, AIBIDataListFlag);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
						    		
						    		n = n+50;
						    	}
						    	
						    	AIBIDataListUserName = AIBIContainer.get(0);
						    	AIBIDataListDisplayName = AIBIContainer.get(1);
						    	AIBIDataListEmailAddress = AIBIContainer.get(2);	
						    	
						    	AIBIDataListFlag = AIBIFlagContainer.get(0);
						  		System.out.println("AIBI Group Count is "+AIBICount);
						  		
						  		//AIBI END HERE	
						  		
						  		int o = 0;
								int PROCUREMENTCount = 0;
								
								 ArrayList<String> PROCUREMENTDataListUserName = new ArrayList<String>();
								 ArrayList<String> PROCUREMENTDataListDisplayName = new ArrayList<String>();
								 ArrayList<String> PROCUREMENTDataListEmailAddress = new ArrayList<String>();
								 ArrayList<Boolean> PROCUREMENTDataListFlag= new ArrayList<Boolean>();
								 ArrayList<ArrayList<String>> PROCUREMENTContainer = new ArrayList<>();
								 ArrayList<ArrayList<Boolean>> PROCUREMENTFlagContainer = new ArrayList<>();
							    	
							    	try {
							    		PROCUREMENTCount = jira.passCount(PROCUREMENTGROUP);
										}
									catch (JSONException e1) {
										e1.printStackTrace();
									}
									
							    	while(o < PROCUREMENTCount)
							    	{
							    		try {
							    			PROCUREMENTContainer = jira.Run(o, PROCUREMENTGROUP, PROCUREMENTDataListUserName, PROCUREMENTDataListDisplayName, PROCUREMENTDataListEmailAddress);
							    			PROCUREMENTFlagContainer = jira.getUserFLAG(o, PROCUREMENTGROUP, PROCUREMENTDataListFlag);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
							    		
							    		o = o+50;
							    	}
							    	try {
							    	PROCUREMENTDataListUserName = PROCUREMENTContainer.get(0);
							    	PROCUREMENTDataListDisplayName = PROCUREMENTContainer.get(1);
							    	PROCUREMENTDataListEmailAddress = PROCUREMENTContainer.get(2);	
							    	
							    	PROCUREMENTDataListFlag = PROCUREMENTFlagContainer.get(0);
							  		
							    	}
							    	catch(Exception e)
							    	{
							    		
							    	}
							    	System.out.println("PROCUREMENT Group Count is "+PROCUREMENTCount);
							  		//PROCUREMENT END HERE	
							  		
							  		int p = 0;
									int DSECount = 0;
									
									 ArrayList<String> DSEDataListUserName = new ArrayList<String>();
									 ArrayList<String> DSEDataListDisplayName = new ArrayList<String>();
									 ArrayList<String> DSEDataListEmailAddress = new ArrayList<String>();
									 ArrayList<Boolean> DSEDataListFlag= new ArrayList<Boolean>();
									 ArrayList<ArrayList<String>> DSEContainer = new ArrayList<>();
									 ArrayList<ArrayList<Boolean>> DSEFlagContainer = new ArrayList<>();
								    	
								    	try {
								    		DSECount = jira.passCount(DSEGROUP);
											}
										catch (JSONException e1) {
											e1.printStackTrace();
										}
										
								    	while(p < DSECount)
								    	{
								    		try {
								    			DSEContainer = jira.Run(p, DSEGROUP, DSEDataListUserName, DSEDataListDisplayName, DSEDataListEmailAddress);
								    			DSEFlagContainer = jira.getUserFLAG(p, DSEGROUP, DSEDataListFlag);
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
								    		
								    		p = p+50;
								    	}
								    	
								    	DSEDataListUserName = DSEContainer.get(0);
								    	DSEDataListDisplayName = DSEContainer.get(1);
								    	DSEDataListEmailAddress = DSEContainer.get(2);	
								    	
								    	DSEDataListFlag = DSEFlagContainer.get(0);
								  		System.out.println("DSE Group Count is "+DSECount);
								  		
								  		//DSE END HERE	
								  		
		int q = 0;
		int ARCHITECTURECount = 0;
										
		ArrayList<String> ARCHITECTUREDataListUserName = new ArrayList<String>();
		ArrayList<String> ARCHITECTUREDataListDisplayName = new ArrayList<String>();
		ArrayList<String> ARCHITECTUREDataListEmailAddress = new ArrayList<String>();
		ArrayList<Boolean> ARCHITECTUREDataListFlag= new ArrayList<Boolean>();
		ArrayList<ArrayList<String>> ARCHITECTUREContainer = new ArrayList<>();
		ArrayList<ArrayList<Boolean>> ARCHITECTUREFlagContainer = new ArrayList<>();
									    	
		try {
			ARCHITECTURECount = jira.passCount(ARCHITECTUREGROUP);
		}
		catch (JSONException e1) {
		e1.printStackTrace();
		}
											
		while(q < ARCHITECTURECount)
		{
		try {
		ARCHITECTUREContainer = jira.Run(q, ARCHITECTUREGROUP, ARCHITECTUREDataListUserName, ARCHITECTUREDataListDisplayName, ARCHITECTUREDataListEmailAddress);
		ARCHITECTUREFlagContainer = jira.getUserFLAG(q, ARCHITECTUREGROUP, ARCHITECTUREDataListFlag);
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
									    		
		q = q+50;
		}
									    	
		ARCHITECTUREDataListUserName = ARCHITECTUREContainer.get(0);
		ARCHITECTUREDataListDisplayName = ARCHITECTUREContainer.get(1);
		ARCHITECTUREDataListEmailAddress = ARCHITECTUREContainer.get(2);	
									    	
		ARCHITECTUREDataListFlag = ARCHITECTUREFlagContainer.get(0);
		System.out.println("ARCHITECTURE Group Count is "+ARCHITECTURECount);
									  		
		//ARCHITECTURE END HERE	

		int r = 0;
		int HRCFSCount = 0;
										
		ArrayList<String> HRCFSDataListUserName = new ArrayList<String>();
		ArrayList<String> HRCFSDataListDisplayName = new ArrayList<String>();
		ArrayList<String> HRCFSDataListEmailAddress = new ArrayList<String>();
		ArrayList<Boolean> HRCFSDataListFlag= new ArrayList<Boolean>();
		ArrayList<ArrayList<String>> HRCFSContainer = new ArrayList<>();
		ArrayList<ArrayList<Boolean>> HRCFSFlagContainer = new ArrayList<>();
									    	
		try {
			HRCFSCount = jira.passCount(HRCFSGROUP);
		}
		catch (JSONException e1) {
		e1.printStackTrace();
		}
											
		while(r < HRCFSCount)
		{
		try {
		HRCFSContainer = jira.Run(r, HRCFSGROUP, HRCFSDataListUserName, HRCFSDataListDisplayName, HRCFSDataListEmailAddress);
		HRCFSFlagContainer = jira.getUserFLAG(r, HRCFSGROUP, HRCFSDataListFlag);
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
									    		
		r = r+50;
		}
									    	
		HRCFSDataListUserName = HRCFSContainer.get(0);
		HRCFSDataListDisplayName = HRCFSContainer.get(1);
		HRCFSDataListEmailAddress = HRCFSContainer.get(2);	
									    	
		HRCFSDataListFlag = HRCFSFlagContainer.get(0);
		System.out.println("HRCFS Group Count is "+HRCFSCount);
									  		
		//HRCFS END HERE	
		
		int s = 0;
		int GPDCount = 0;
										
		ArrayList<String> GPDDataListUserName = new ArrayList<String>();
		ArrayList<String> GPDDataListDisplayName = new ArrayList<String>();
		ArrayList<String> GPDDataListEmailAddress = new ArrayList<String>();
		ArrayList<Boolean> GPDDataListFlag= new ArrayList<Boolean>();
		ArrayList<ArrayList<String>> GPDContainer = new ArrayList<>();
		ArrayList<ArrayList<Boolean>> GPDFlagContainer = new ArrayList<>();
									    	
		try {
			GPDCount = jira.passCount(GPDGROUP);
		}
		catch (JSONException e1) {
		e1.printStackTrace();
		}
											
		while(s < GPDCount)
		{
		try {
		GPDContainer = jira.Run(s, GPDGROUP, GPDDataListUserName, GPDDataListDisplayName, GPDDataListEmailAddress);
		GPDFlagContainer = jira.getUserFLAG(s, GPDGROUP, GPDDataListFlag);
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
									    		
		s = s+50;
		}
									    	
		GPDDataListUserName = GPDContainer.get(0);
		GPDDataListDisplayName = GPDContainer.get(1);
		GPDDataListEmailAddress = GPDContainer.get(2);	
									    	
		GPDDataListFlag = GPDFlagContainer.get(0);
		System.out.println("GPD Group Count is "+GPDCount);
									  		
		//GPD END HERE	
		
		int t = 0;
		int ECSCount = 0;
										
		ArrayList<String> ECSDataListUserName = new ArrayList<String>();
		ArrayList<String> ECSDataListDisplayName = new ArrayList<String>();
		ArrayList<String> ECSDataListEmailAddress = new ArrayList<String>();
		ArrayList<Boolean> ECSDataListFlag= new ArrayList<Boolean>();
		ArrayList<ArrayList<String>> ECSContainer = new ArrayList<>();
		ArrayList<ArrayList<Boolean>> ECSFlagContainer = new ArrayList<>();
									    	
		try {
			ECSCount = jira.passCount(ECSGROUP);
		}
		catch (JSONException e1) {
		e1.printStackTrace();
		}
											
		while(t < ECSCount)
		{
		try {
			ECSContainer = jira.Run(t, ECSGROUP, ECSDataListUserName, ECSDataListDisplayName, ECSDataListEmailAddress);
			ECSFlagContainer = jira.getUserFLAG(t, ECSGROUP, ECSDataListFlag);
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
									    		
		t = t+50;
		}
									    	
		ECSDataListUserName = ECSContainer.get(0);
		ECSDataListDisplayName = ECSContainer.get(1);
		ECSDataListEmailAddress = ECSContainer.get(2);	
									    	
		ECSDataListFlag = ECSFlagContainer.get(0);
		System.out.println("ECS Group Count is "+ECSCount);
									  		
		//ECS END HERE		

								  		
	jira.writeSummary(SUMMARYSHEET, PGSDataListUserName, RNDDataListUserName, GISDataListUserName, RREMSDataListUserName, FBODataListUserName, CPPMDataListUserName, AIBIDataListUserName, PROCUREMENTDataListUserName, DSEDataListUserName, ARCHITECTUREDataListUserName, HRCFSDataListUserName, GPDDataListUserName, ECSDataListUserName, SUMMARYTEXT);
				  		
	  		jira.WriteData(PGSSheet, PGSDataListUserName, PGSDataListDisplayName, PGSDataListEmailAddress, PGSDataListFlag, PGSTEXT);
	  		jira.WriteData(RNDSheet, RNDDataListUserName, RNDDataListDisplayName, RNDDataListEmailAddress, RNDDataListFlag, RNDTEXT);
	  		jira.WriteData(GISSheet, GISDataListUserName, GISDataListDisplayName, GISDataListEmailAddress, GISDataListFlag, GISTEXT);
	  		jira.WriteData(RREMSSheet, RREMSDataListUserName, RREMSDataListDisplayName, RREMSDataListEmailAddress, RREMSDataListFlag, RREMSTEXT);
	  		jira.WriteData(FBOSheet, FBODataListUserName, FBODataListDisplayName, FBODataListEmailAddress, FBODataListFlag, FBOTEXT);
	  		jira.WriteData(CPPMSheet, CPPMDataListUserName, CPPMDataListDisplayName, CPPMDataListEmailAddress, CPPMDataListFlag, CPPMTEXT);
	  		jira.WriteData(AIBISheet, AIBIDataListUserName, AIBIDataListDisplayName, AIBIDataListEmailAddress, AIBIDataListFlag, AIBITEXT);
	  		jira.WriteData(PROCUREMENTSheet, PROCUREMENTDataListUserName, PROCUREMENTDataListDisplayName, PROCUREMENTDataListEmailAddress, PROCUREMENTDataListFlag, PROCUREMENTTEXT);
	  		jira.WriteData(DSESheet, DSEDataListUserName, DSEDataListDisplayName, DSEDataListEmailAddress, DSEDataListFlag, DSETEXT);
	  		jira.WriteData(ARCHITECTURESheet, ARCHITECTUREDataListUserName, ARCHITECTUREDataListDisplayName, ARCHITECTUREDataListEmailAddress, ARCHITECTUREDataListFlag, ARCHITECTURETEXT);
	  		jira.WriteData(HRCFSSheet, HRCFSDataListUserName, HRCFSDataListDisplayName, HRCFSDataListEmailAddress, HRCFSDataListFlag, HRCFSTEXT);
	  		jira.WriteData(GPDSheet, GPDDataListUserName, GPDDataListDisplayName, GPDDataListEmailAddress, GPDDataListFlag, GPDTEXT);
	  		jira.WriteData(ECSSheet, ECSDataListUserName, ECSDataListDisplayName, ECSDataListEmailAddress, ECSDataListFlag, ECSTEXT);



	  		String filename = "Jira_user_report.xlsx";
	  		String outputFile= "/usr/local/Enhanced/reports/"+filename;
	  		//String outputFile= "D:\\Users\\"+filename;
	  		
	  		File file = new File(outputFile);

			if(file.exists())
			{
				//			Desktop desktop = Desktop.getDesktop();
				//			desktop.open(file);
				//			InputStream in = new URL(f).openStream();
				//			Files.copy(in, Paths.get(f));

   
				response.setContentType("application/vnd.ms-excel");   
				response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
				System.out.println("filename new  "+filename);
				
				BufferedInputStream in=null;
				ServletOutputStream outs=null;
				byte[] buffer = new byte[1024]; 
			
				
				try {
					int g = 0;
					in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
					outs=response.getOutputStream();
					while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
						outs.write(buffer, 0, g);  
					}  
					outs.flush();  

				} catch (IOException ioe) {
					ioe.printStackTrace(System.out);
				}

				System.out.println("Done !!");

				outs.close();
				in.close();
				
			}
			else
			{
				System.out.println("not found");
			}

	  		
	  		
	}
	
	
	public void writeSummary(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six, ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine, ArrayList<String> ten, ArrayList<String> eleven, ArrayList<String> twelve, ArrayList<String> thirteen, String text) throws IOException
	{
		//XSSFSheet newwSheet = newSheet;
		newSheet = newWorkbook.createSheet(text);
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		System.out.println("From summary tab");
		Row headerRow = newSheet.createRow(0);
		
			Cell headerCell1 = headerRow.createCell(0);	
			Cell headerCell2 = headerRow.createCell(1);	
			Cell headerCell3 = headerRow.createCell(2);	


			headerCell1.setCellValue("Business Line");	
			headerCell1.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(0);
			headerCell2.setCellValue("Count"); 
			headerCell2.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(1);
			headerCell3.setCellValue("($)Dollar"); 
			headerCell3.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(2);

			//
			Row row1 = newSheet.createRow(1);
			Cell PGS1 = row1.createCell(0);
			Cell PGS2 = row1.createCell(1);
			PGS1.setCellValue("PGS_RND");
			PGS2.setCellValue(one.size());
			
			Row row2 = newSheet.createRow(2);
			Cell RND1= row2.createCell(0);
			Cell RND2 = row2.createCell(1);
			RND1.setCellValue("RND");
			RND2.setCellValue(two.size());
			
			Row row3 = newSheet.createRow(3);
			Cell GIS1 = row3.createCell(0);
			Cell GIS2 = row3.createCell(1);
			GIS1.setCellValue("GIS");
			GIS2.setCellValue(three.size());
			
			Row row4 = newSheet.createRow(4);
			Cell RREMS1 = row4.createCell(0);
			Cell RREMS2 = row4.createCell(1);
			RREMS1.setCellValue("RREMS");
			RREMS2.setCellValue(four.size());
			
			Row row5 = newSheet.createRow(5);
			Cell FBO1 = row5.createCell(0);
			Cell FBO2 = row5.createCell(1);
			FBO1.setCellValue("FBO");
			FBO2.setCellValue(five.size());
			
			Row row6 = newSheet.createRow(6);
			Cell CPPM1 = row6.createCell(0);
			Cell CPPM2 = row6.createCell(1);
			CPPM1.setCellValue("CPPM");
			CPPM2.setCellValue(six.size());

			Row row7 = newSheet.createRow(7);
			Cell AIBI1 = row7.createCell(0);
			Cell AIBI2 = row7.createCell(1);
			AIBI1.setCellValue("AIBI");
			AIBI2.setCellValue(seven.size());
			
			Row row8 = newSheet.createRow(8);
			Cell PROCUREMENT1 = row8.createCell(0);
			Cell PROCUREMENT2 = row8.createCell(1);
			PROCUREMENT1.setCellValue("PROCUREMENT");
			PROCUREMENT2.setCellValue(eight.size());
			
			Row row9 = newSheet.createRow(9);
			Cell DSE1 = row9.createCell(0);
			Cell DSE2 = row9.createCell(1);
			DSE1.setCellValue("DSE-Comerrcial");
			DSE2.setCellValue(nine.size());
			
			Row row10 = newSheet.createRow(10);
			Cell ARCHITECTURE1 = row10.createCell(0);
			Cell ARCHITECTURE2 = row10.createCell(1);
			ARCHITECTURE1.setCellValue("ARCHITECTURE");
			ARCHITECTURE2.setCellValue(ten.size());
			
			Row row11 = newSheet.createRow(11);
			Cell HRCFS1 = row11.createCell(0);
			Cell HRCFS2 = row11.createCell(1);
			HRCFS1.setCellValue("HRCFS");
			HRCFS2.setCellValue(eleven.size());
			
			Row row12 = newSheet.createRow(12);
			Cell GPD1 = row12.createCell(0);
			Cell GPD2 = row12.createCell(1);
			GPD1.setCellValue("GPD");
			GPD2.setCellValue(twelve.size());
			
			Row row13 = newSheet.createRow(13);
			Cell ECS1 = row13.createCell(0);
			Cell ECS2 = row13.createCell(1);
			ECS1.setCellValue("ECS");
			ECS2.setCellValue(thirteen.size());
			
			System.out.println(one);
			
	}
	public int passCount(String groupName) throws JSONException, IOException
	{
		String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName; 		
		  
    	URL url = new URL(api);
    	TrustAllCertificates.install();
    	HttpURLConnection conn = null;
    	conn = (HttpURLConnection) url.openConnection();
    	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
    	String inputLine = "";
    	BufferedReader in = null;
    	System.out.println("Authentication process");
    	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    	StringBuffer response = new StringBuffer();
    	while((inputLine = in.readLine()) != null)
    	{
    		response.append(inputLine);
    	}
			in.close();
		  
    	JSONObject json  = new JSONObject(response.toString());
    	System.out.println("From Main method "+json.getInt("total"));
		
    	int PGSCount= 0;
		try {
			PGSCount = json.getInt("total");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return PGSCount;	
	}
	
public ArrayList<ArrayList<String>> Run(int startAt, String groupName, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three) throws IOException, JSONException  {
		
		String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName+"&startAt="+startAt; 				    	  
		URL url = new URL(api);
   	  	TrustAllCertificates.install();
   	  	HttpURLConnection conn = null;
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
		String inputLine = "";
		BufferedReader in = null;
   		System.out.println("Authentication process");
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
   	  	StringBuffer response = new StringBuffer();
   	  	while((inputLine = in.readLine()) != null)
   	  	{
   		  response.append(inputLine);
   	  	}
		in.close();
			    	  
        JSONObject json  = new JSONObject(response.toString());
        JSONArray innerJsonArray = json.getJSONArray("values");
           	    	  
        for(int i=0; i<innerJsonArray.length(); i++)
        { 
        	    		  
        	one.add(innerJsonArray.getJSONObject(i).getString("name"));
        	two.add(innerJsonArray.getJSONObject(i).getString("displayName"));
        	three.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
        	
        }   	    	 
//        for(int k=0; k<one.size(); k++)
//        {
//        	System.out.println(k+" "+one.get(k));
//        }
       
       
       
        ArrayList<ArrayList<String>> ParentList = new ArrayList<ArrayList<String>>();
        ParentList.add(one);
        ParentList.add(two);
        ParentList.add(three);
        
       
        return ParentList;
	}

public ArrayList<ArrayList<Boolean>> getUserFLAG(int startAt, String groupName, ArrayList<Boolean> four) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName+"&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
    JSONObject json  = new JSONObject(response.toString());
    JSONArray innerJsonArray = json.getJSONArray("values");
       	    	  
    for(int i=0; i<innerJsonArray.length(); i++)
    { 
    	    		  
    	four.add(innerJsonArray.getJSONObject(i).getBoolean("active"));    	
    }   	    	 
    
	ArrayList<ArrayList<Boolean>> FlagList = new ArrayList<>();
	FlagList.add(four);
	
	return FlagList;

}

	public void WriteData(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<Boolean> four, String TEXT) throws IOException
	{
  		String filename = "Jira_user_report.xlsx";
  		String outputFile= "/usr/local/Enhanced/reports/"+filename;
  		//String outputFile= "D:\\Users\\"+filename;

		
		newSheet = newWorkbook.createSheet(TEXT);
		
		System.out.println("Sheet Created");
		String Columns[] = new String[] {"Username", "Full Name", "Email Address", "Active Flag"};     
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
			
		Row headerRow = newSheet.createRow(0);
			
		for(int columnI=0; columnI<Columns.length; columnI++)
		{ 
			Cell cell = headerRow.createCell(columnI);	
			cell.setCellValue(Columns[columnI]);	
			cell.setCellStyle(headerCellStyle); 
			newSheet.autoSizeColumn(columnI);	
		}
		int fork = 1;
		for(int i=0; i<one.size(); i++)
		{
			System.out.println("Running "+i);
			System.out.println(one.get(i));
			Row row = newSheet.createRow(fork);
	        fork++;
	        row.createCell(0).setCellValue(one.get(i));
	        row.createCell(1).setCellValue(two.get(i));
	        row.createCell(2).setCellValue(three.get(i));
	        row.createCell(3).setCellValue(four.get(i));

	        //FileOutputStream oFile = new FileOutputStream("D:\\Users\\Jira_user_report.xlsx"); 
	        FileOutputStream oFile = new FileOutputStream(outputFile); 
     		newWorkbook.write(oFile);
	}

}
	
}
