

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


@WebServlet("/JiraController")
public class JiraController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public JiraController() {
		super();
		// TODO Auto-generated constructor stub
	}

	String userhome = System.getProperty("user.home");
	
	int startAt = 0;

	
	String login= "bc8c2e2b59c44e8497a279fa85848fb3";
	String password= "a03f507332514bddBf02288759961A1c";
	
	int maxResults = 50;
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	String Researchlogin = "SRVAMR-ATLRPT";
	String Researchpass = "Pfe63478";
	

	String userNameDB = "JIRADB_P_User";
	String passwordDB = "2wsxcde@2019";
	String url = "jdbc:sqlserver://AMRDRMW638:2023;databaseName=JIRADB_P";

	

	XSSFWorkbook newWorkbook = new XSSFWorkbook();
	
	XSSFSheet PGSSheet, RNDSheet, GISSheet, RREMSSheet, FBOSheet, CPPMSheet, AIBISheet, PROCUREMENTSheet, DSESheet, FULLSHEET, SUMMARYSHEET , ARCHITECTURESheet, HRCFSSheet = null, GPDSheet = null, ECSSheet = null;
	String PGSTEXT = "PGS-User List";
	String RNDTEXT = "RND-User List";
	String GISTEXT = "GIS-User List";
	String RREMSTEXT = "RREMS-User List";
	String FBOTEXT = "FBO-User List";
    String CPPMTEXT = "CPPM-User List";
    String AIBITEXT = "AIBI-User List";
    String PROCUREMENTTEXT = "PROCUREMENT-USER List";
	String DSETEXT = "DSE-COMMERCIAL-USER List";
	String ARCHITECTURETEXT = "ARCHITECTURE-USER List";
	String HRCFSTEXT = "HRCFS-USER List";
	String GPDTEXT = "GPD-USER List";
	String ECSTEXT = "ECS-USER List";
	String FULLTEXT = "Full List";
	 String SUMMARYTEXT = "SUMMARY";
  
	String PGSGROUP = "jiraprodpgs", RNDGROUP = "jiraprodrnd", GISGROUP="jiraprod", FBOGROUP="jiraprodfbo", RREMSGROUP="jiraprodrrems", CPPMGROUP="jiraprodcppm", AIBIGROUP="jiraprodaibi", PROCUREMENTGROUP="jiraprodprocurement", DSEGROUP="jiraproddse-commercial";
	String ARCHITECTUREGROUP="jiraprodarchitecture", HRCFSGROUP = "jiraprodhrcfs", GPDGROUP = "jiraprodgpd", ECSGROUP = "jiraprodecs";	
	String authString = login + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	//Parent ArrayList to unwrap data.
	public ArrayList<ArrayList<String>> PGSData = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<Boolean>> PGSActiveCall = new ArrayList<ArrayList<Boolean>>();

	//ArrayList Holder
	public ArrayList<ArrayList<String>> PGSDataList = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<Boolean>> PGSActive = new ArrayList<ArrayList<Boolean>>();

	//ArrayList to store all users details.
	public ArrayList<String> PGSDataListUserName = new ArrayList<String>();
	public ArrayList<String> PGSDataListDisplayName = new ArrayList<String>();
	public ArrayList<String> PGSDataListEmailAddress = new ArrayList<String>();
	public ArrayList<Boolean> PGSDataListFlag= new ArrayList<Boolean>();
	
	
// ==== RND List
	
	//Parent ArrayList to unwrap data.
	public ArrayList<ArrayList<String>> RNDData = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<Boolean>> RNDActiveCall = new ArrayList<ArrayList<Boolean>>();

	//ArrayList Holder
	public ArrayList<ArrayList<String>> RNDDataList = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<Boolean>> RNDActive = new ArrayList<ArrayList<Boolean>>();

	//ArrayList to store all users details.
	public ArrayList<String> RNDDataListUserName = new ArrayList<String>();
	public ArrayList<String> RNDDataListDisplayName = new ArrayList<String>();
	public ArrayList<String> RNDDataListEmailAddress = new ArrayList<String>();
	public ArrayList<Boolean> RNDDataListFlag= new ArrayList<Boolean>();

	
// ---- GIS List 
	
	//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> GISData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> GISActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> GISDataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> GISActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> GISDataListUserName = new ArrayList<String>();
		public ArrayList<String> GISDataListDisplayName = new ArrayList<String>();
		public ArrayList<String> GISDataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> GISDataListFlag= new ArrayList<Boolean>();
	
		
// ---- RREMS List 
		
	//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> RREMSData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> RREMSActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> RREMSDataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> RREMSActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> RREMSDataListUserName = new ArrayList<String>();
		public ArrayList<String> RREMSDataListDisplayName = new ArrayList<String>();
		public ArrayList<String> RREMSDataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> RREMSDataListFlag= new ArrayList<Boolean>();
		
// ---- FBO List 
		
	//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> FBOData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> FBOActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> FBODataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> FBOActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> FBODataListUserName = new ArrayList<String>();
		public ArrayList<String> FBODataListDisplayName = new ArrayList<String>();
		public ArrayList<String> FBODataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> FBODataListFlag= new ArrayList<Boolean>();
		
		
// ---- CPPM List 
		
	//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> CPPMData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> CPPMActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> CPPMDataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> CPPMActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> CPPMDataListUserName = new ArrayList<String>();
		public ArrayList<String> CPPMDataListDisplayName = new ArrayList<String>();
		public ArrayList<String> CPPMDataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> CPPMDataListFlag= new ArrayList<Boolean>();
		
		
// ---- AIBI List 
		
	//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> AIBIData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> AIBIActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> AIBIDataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> AIBIActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> AIBIDataListUserName = new ArrayList<String>();
		public ArrayList<String> AIBIDataListDisplayName = new ArrayList<String>();
		public ArrayList<String> AIBIDataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> AIBIDataListFlag= new ArrayList<Boolean>();
		
// ---- PROCUREMENT List 
		
//Parent ArrayList to unwrap data.
		public ArrayList<ArrayList<String>> PROCUREMENTData = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> PROCUREMENTActiveCall = new ArrayList<ArrayList<Boolean>>();

		//ArrayList Holder
		public ArrayList<ArrayList<String>> PROCUREMENTDataList = new ArrayList<ArrayList<String>>();
		public ArrayList<ArrayList<Boolean>> PROCUREMENTActive = new ArrayList<ArrayList<Boolean>>();

		//ArrayList to store all users details.
		public ArrayList<String> PROCUREMENTDataListUserName = new ArrayList<String>();
		public ArrayList<String> PROCUREMENTDataListDisplayName = new ArrayList<String>();
		public ArrayList<String> PROCUREMENTDataListEmailAddress = new ArrayList<String>();
		public ArrayList<Boolean> PROCUREMENTDataListFlag= new ArrayList<Boolean>();

		
// ---- DSE-COMMERICIAL List 
		
		//Parent ArrayList to unwrap data.
				public ArrayList<ArrayList<String>> DSEData = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> DSEActiveCall = new ArrayList<ArrayList<Boolean>>();

				//ArrayList Holder
				public ArrayList<ArrayList<String>> DSEDataList = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> DSEActive = new ArrayList<ArrayList<Boolean>>();

				//ArrayList to store all users details.
				public ArrayList<String> DSEDataListUserName = new ArrayList<String>();
				public ArrayList<String> DSEDataListDisplayName = new ArrayList<String>();
				public ArrayList<String> DSEDataListEmailAddress = new ArrayList<String>();
				public ArrayList<Boolean> DSEDataListFlag= new ArrayList<Boolean>();


				// ---- ARCHITECTURE List 
				
				//Parent ArrayList to unwrap data.
						public ArrayList<ArrayList<String>> ARCHITECTUREData = new ArrayList<ArrayList<String>>();
						public ArrayList<ArrayList<Boolean>> ARCHITECTUREActiveCall = new ArrayList<ArrayList<Boolean>>();

						//ArrayList Holder
						public ArrayList<ArrayList<String>> ARCHITECTUREDataList = new ArrayList<ArrayList<String>>();
						public ArrayList<ArrayList<Boolean>> ARCHITECTUREActive = new ArrayList<ArrayList<Boolean>>();

						//ArrayList to store all users details.
						public ArrayList<String> ARCHITECTUREDataListUserName = new ArrayList<String>();
						public ArrayList<String> ARCHITECTUREDataListDisplayName = new ArrayList<String>();
						public ArrayList<String> ARCHITECTUREDataListEmailAddress = new ArrayList<String>();
						public ArrayList<Boolean> ARCHITECTUREDataListFlag= new ArrayList<Boolean>();
						
						
				// ---- HRCFS List 
						
				//Parent ArrayList to unwrap data.
				public ArrayList<ArrayList<String>> HRCFSData = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> HRCFSActiveCall = new ArrayList<ArrayList<Boolean>>();

				//ArrayList Holder
				public ArrayList<ArrayList<String>> HRCFSDataList = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> HRCFSActive = new ArrayList<ArrayList<Boolean>>();

				//ArrayList to store all users details.
				public ArrayList<String> HRCFSDataListUserName = new ArrayList<String>();
				public ArrayList<String> HRCFSDataListDisplayName = new ArrayList<String>();
				public ArrayList<String> HRCFSDataListEmailAddress = new ArrayList<String>();
				public ArrayList<Boolean> HRCFSDataListFlag= new ArrayList<Boolean>();


				// ---- GPD List 
				
				//Parent ArrayList to unwrap data.
				public ArrayList<ArrayList<String>> GPDData = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> GPDActiveCall = new ArrayList<ArrayList<Boolean>>();

				//ArrayList Holder
				public ArrayList<ArrayList<String>> GPDDataList = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> GPDActive = new ArrayList<ArrayList<Boolean>>();

				//ArrayList to store all users details.
				public ArrayList<String> GPDDataListUserName = new ArrayList<String>();
				public ArrayList<String> GPDDataListDisplayName = new ArrayList<String>();
				public ArrayList<String> GPDDataListEmailAddress = new ArrayList<String>();
				public ArrayList<Boolean> GPDDataListFlag= new ArrayList<Boolean>();
				
				
	// ---- ECS List 
				
				//Parent ArrayList to unwrap data.
				public ArrayList<ArrayList<String>> ECSData = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> ECSActiveCall = new ArrayList<ArrayList<Boolean>>();

				//ArrayList Holder
				public ArrayList<ArrayList<String>> ECSDataList = new ArrayList<ArrayList<String>>();
				public ArrayList<ArrayList<Boolean>> ECSActive = new ArrayList<ArrayList<Boolean>>();

				//ArrayList to store all users details.
				public ArrayList<String> ECSDataListUserName = new ArrayList<String>();
				public ArrayList<String> ECSDataListDisplayName = new ArrayList<String>();
				public ArrayList<String> ECSDataListEmailAddress = new ArrayList<String>();
				public ArrayList<Boolean> ECSDataListFlag= new ArrayList<Boolean>();
				
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JiraController jira = new JiraController();
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			

		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}			
	

		int i = 0, i1= 0, j=0,k=0,l=0, m=0, n=0, o=0,p=0, q=0, r=0, s=0, t=0;
		int PGSCount = 0;
  	
		try {
			PGSCount = jira.passPgsCount(PGSGROUP);
			}
		catch (JSONException e1) {
			e1.printStackTrace();
		}
		
  		System.out.println("PGS Group Count is "+PGSCount);
  		while(i<PGSCount)
  		{
  		  try {
  			  jira.PGSRun(i);
  			  jira.PGSActive(i);
		} catch (JSONException e) {
			e.printStackTrace();
		}
  		  	i= i + 50;	
  		}
  	
  	  PGSData= jira.getPGSData();
  	  ArrayList<String> one = new ArrayList<>();
  	  ArrayList<String> two = new ArrayList<>();
  	  ArrayList<String> three = new ArrayList<>();
  	  ArrayList<Boolean> four = new ArrayList<>();
 	 
  	  one = PGSData.get(0);
  	  two = PGSData.get(1);
  	  three = PGSData.get(2);
  	  
  	  PGSActiveCall = jira.getPGSActive();
  	  four = PGSActiveCall.get(0);
  	  
  	  System.out.println(one.toString());
  	  System.out.println(two.toString());
  	  System.out.println(three.toString());
  	    	  System.out.println(four.toString());
  	    	
  	    	
  	 
  	  // PGS END HERE
  	  
  			int RNDCount = 0;
  		  	
  			try {
  				RNDCount = jira.passRndCount(RNDGROUP);
  				}
  			catch (JSONException e1) {
  				e1.printStackTrace();
  			}
  			
  	  		System.out.println("RND Group Count is "+RNDCount);
  	  		while(i1<RNDCount)
  	  		{
  	  		  try {
  	  			  jira.RNDRun(i1);
  	  			  jira.RNDActive(i1);
  			} catch (JSONException e) {
  				e.printStackTrace();
  			}
  	  		  	i1= i1 + 50;	
  	  		}
  	  	
  	  	  RNDData = jira.getRNDData();
  	  	  ArrayList<String> oneRnd = new ArrayList<>();
  	  	  ArrayList<String> twoRnd = new ArrayList<>();
  	  	  ArrayList<String> threeRnd = new ArrayList<>();
  	  	  ArrayList<Boolean> fourRnd = new ArrayList<>();
  	 	 
  	  	oneRnd = RNDData.get(0);
  	  	twoRnd = RNDData.get(1);
  	  	threeRnd = RNDData.get(2);
  	  	  
  		RNDActiveCall = jira.getRNDActive();
  		fourRnd = RNDActiveCall.get(0);
  	  	  
  	  	  System.out.println(oneRnd.toString());
  	  	  System.out.println(twoRnd.toString());
  	  	  System.out.println(threeRnd.toString());
  	  	    	  System.out.println(fourRnd.toString());
  	  	    	
  	  	    	
  	  	 
  	  	  // RND END HERE

  	int GISCount = 0;
  	
	try {
		GISCount = jira.passPgsCount(GISGROUP);
		}
	catch (JSONException e1) {
		e1.printStackTrace();
	}
	
		System.out.println("GIS Group Count is "+GISCount);
		while(j<GISCount)
		{
		  try {
			  jira.GISRun(j);
			  jira.GISActive(j);
	} catch (JSONException e) {
		e.printStackTrace();
	}
		  	j= j + 50;	
		}
	
	  GISData= jira.getGISData();
	  ArrayList<String> oneGis = new ArrayList<>();
	  ArrayList<String> twoGis = new ArrayList<>();
	  ArrayList<String> threeGis = new ArrayList<>();
	  ArrayList<Boolean> fourGis = new ArrayList<>();
	 
	  oneGis = GISData.get(0);
	  twoGis = GISData.get(1);
	  threeGis = GISData.get(2);
	  
	  GISActiveCall = jira.getGISActive();
	  fourGis = GISActiveCall.get(0);
	  
	  System.out.println(oneGis.toString());
	  System.out.println(twoGis.toString());
	  System.out.println(threeGis.toString());
	    	  System.out.println(fourGis.toString());
	 

	  // GIS END HERE
  	  
  	  
	  	int RREMSCount = 0;
	  	
		try {
			RREMSCount = jira.passPgsCount(RREMSGROUP);
			}
		catch (JSONException e1) {
			e1.printStackTrace();
		}
		
			System.out.println("RREMS Group Count is "+RREMSCount);
			while(k<RREMSCount)
			{
			  try {
				  jira.RREMSRun(k);
				  jira.RREMSActive(k);
		} catch (JSONException e) {
			e.printStackTrace();
		}
			  	k= k + 50;	
			}
		
		  RREMSData= jira.getRREMSData();
		  ArrayList<String> oneRrems = new ArrayList<>();
		  ArrayList<String> twoRrems = new ArrayList<>();
		  ArrayList<String> threeRrems = new ArrayList<>();
		  ArrayList<Boolean> fourRrems = new ArrayList<>();
		 
		  oneRrems = RREMSData.get(0);
		  twoRrems = RREMSData.get(1);
		  threeRrems = RREMSData.get(2);
		  
		  RREMSActiveCall = jira.getRREMSActive();
		  fourRrems= RREMSActiveCall.get(0);
		  
		  System.out.println(oneRrems.toString());
		  System.out.println(twoRrems.toString());
		  System.out.println(threeRrems.toString());
		    	  System.out.println(fourRrems.toString());
		  
		      
		  // RREMS END HERE
	  	  
		  	int FBOCount = 0;
		  	
			try {
				FBOCount = jira.passPgsCount(FBOGROUP);
				}
			catch (JSONException e1) {
				e1.printStackTrace();
			}
			
				System.out.println("FBO Group Count is "+FBOCount);
				while(l<FBOCount)
				{
				  try {
					  jira.FBORun(l);
					  jira.FBOActive(l);
			} catch (JSONException e) {
				e.printStackTrace();
			}
				  	l= l + 50;	
				}
			
			  FBOData= jira.getFBOData();
			  ArrayList<String> oneFbo = new ArrayList<>();
			  ArrayList<String> twoFbo = new ArrayList<>();
			  ArrayList<String> threeFbo = new ArrayList<>();
			  ArrayList<Boolean> fourFbo = new ArrayList<>();
			 
			  oneFbo = FBOData.get(0);
			  twoFbo = FBOData.get(1);
			  threeFbo = FBOData.get(2);
			  
			  FBOActiveCall = jira.getFBOActive();
			  fourFbo = FBOActiveCall.get(0);
			  
			  System.out.println(oneFbo.toString());
			  System.out.println(twoFbo.toString());
			  System.out.println(threeFbo.toString());
			    	  System.out.println(fourFbo.toString());
			  
			    	
			  // FBO END HERE



			  	int CPPMCount = 0;
			  	
				try {
					CPPMCount  = jira.passPgsCount(CPPMGROUP);
					}
				catch (JSONException e1) {
					e1.printStackTrace();
				}
				
					System.out.println("CPPM Group Count is "+CPPMCount);
					while(m<CPPMCount)
					{
					  try {
						  jira.CPPMRun(m);
						  jira.CPPMActive(m);
				} catch (JSONException e) {
					e.printStackTrace();
				}
					  	m= m + 50;	
					}
				
				  CPPMData= jira.getCPPMData();
				  ArrayList<String> oneCppm = new ArrayList<>();
				  ArrayList<String> twoCppm = new ArrayList<>();
				  ArrayList<String> threeCppm = new ArrayList<>();
				  ArrayList<Boolean> fourCppm = new ArrayList<>();
				 
				  oneCppm = CPPMData.get(0);
				  twoCppm = CPPMData.get(1);
				  threeCppm = CPPMData.get(2);
				  
				  CPPMActiveCall = jira.getCPPMActive();
				  fourCppm = CPPMActiveCall.get(0);
				  
				  System.out.println(oneCppm.toString());
				  System.out.println(twoCppm.toString());
				  System.out.println(threeCppm.toString());
				    	  System.out.println(fourCppm.toString());
				  
				    	
				  // CPPM END HERE
	
				  	int AIBICount = 0;
				  	
					try {
						AIBICount  = jira.passPgsCount(AIBIGROUP);
						}
					catch (JSONException e1) {
						e1.printStackTrace();
					}
					
						System.out.println("AIBI Group Count is "+AIBICount);
						while(n<AIBICount)
						{
						  try {
							  jira.AIBIRun(n);
							  jira.AIBIActive(n);
					} catch (JSONException e) {
						e.printStackTrace();
					}
						  	n= n + 50;	
						}
					
				      AIBIData= jira.getAIBIData();
					  ArrayList<String> oneAibi = new ArrayList<>();
					  ArrayList<String> twoAibi = new ArrayList<>();
					  ArrayList<String> threeAibi = new ArrayList<>();
					  ArrayList<Boolean> fourAibi = new ArrayList<>();
					 
					  oneAibi = AIBIData.get(0);
					  twoAibi = AIBIData.get(1);
					  threeAibi = AIBIData.get(2);
					  
					  AIBIActiveCall = jira.getAIBIActive();
					  fourAibi = AIBIActiveCall.get(0);
					  
					  System.out.println(oneAibi.toString());
					  System.out.println(twoAibi.toString());
					  System.out.println(threeAibi.toString());
					    	  System.out.println(fourAibi.toString());
					  
					    	
					  
					  // AIBI END HERE
				  
					  	int PROCUREMENTCount = 0;
					  	
						try {
							PROCUREMENTCount  = jira.passPgsCount(PROCUREMENTGROUP);
							}
						catch (JSONException e1) {
							e1.printStackTrace();
						}
						
							System.out.println("PROCUREMENT Group Count is "+PROCUREMENTCount);
							while(o<PROCUREMENTCount)
							{
							  try {
								  jira.PROCUREMENTRun(o);
								  jira.PROCUREMENTActive(o);
						} catch (JSONException e) {
							e.printStackTrace();
						}
							  	o= o + 50;	
							}
						
						  PROCUREMENTData= jira.getPROCUREMENTData();
						  ArrayList<String> oneProcurement = new ArrayList<>();
						  ArrayList<String> twoProcurement = new ArrayList<>();
						  ArrayList<String> threeProcurement = new ArrayList<>();
						  ArrayList<Boolean> fourProcurement = new ArrayList<>();
						 
						  oneProcurement = PROCUREMENTData.get(0);
						  twoProcurement = PROCUREMENTData.get(1);
						  threeProcurement = PROCUREMENTData.get(2);
						  
						  PROCUREMENTActiveCall = jira.getPROCUREMENTActive();
						  fourProcurement = PROCUREMENTActiveCall.get(0);
						  
						  System.out.println(oneProcurement.toString());
						  System.out.println(twoProcurement.toString());
						  System.out.println(threeProcurement.toString());
						  System.out.println(fourProcurement.toString());
						  
			
						  // PROCUREMENT END HERE
						  
						  
						  int DSECount = 0;
						  	
							try {
								DSECount  = jira.passPgsCount(DSEGROUP);
								}
							catch (JSONException e1) {
								e1.printStackTrace();
							}
							
								System.out.println("DSE Group Count is "+DSECount);
								while(p<DSECount)
								{
								  try {
									  jira.DSERun(p);
									  jira.DSEActive(p);
							} catch (JSONException e) {
								e.printStackTrace();
							}
								  	p= p + 50;	
								}
							
								DSEData= jira.getDSEData();
							  ArrayList<String> oneDse = new ArrayList<>();
							  ArrayList<String> twoDse = new ArrayList<>();
							  ArrayList<String> threeDse = new ArrayList<>();
							  ArrayList<Boolean> fourDse = new ArrayList<>();
							 
							  oneDse = DSEData.get(0);
							  twoDse = DSEData.get(1);
							  threeDse = DSEData.get(2);
							  
							  DSEActiveCall = jira.getDSEActive();
							  fourDse = DSEActiveCall.get(0);
							  
							  System.out.println(oneDse.toString());
							  System.out.println(twoDse.toString());
							  System.out.println(threeDse.toString());
							    	  System.out.println(fourDse.toString());
							  
									
							  // DSE-COMMERICAL END HERE
									  
						int ARCHITECTURECount = 0;
									  	
						try {
						ARCHITECTURECount  = jira.passPgsCount(ARCHITECTUREGROUP);
							}
							catch (JSONException e1) {
								e1.printStackTrace();
								}
										
						System.out.println("ARCHITECTURE Group Count is "+ARCHITECTURECount);
						while(q<ARCHITECTURECount)
						{
							try {
							jira.ARCHITECTURERun(q);
							jira.ARCHITECTUREActive(q);
							} catch (JSONException e) {
							e.printStackTrace();
							}
								q= q + 50;	
								}
										
						ARCHITECTUREData= jira.getARCHITECTUREData();
							ArrayList<String> oneArchitecture = new ArrayList<>();
							ArrayList<String> twoArchitecture = new ArrayList<>();
							ArrayList<String> threeArchitecture = new ArrayList<>();
							ArrayList<Boolean> fourArchitecture = new ArrayList<>();
										 
							oneArchitecture = ARCHITECTUREData.get(0);
							twoArchitecture = ARCHITECTUREData.get(1);
							threeArchitecture = ARCHITECTUREData.get(2);
										  
							ARCHITECTUREActiveCall = jira.getARCHITECTUREActive();
							fourArchitecture = ARCHITECTUREActiveCall.get(0);
										  
							System.out.println(oneArchitecture.toString());
							System.out.println(twoArchitecture.toString());
							System.out.println(threeArchitecture.toString());
							System.out.println(fourArchitecture.toString());
										  
												
							// ARCHITECTURE END HERE

							int HRCFSCount = 0;
						  	
							try {
								HRCFSCount  = jira.passPgsCount(HRCFSGROUP);
								}
								catch (JSONException e1) {
									e1.printStackTrace();
									}
											
							System.out.println("HRCFS Group Count is "+HRCFSCount);
							while(r<HRCFSCount)
							{
								try {
								jira.HRCFSRun(r);
								jira.HRCFSActive(r);
								} catch (JSONException e) {
								e.printStackTrace();
								}
									r= r + 50;	
									}
											
							HRCFSData= jira.getHRCFSData();
								ArrayList<String> oneHrcfs = new ArrayList<>();
								ArrayList<String> twoHrcfs = new ArrayList<>();
								ArrayList<String> threeHrcfs = new ArrayList<>();
								ArrayList<Boolean> fourHrcfs = new ArrayList<>();
											 
								oneHrcfs = HRCFSData.get(0);
								twoHrcfs = HRCFSData.get(1);
								threeHrcfs = HRCFSData.get(2);
											  
								HRCFSActiveCall = jira.getHRCFSActive();
								fourHrcfs = HRCFSActiveCall.get(0);
											  
								System.out.println(oneHrcfs.toString());
								System.out.println(twoHrcfs.toString());
								System.out.println(threeHrcfs.toString());
								System.out.println(fourHrcfs.toString());
											  
													
								// HRCFS END HERE
		
								int GPDCount = 0;
							  	
								try {
									GPDCount  = jira.passPgsCount(GPDGROUP);
									}
									catch (JSONException e1) {
										e1.printStackTrace();
										}
												
								System.out.println("GPD Group Count is "+GPDCount);
								while(s<GPDCount)
								{
									try {
									jira.GPDRun(s);
									jira.GPDActive(s);
									} catch (JSONException e) {
									e.printStackTrace();
									}
										s= s + 50;	
										}
												
								GPDData= jira.getGPDData();
									ArrayList<String> oneGpd = new ArrayList<>();
									ArrayList<String> twoGpd = new ArrayList<>();
									ArrayList<String> threeGpd = new ArrayList<>();
									ArrayList<Boolean> fourGpd = new ArrayList<>();
												 
									oneGpd = GPDData.get(0);
									twoGpd = GPDData.get(1);
									threeGpd = GPDData.get(2);
												  
									GPDActiveCall = jira.getGPDActive();
									fourGpd= GPDActiveCall.get(0);
												  
									System.out.println(oneGpd.toString());
									System.out.println(twoGpd.toString());
									System.out.println(threeGpd.toString());
									System.out.println(fourGpd.toString());
												  
														
									// GPD END HERE
									
									int ECSCount = 0;
								  	
									try {
										ECSCount  = jira.passPgsCount(ECSGROUP);
										}
										catch (JSONException e1) {
											e1.printStackTrace();
											}
													
									System.out.println("ECS Group Count is "+ECSCount);
									while(t<ECSCount)
									{
										try {
										jira.ECSRun(t);
										jira.ECSActive(t);
										} catch (JSONException e) {
										e.printStackTrace();
										}
											t= t + 50;	
											}
													
									ECSData= jira.getECSData();
										ArrayList<String> oneEcs = new ArrayList<>();
										ArrayList<String> twoEcs = new ArrayList<>();
										ArrayList<String> threeEcs = new ArrayList<>();
										ArrayList<Boolean> fourEcs = new ArrayList<>();
													 
										oneEcs = ECSData.get(0);
										twoEcs = ECSData.get(1);
										threeEcs = ECSData.get(2);
													  
										ECSActiveCall = jira.getECSActive();
										fourEcs = ECSActiveCall.get(0);
													  
										System.out.println(oneEcs.toString());
										System.out.println(twoEcs.toString());
										System.out.println(threeEcs.toString());
										System.out.println(fourEcs.toString());
													  
															
										// ECS END HERE
			
							
							  ArrayList<String> AllinOneUsername = new ArrayList<>();
							  AllinOneUsername.addAll(one);
							  AllinOneUsername.addAll(oneRnd);
							  AllinOneUsername.addAll(oneGis);
							  AllinOneUsername.addAll(oneRrems);
							  AllinOneUsername.addAll(oneFbo);
							  AllinOneUsername.addAll(oneCppm);
							  AllinOneUsername.addAll(oneAibi);
							  AllinOneUsername.addAll(oneProcurement);
							  AllinOneUsername.addAll(oneDse);
							  AllinOneUsername.addAll(oneArchitecture);
							  AllinOneUsername.addAll(oneHrcfs);
							  AllinOneUsername.addAll(oneGpd);
							  AllinOneUsername.addAll(oneEcs);

					
							  ArrayList<String> PGSLogin = new ArrayList<>();
							  ArrayList<String> RNDLogin = new ArrayList<>();
							  ArrayList<String> GISLogin = new ArrayList<>();
							  ArrayList<String> RREMSLogin = new ArrayList<>();
							  ArrayList<String> FBOLogin = new ArrayList<>();
							  ArrayList<String> CPPMLogin = new ArrayList<>();
							  ArrayList<String> AIBILogin = new ArrayList<>();
							  ArrayList<String> PROCUREMENTLogin = new ArrayList<>();
							  ArrayList<String> DSELogin = new ArrayList<>();
							  ArrayList<String> ARCHITECTURELogin = new ArrayList<>();
							  ArrayList<String> HRCFSLogin = new ArrayList<>();
							  ArrayList<String> GPDLogin = new ArrayList<>();
							  ArrayList<String> ECSLogin = new ArrayList<>();
							

							  
							  ArrayList<ArrayList<String>> callForAll = new ArrayList<>();
							 	Connection con =null;
						    	
						     	 try {
						    		con = DriverManager.getConnection(url, userNameDB, passwordDB);
						    	

						    		 callForAll = jira.getLoginFromDB(con, one, oneRnd, oneGis,oneRrems,oneFbo, oneCppm, oneAibi, oneProcurement, oneDse , oneArchitecture, oneHrcfs, oneGpd, oneEcs, AllinOneUsername);
						    	} catch (SQLException e2) {
						    		// TODO Auto-generated catch block
						    		e2.printStackTrace();
						    	}
						     	  ArrayList<String> AllUserName = new ArrayList<>();
						     	  ArrayList<String> AllFullName = new ArrayList<>();
						     	  ArrayList<String> AllEmailAddress = new ArrayList<>();
						     	
						     	  ArrayList<String> AllLogin = new ArrayList<>();
						     	  
						     	 PGSLogin = callForAll.get(0);
						     	 RNDLogin = callForAll.get(1);
						     	 GISLogin = callForAll.get(2);
						     	 RREMSLogin = callForAll.get(3);
						     	 FBOLogin = callForAll.get(4);
						     	 CPPMLogin = callForAll.get(5);
						     	 AIBILogin = callForAll.get(6);
						     	 PROCUREMENTLogin = callForAll.get(7);
						     	 DSELogin = callForAll.get(8);
						     	 ARCHITECTURELogin = callForAll.get(9);
						     	 HRCFSLogin = callForAll.get(10);
						     	 GPDLogin = callForAll.get(11);
						     	 ECSLogin = callForAll.get(12);
						     	
						     	AllUserName = callForAll.get(13);
						     	AllFullName = callForAll.get(14);
						     	AllEmailAddress = callForAll.get(15);
						     
						     	AllLogin = callForAll.get(16);
						     	
						     	ArrayList<ArrayList<Boolean>> CallForAllFlag = new ArrayList<>();
						     	try {
									CallForAllFlag= jira.getBooleanList(con, AllinOneUsername);
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
						     	  ArrayList<Boolean> AllFlag= new ArrayList<>();
						     	  
						     	  AllFlag = CallForAllFlag.get(0);
						     	 
//						     	 ArrayList<String> AllinOneLogin = new ArrayList<>();
//						     	 AllinOneLogin.addAll(PGSLogin);
//						     	 AllinOneLogin.addAll(GISLogin);
//						     	 AllinOneLogin.addAll(RREMSLogin);
//						     	 AllinOneLogin.addAll(FBOLogin);
//						     	 AllinOneLogin.addAll(CPPMLogin);
//						     	 AllinOneLogin.addAll(AIBILogin);
//						     	 AllinOneLogin.addAll(PROCUREMENTLogin);
//						     	 AllinOneLogin.addAll(DSELogin);
//						     	 AllinOneLogin.addAll(FullLogin);
						     	 
	jira.writeSummary(SUMMARYSHEET,one, oneRnd, oneGis, oneRrems, oneFbo, oneCppm, oneAibi, oneProcurement, oneDse, oneArchitecture, oneHrcfs, oneGpd, oneEcs, SUMMARYTEXT);
						     	 
						     	 
						     	 try {
						      		  	jira.WriteData(PGSSheet, one, two, three, four,PGSLogin, PGSTEXT);
						      		  } catch (ParserConfigurationException e) {
						    			e.printStackTrace();
						      		  }
						    	
			     	 try {
		      		  	jira.WriteData(RNDSheet, oneRnd, twoRnd, threeRnd, fourRnd,RNDLogin, RNDTEXT);
		      		  } catch (ParserConfigurationException e) {
		    			e.printStackTrace();
		      		  }
						     	 
						     	 
						  try {
							  	jira.WriteData(GISSheet, oneGis, twoGis, threeGis, fourGis, GISLogin, GISTEXT);
							  } catch (ParserConfigurationException e) {
							e.printStackTrace();
							  }
						 
						  try {
							  	jira.WriteData(RREMSSheet, oneRrems, twoRrems, threeRrems, fourRrems, RREMSLogin, RREMSTEXT);
							  } catch (ParserConfigurationException e) {
							e.printStackTrace();
							  }
					  try {
						  	jira.WriteData(FBOSheet, oneFbo, twoFbo, threeFbo, fourFbo, FBOLogin, FBOTEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
					  try {
						  	jira.WriteData(CPPMSheet, oneCppm, twoCppm, threeCppm, fourCppm, CPPMLogin, CPPMTEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
					  try {
						  	jira.WriteData(AIBISheet, oneAibi, twoAibi, threeAibi, fourAibi, AIBILogin, AIBITEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
							  try {
								  	jira.WriteData(PROCUREMENTSheet, oneProcurement, twoProcurement, threeProcurement, fourProcurement, PROCUREMENTLogin, PROCUREMENTTEXT);
								  } catch (ParserConfigurationException e) {
								e.printStackTrace();
								  }
							  
					  try {
						  	jira.WriteData(DSESheet, oneDse, twoDse, threeDse, fourDse, DSELogin, DSETEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
					  try {
						  	jira.WriteData(ARCHITECTURESheet, oneArchitecture, twoArchitecture, threeArchitecture, fourArchitecture, ARCHITECTURELogin, ARCHITECTURETEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
		
					  try {
						  	jira.WriteData(HRCFSSheet, oneHrcfs, twoHrcfs, threeHrcfs, fourHrcfs, HRCFSLogin, HRCFSTEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
					  try {
						  	jira.WriteData(GPDSheet, oneGpd, twoGpd, threeGpd, fourGpd, GPDLogin, GPDTEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
					  try {
						  	jira.WriteData(ECSSheet, oneEcs, twoEcs, threeEcs, fourEcs, ECSLogin, ECSTEXT);
						  } catch (ParserConfigurationException e) {
						e.printStackTrace();
						  }
					  
		try {
			jira.WriteData(FULLSHEET, AllUserName, AllFullName, AllEmailAddress, AllFlag, AllLogin,  FULLTEXT);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			

		String filename = "Jira_org_report.xlsx";
//		String outputFile= "D:\\Users\\"+filename;	
		String outputFile= "/usr/local/Enhanced/reports/"+filename;
//  		
  		File file = new File(outputFile);

		if(file.exists())
		{
			//			Desktop desktop = Desktop.getDesktop();
			//			desktop.open(file);
			//			InputStream in = new URL(f).openStream();
			//			Files.copy(in, Paths.get(f));


			response.setContentType("application/vnd.ms-excel");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
			System.out.println("filename new  "+filename);
			
			BufferedInputStream in=null;
			ServletOutputStream outs=null;
			byte[] buffer = new byte[1024]; 
		
			
			try {
				int g = 0;
				in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
				outs=response.getOutputStream();
				while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
					outs.write(buffer, 0, g);  
				}  
				outs.flush();  

			} catch (IOException ioe) {
				ioe.printStackTrace(System.out);
			}

			System.out.println("Done !!");

			outs.close();
			in.close();
			
		}
		else
		{
			System.out.println("not found");
		}
}
		
	public void writeSummary(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six, ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine, ArrayList<String> ten, ArrayList<String> eleven, ArrayList<String> twelve, ArrayList<String> thirteen, String text) throws IOException
	{
		//XSSFSheet newwSheet = newSheet;
		newSheet = newWorkbook.createSheet(text);
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		System.out.println("From summary tab");
		Row headerRow = newSheet.createRow(0);
		
			Cell headerCell1 = headerRow.createCell(0);	
			Cell headerCell2 = headerRow.createCell(1);	
			Cell headerCell3 = headerRow.createCell(2);	


			headerCell1.setCellValue("Business Line");	
			headerCell1.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(0);
			headerCell2.setCellValue("Count"); 
			headerCell2.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(1);
			headerCell3.setCellValue("($)Dollar"); 
			headerCell3.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(2);

			//
			Row row1 = newSheet.createRow(1);
			Cell PGS1 = row1.createCell(0);
			Cell PGS2 = row1.createCell(1);
			PGS1.setCellValue("PGS");
			PGS2.setCellValue(one.size());
			
			Row row2 = newSheet.createRow(2);
			Cell RND1 = row2.createCell(0);
			Cell RND2 = row2.createCell(1);
			RND1.setCellValue("RND");
			RND2.setCellValue(two.size());

			
			Row row3 = newSheet.createRow(3);
			Cell GIS1 = row3.createCell(0);
			Cell GIS2 = row3.createCell(1);
			GIS1.setCellValue("GIS");
			GIS2.setCellValue(three.size());
			
			Row row4 = newSheet.createRow(4);
			Cell RREMS1 = row4.createCell(0);
			Cell RREMS2 = row4.createCell(1);
			RREMS1.setCellValue("RREMS");
			RREMS2.setCellValue(four.size());
			
			Row row5 = newSheet.createRow(5);
			Cell FBO1 = row5.createCell(0);
			Cell FBO2 = row5.createCell(1);
			FBO1.setCellValue("FBO");
			FBO2.setCellValue(five.size());
			
			Row row6 = newSheet.createRow(6);
			Cell CPPM1 = row6.createCell(0);
			Cell CPPM2 = row6.createCell(1);
			CPPM1.setCellValue("CPPM");
			CPPM2.setCellValue(six.size());

			Row row7 = newSheet.createRow(7);
			Cell AIBI1 = row7.createCell(0);
			Cell AIBI2 = row7.createCell(1);
			AIBI1.setCellValue("AIBI");
			AIBI2.setCellValue(seven.size());
			
			Row row8 = newSheet.createRow(8);
			Cell PROCUREMENT1 = row8.createCell(0);
			Cell PROCUREMENT2 = row8.createCell(1);
			PROCUREMENT1.setCellValue("PROCUREMENT");
			PROCUREMENT2.setCellValue(eight.size());
			
			Row row9 = newSheet.createRow(9);
			Cell DSE1 = row9.createCell(0);
			Cell DSE2 = row9.createCell(1);
			DSE1.setCellValue("DSE-Comerrcial");
			DSE2.setCellValue(nine.size());
			
			Row row10 = newSheet.createRow(10);
			Cell ARCHITECTURE1 = row10.createCell(0);
			Cell ARCHITECTURE2 = row10.createCell(1);
			ARCHITECTURE1.setCellValue("ARCHITECTURE");
			ARCHITECTURE2.setCellValue(ten.size());
			
			Row row11 = newSheet.createRow(11);
			Cell HRCFS1 = row11.createCell(0);
			Cell HRCFS2 = row11.createCell(1);
			HRCFS1.setCellValue("HRCFS");
			HRCFS2.setCellValue(eleven.size());
			
			Row row12 = newSheet.createRow(12);
			Cell GPD1 = row12.createCell(0);
			Cell GPD2 = row12.createCell(1);
			GPD1.setCellValue("GPD");
			GPD2.setCellValue(twelve.size());
			
			Row row13 = newSheet.createRow(13);
			Cell ECS1 = row13.createCell(0);
			Cell ECS2 = row13.createCell(1);
			ECS1.setCellValue("ECS");
			ECS2.setCellValue(thirteen.size());
			
			System.out.println(one);
			
	}
						    	
	
	public int passPgsCount(String groupName) throws JSONException, IOException
	{
		String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName; 		
		  
    	URL url = new URL(api);
    	TrustAllCertificates.install();
    	HttpURLConnection conn = null;
    	conn = (HttpURLConnection) url.openConnection();
    	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
    	String inputLine = "";
    	BufferedReader in = null;
    	System.out.println("Authentication process");
    	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    	StringBuffer response = new StringBuffer();
    	while((inputLine = in.readLine()) != null)
    	{
    		response.append(inputLine);
    	}
			in.close();
		  
    	JSONObject json  = new JSONObject(response.toString());
    	System.out.println("From Main method "+json.getInt("total"));
		
    	int PGSCount= 0;
		try {
			PGSCount = json.getInt("total");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println("PGS count is "+PGSCount);
		return PGSCount;	
	}
	
 	
 	public void PGSRun(int startAt) throws IOException, JSONException  {
		
		String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodpgs&startAt="+startAt; 				    	  
		URL url = new URL(api);
   	  	TrustAllCertificates.install();
   	  	HttpURLConnection conn = null;
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
		String inputLine = "";
		BufferedReader in = null;
   		System.out.println("Authentication process");
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
   	  	StringBuffer response = new StringBuffer();
   	  	while((inputLine = in.readLine()) != null)
   	  	{
   		  response.append(inputLine);
   	  	}
		in.close();
			    	  
        JSONObject json  = new JSONObject(response.toString());
        JSONArray innerJsonArray = json.getJSONArray("values");
           	    	  
        for(int i=0; i<innerJsonArray.length(); i++)
        { 
        	    		  
        	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
        	PGSDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
        	PGSDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
        	PGSDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
        	
        }   	    	 
	}
 	
 	private ArrayList<ArrayList<String>> getPGSData()
 	{
 		PGSDataList.add(PGSDataListUserName);
 		PGSDataList.add(PGSDataListDisplayName);
 		PGSDataList.add(PGSDataListEmailAddress);
 		//PGSDataList.add(PGSDataListFlag);
 		
		return PGSDataList;
 	}
 	
public void PGSActive(int startAt) throws IOException, JSONException  {
		
		String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodpgs&startAt="+startAt; 				    	  
		URL url = new URL(api);
   	  	TrustAllCertificates.install();
   	  	HttpURLConnection conn = null;
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
		String inputLine = "";
		BufferedReader in = null;
   		System.out.println("Authentication process");
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
   	  	StringBuffer response = new StringBuffer();
   	  	while((inputLine = in.readLine()) != null)
   	  	{
   		  response.append(inputLine);
   	  	}
		in.close();
			    	  
        JSONObject json  = new JSONObject(response.toString());
        JSONArray innerJsonArray = json.getJSONArray("values");
           	    	  
        for(int i=0; i<innerJsonArray.length(); i++)
        { 
        	    		  
        	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
        	
        	PGSDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
        	
        }   	    	 
	}
public ArrayList<ArrayList<Boolean>> getPGSActive()
	{
	PGSActive.add(PGSDataListFlag);
	System.out.println("Flag In Method "+PGSActive.get(0).toString());
	return PGSActive;
	}

//---------------------------------------------------------RND-------------------------------------------------------------------------------------

public int passRndCount(String groupName) throws JSONException, IOException
{
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname="+groupName; 		
	  
	URL url = new URL(api);
	TrustAllCertificates.install();
	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
	System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	StringBuffer response = new StringBuffer();
	while((inputLine = in.readLine()) != null)
	{
		response.append(inputLine);
	}
		in.close();
	  
	JSONObject json  = new JSONObject(response.toString());
	System.out.println("From Main method "+json.getInt("total"));
	
	int RNDCount= 0;
	try {
		RNDCount = json.getInt("total");
	} catch (JSONException e) {
		e.printStackTrace();
	}
	System.out.println("RND count is "+RNDCount);
	return RNDCount;	
}

	
	public void RNDRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodrnd&startAt="+startAt; 				    	  
	URL url = new URL(api);
//	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
    JSONObject json  = new JSONObject(response.toString());
    JSONArray innerJsonArray = json.getJSONArray("values");
       	    	  
    for(int i=0; i<innerJsonArray.length(); i++)
    { 
    	    		  
    	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
    	RNDDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
    	RNDDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
    	RNDDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
    	
    }   	    	 
}
	
	private ArrayList<ArrayList<String>> getRNDData()
	{
		RNDDataList.add(RNDDataListUserName);
		RNDDataList.add(RNDDataListDisplayName);
		RNDDataList.add(RNDDataListEmailAddress);
		//RNDDataList.add(RNDDataListFlag);
		
	return RNDDataList;
	}
	
public void RNDActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodrnd&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
    JSONObject json  = new JSONObject(response.toString());
    JSONArray innerJsonArray = json.getJSONArray("values");
       	    	  
    for(int i=0; i<innerJsonArray.length(); i++)
    { 
    	    		  
    	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
    	
    	RNDDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
    	
    }   	    	 
}
public ArrayList<ArrayList<Boolean>> getRNDActive()
{
	RNDActive.add(RNDDataListFlag);
System.out.println("Flag In Method "+RNDActive.get(0).toString());
return RNDActive;
}

//---------------------------------------------------------------GIS-----------------------------------------------------------------------//
	
	public void GISRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprod&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
    JSONObject json  = new JSONObject(response.toString());
    JSONArray innerJsonArray = json.getJSONArray("values");
       	    	  
    for(int i=0; i<innerJsonArray.length(); i++)
    { 
    	    		  
    	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
    	GISDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
    	GISDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
    	GISDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
    	
    }   	    	 
}
	
	public ArrayList<ArrayList<String>> getGISData()
	{
		GISDataList.add(GISDataListUserName);
		GISDataList.add(GISDataListDisplayName);
		GISDataList.add(GISDataListEmailAddress);
		//GISDataList.add(GISDataListFlag);
		
	return GISDataList;
	}
	
public void GISActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprod&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
    JSONObject json  = new JSONObject(response.toString());
    JSONArray innerJsonArray = json.getJSONArray("values");
       	    	  
    for(int i=0; i<innerJsonArray.length(); i++)
    { 
    	    		  
    	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
    	
    	GISDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
    	
    }   	    	 
}
public ArrayList<ArrayList<Boolean>> getGISActive()
{
GISActive.add(GISDataListFlag);
System.out.println("Flag In Method "+GISActive.get(0).toString());
return GISActive;
}


//---------------------------------------------------------------RREMS-----------------------------------------------------------------------//

	public void RREMSRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodrrems&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
  JSONObject json  = new JSONObject(response.toString());
  JSONArray innerJsonArray = json.getJSONArray("values");
     	    	  
  for(int i=0; i<innerJsonArray.length(); i++)
  { 
  	    		  
  	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	RREMSDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	RREMSDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	RREMSDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
  	
  }   	    	 
}
	
	public ArrayList<ArrayList<String>> getRREMSData()
	{
		RREMSDataList.add(RREMSDataListUserName);
		RREMSDataList.add(RREMSDataListDisplayName);
		RREMSDataList.add(RREMSDataListEmailAddress);
		//RREMSDataList.add(RREMSDataListFlag);
		
	return RREMSDataList;
	}
	
public void RREMSActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodrrems&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
  JSONObject json  = new JSONObject(response.toString());
  JSONArray innerJsonArray = json.getJSONArray("values");
     	    	  
  for(int i=0; i<innerJsonArray.length(); i++)
  { 
  	    		  
  	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
  	
	  RREMSDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
  	
  }   	    	 
}
public ArrayList<ArrayList<Boolean>> getRREMSActive()
{
	RREMSActive.add(RREMSDataListFlag);
System.out.println("Flag In Method "+RREMSActive.get(0).toString());
return RREMSActive;
}

//---------------------------------------------------------------FBO-----------------------------------------------------------------------//

	public void FBORun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodfbo&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
   	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	FBODataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	FBODataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	FBODataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getFBOData()
	{
		FBODataList.add(FBODataListUserName);
		FBODataList.add(FBODataListDisplayName);
		FBODataList.add(FBODataListEmailAddress);
		//FBODataList.add(FBODataListFlag);
		
	return FBODataList;
	}
	
public void FBOActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodfbo&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
   	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
		FBODataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getFBOActive()
{
	FBOActive.add(FBODataListFlag);
System.out.println("Flag In Method "+FBOActive.get(0).toString());
return FBOActive;
}


//---------------------------------------------------------------CPPM-----------------------------------------------------------------------//

	public void CPPMRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodcppm&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
 	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	CPPMDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	CPPMDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	CPPMDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getCPPMData()
	{
		CPPMDataList.add(CPPMDataListUserName);
		CPPMDataList.add(CPPMDataListDisplayName);
		CPPMDataList.add(CPPMDataListEmailAddress);
		//CPPMDataList.add(CPPMDataListFlag);
		
	return CPPMDataList;
	}
	
public void CPPMActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodcppm&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
 	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	CPPMDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getCPPMActive()
{
	CPPMActive.add(CPPMDataListFlag);
System.out.println("Flag In Method "+CPPMActive.get(0).toString());
return CPPMActive;
}

//---------------------------------------------------------------AIBI-----------------------------------------------------------------------//

	public void AIBIRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodaibi&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	AIBIDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	AIBIDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	AIBIDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getAIBIData()
	{
		AIBIDataList.add(AIBIDataListUserName);
		AIBIDataList.add(AIBIDataListDisplayName);
		AIBIDataList.add(AIBIDataListEmailAddress);
		//AIBIDataList.add(AIBIDataListFlag);
		
	return AIBIDataList;
	}
	
public void AIBIActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodaibi&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	AIBIDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getAIBIActive()
{
	AIBIActive.add(AIBIDataListFlag);
System.out.println("Flag In Method "+AIBIActive.get(0).toString());
return AIBIActive;
}


//---------------------------------------------------------------PROCUREMENT-----------------------------------------------------------------------//

	public void PROCUREMENTRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodprocurement&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	PROCUREMENTDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	PROCUREMENTDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	PROCUREMENTDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getPROCUREMENTData()
	{
		PROCUREMENTDataList.add(PROCUREMENTDataListUserName);
		PROCUREMENTDataList.add(PROCUREMENTDataListDisplayName);
		PROCUREMENTDataList.add(PROCUREMENTDataListEmailAddress);
		//PROCUREMENTDataList.add(PROCUREMENTDataListFlag);
		
	return PROCUREMENTDataList;
	}
	
public void PROCUREMENTActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodprocurement&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	PROCUREMENTDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getPROCUREMENTActive()
{
	PROCUREMENTActive.add(PROCUREMENTDataListFlag);
System.out.println("Flag In Method "+PROCUREMENTActive.get(0).toString());
return PROCUREMENTActive;
}


//---------------------------------------------------------------DSE-----------------------------------------------------------------------//

	public void DSERun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraproddse-commercial&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	DSEDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	DSEDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	DSEDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getDSEData()
	{
		DSEDataList.add(DSEDataListUserName);
		DSEDataList.add(DSEDataListDisplayName);
		DSEDataList.add(DSEDataListEmailAddress);
		//DSEDataList.add(DSEDataListFlag);
		
	return DSEDataList;
	}
	
public void DSEActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraproddse-commercial&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	DSEDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getDSEActive()
{
	DSEActive.add(DSEDataListFlag);
System.out.println("Flag In Method "+DSEActive.get(0).toString());
return DSEActive;
}


//---------------------------------------------------------------ARCHITECTURE-----------------------------------------------------------------------//

	public void ARCHITECTURERun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodarchitecture&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	ARCHITECTUREDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	ARCHITECTUREDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	ARCHITECTUREDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getARCHITECTUREData()
	{
		ARCHITECTUREDataList.add(ARCHITECTUREDataListUserName);
		ARCHITECTUREDataList.add(ARCHITECTUREDataListDisplayName);
		ARCHITECTUREDataList.add(ARCHITECTUREDataListEmailAddress);
		//ARCHITECTUREDataList.add(ARCHITECTUREDataListFlag);
		
	return ARCHITECTUREDataList;
	}
	
public void ARCHITECTUREActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodarchitecture&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	ARCHITECTUREDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getARCHITECTUREActive()
{
	ARCHITECTUREActive.add(ARCHITECTUREDataListFlag);
System.out.println("Flag In Method "+ARCHITECTUREActive.get(0).toString());
return ARCHITECTUREActive;
}


//---------------------------------------------------------------HRCFS-----------------------------------------------------------------------//

	public void HRCFSRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodhrcfs&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	HRCFSDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	HRCFSDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	HRCFSDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getHRCFSData()
	{
		HRCFSDataList.add(HRCFSDataListUserName);
		HRCFSDataList.add(HRCFSDataListDisplayName);
		HRCFSDataList.add(HRCFSDataListEmailAddress);
		//HRCFSDataList.add(HRCFSDataListFlag);
		
	return HRCFSDataList;
	}
	
public void HRCFSActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodhrcfs&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	HRCFSDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getHRCFSActive()
{
	HRCFSActive.add(HRCFSDataListFlag);
System.out.println("Flag In Method "+HRCFSActive.get(0).toString());
return HRCFSActive;
}



//---------------------------------------------------------------GPD-----------------------------------------------------------------------//

	public void GPDRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodgpd&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	GPDDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	GPDDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	GPDDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getGPDData()
	{
		GPDDataList.add(GPDDataListUserName);
		GPDDataList.add(GPDDataListDisplayName);
		GPDDataList.add(GPDDataListEmailAddress);
		//GPDDataList.add(GPDDataListFlag);
		
	return GPDDataList;
	}
	
public void GPDActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodgpd&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	GPDDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getGPDActive()
{
	GPDActive.add(GPDDataListFlag);
System.out.println("Flag In Method "+GPDActive.get(0).toString());
return GPDActive;
}


//---------------------------------------------------------------ECS-----------------------------------------------------------------------//

	public void ECSRun(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodecs&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getString("name")); 
	ECSDataListUserName.add(innerJsonArray.getJSONObject(i).getString("name"));
	ECSDataListDisplayName.add(innerJsonArray.getJSONObject(i).getString("displayName"));
	ECSDataListEmailAddress.add(innerJsonArray.getJSONObject(i).getString("emailAddress"));
	
}   	    	 
}
	
	public ArrayList<ArrayList<String>> getECSData()
	{
		ECSDataList.add(ECSDataListUserName);
		ECSDataList.add(ECSDataListDisplayName);
		ECSDataList.add(ECSDataListEmailAddress);
		//ECSDataList.add(ECSDataListFlag);
		
	return ECSDataList;
	}
	
public void ECSActive(int startAt) throws IOException, JSONException  {
	
	String api = "http://muleapi-ent-amer.pfizer.com/jira-v1/rest/api/2/group/member?groupname=jiraprodecs&startAt="+startAt; 				    	  
	URL url = new URL(api);
	  	TrustAllCertificates.install();
	  	HttpURLConnection conn = null;
	conn = (HttpURLConnection) url.openConnection();
	conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
	String inputLine = "";
	BufferedReader in = null;
		System.out.println("Authentication process");
	in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	  	StringBuffer response = new StringBuffer();
	  	while((inputLine = in.readLine()) != null)
	  	{
		  response.append(inputLine);
	  	}
	in.close();
		    	  
JSONObject json  = new JSONObject(response.toString());
JSONArray innerJsonArray = json.getJSONArray("values");
	    	  
for(int i=0; i<innerJsonArray.length(); i++)
{ 
	    		  
	//System.out.println("Names  "+innerJsonArray.getJSONObject(i).getBoolean("active")); 
	
	ECSDataListFlag.add(innerJsonArray.getJSONObject(i).getBoolean("active"));
	
}   	    	 
}
public ArrayList<ArrayList<Boolean>> getECSActive()
{
	ECSActive.add(ECSDataListFlag);
System.out.println("Flag In Method "+ECSActive.get(0).toString());
return ECSActive;
}


public void WriteData(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<Boolean> four, ArrayList<String> five, String PGSTEXT) throws IOException, ParserConfigurationException
 	{
 		String filename = "Jira_org_report.xlsx";
     	String outputFile= "/usr/local/Enhanced/reports/"+filename;
 	//	String outputFile= "D:\\Users\\"+filename;
 		newSheet = newWorkbook.createSheet(PGSTEXT);
 		DocumentBuilder db = dbf.newDocumentBuilder();

 		String  costCenter1=null, costCenterDescription2 = null, supervisorId3 = null, supervisorName4 = null, departmentLevel1Id5 = null, departmentLevel1Description6= null, departmentLevel2Id7 = null, departmentLevel2Description8 = null;
 		String departmentLevel3Id9 = null, departmentLevel3Description10 = null, departmentLevel4Id11 = null, departmentLevel4Description12 = null, departmentLevel5Id13 = null, departmentLevel5Description14 = null, departmentLevel6Id15 = null, departmentLevel6Description16 = null, activePfizer17 = null;

 		System.out.println("Sheet Created");
		String Columns[] = new String[] {"Username", "Full Name", "Email Address", "Cost Center", "Cost Center Description", "Supervisor Id", "Supervisor Name", "Department Level1 Id","Department Level1 Description", "Department Level2 Id", "Department Level2 Description", "Department Level3 Id", "Department Level3 Description", "Department Level4 Id", "Department Level4 Description", "Department Level5 Id", "Department Level5 Description", "Department Level6 Id", "Department Level6 Description", "Active in Pfizer", "Active in Jira", "Last Login"};     
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
			
		Row headerRow = newSheet.createRow(0);
			
		for(int columnI=0; columnI<Columns.length; columnI++)
		{ 
			Cell cell = headerRow.createCell(columnI);	
			cell.setCellValue(Columns[columnI]);	
			cell.setCellStyle(headerCellStyle); 
			newSheet.autoSizeColumn(columnI);	
		}
 		String input, url;
		URL urli;
		URLConnection conn; 
		Document doc = null;
		int fork = 1;
		 for(int ntid =0 ; ntid<one.size();ntid++)
		 {   
			input = one.get(ntid).toString();
			url = "http://researchservice.pfizer.com/ResearchServicePeople/person/ntid/";
			url = url.concat(input);
		    
		    System.out.println(ntid+" Running");
		    System.out.println(input);
			Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {          
				return new PasswordAuthentication(Researchlogin, Researchpass.toCharArray());
			}
				});
			
			 try {
		    	urli = new URL(url);
		    	conn = urli.openConnection();
		    	conn.setReadTimeout(50000);
		    	conn.setRequestProperty("Accept-Encoding", "gzip");
		    	conn.setRequestProperty("Accept", "application/xml");
		        conn.setRequestProperty("Content-Type", "application/xml");
		        conn.setUseCaches(true);
		        
					try {
						doc = db.parse(conn.getInputStream());
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        
		        activePfizer17 = doc.getElementsByTagName("activeIndicator").item(0).getTextContent();

		        Node n = doc.getElementsByTagName("organization").item(0);
		        NodeList nl = n.getChildNodes();
		        Node an = null;    	
		   	         
		        for (int i=1; i < nl.getLength(); i++) {
		        	an = nl.item(i);
		        	
		            if(Objects.equals("costCenter", an.getNodeName())) 
		            {
		            	
		            	costCenter1 = an.getChildNodes().item(0).getTextContent();
		            	//System.out.println(costCenter1);
		            }
		            
		            if(Objects.equals("costCenterDescription", an.getNodeName())) 
		            {        	
		            	costCenterDescription2 = an.getChildNodes().item(0).getTextContent();			
		            }
		            
		            if(Objects.equals("supervisorId", an.getNodeName())) 
		            {            		
		            	supervisorId3 = an.getChildNodes().item(0).getTextContent(); 					
		            }
		            
		            if(Objects.equals("supervisorName", an.getNodeName())) 
		            {            	
		            	supervisorName4 = an.getChildNodes().item(0).getTextContent();					
		            }
		            
		            if(Objects.equals("departmentLevel1Id", an.getNodeName())) 
		            {			
		            	departmentLevel1Id5 = an.getChildNodes().item(0).getTextContent();				
		            }            
		            
		            if(Objects.equals("departmentLevel1Description", an.getNodeName())) 
		            {	
		            	departmentLevel1Description6 = an.getChildNodes().item(0).getTextContent();		
		            }
		            
		            if(Objects.equals("departmentLevel2Id", an.getNodeName())) 
		            { 
		            	departmentLevel2Id7 = an.getChildNodes().item(0).getTextContent();				
		            }
		            
		            if(Objects.equals("departmentLevel2Description", an.getNodeName())) 
		            {
		            	departmentLevel2Description8 = an.getChildNodes().item(0).getTextContent();		
		            }
		            
		            if(Objects.equals("departmentLevel3Id", an.getNodeName()))
		            {			
		            	departmentLevel3Id9 = an.getChildNodes().item(0).getTextContent();
		            }    
		            
		            if(Objects.equals("departmentLevel3Description", an.getNodeName())) 
		            {
		            	departmentLevel3Description10 = an.getChildNodes().item(0).getTextContent();	
		            }
		            
		            if(Objects.equals("departmentLevel4Id", an.getNodeName()))
		            {            
		            	departmentLevel4Id11 = an.getChildNodes().item(0).getTextContent();				
		            }
		            
		            if(Objects.equals("departmentLevel4Description", an.getNodeName())) 
		            {	
		            	departmentLevel4Description12 = an.getChildNodes().item(0).getTextContent();	
		            }
		            
		            if(Objects.equals("departmentLevel5Id", an.getNodeName())) 
		            {        
		            	departmentLevel5Id13 = an.getChildNodes().item(0).getTextContent();				
		            }

		            if(Objects.equals("departmentLevel5Description", an.getNodeName())) 
		            {
		            	departmentLevel5Description14 = an.getChildNodes().item(0).getTextContent();	
		            }
		            if(Objects.equals("departmentLevel6Id", an.getNodeName())) 
		            {
		            	departmentLevel6Id15 = an.getChildNodes().item(0).getTextContent();				
		            }
		            
		            if(Objects.equals("departmentLevel6Description", an.getNodeName())) 
		            {
		            	departmentLevel6Description16 = an.getChildNodes().item(0).getTextContent();				
		            }
		            	     	
		    }
		        Row row = newSheet.createRow(fork);
		        fork++;
            	row.createCell(0).setCellValue(input);
            	row.createCell(1).setCellValue(two.get(ntid).toString());
            	row.createCell(2).setCellValue(three.get(ntid).toString());	
         		row.createCell(3).setCellValue(costCenter1);
         		row.createCell(4).setCellValue(costCenterDescription2);
         		row.createCell(5).setCellValue(supervisorId3);
         		row.createCell(6).setCellValue(supervisorName4);
         		row.createCell(7).setCellValue(departmentLevel1Id5);
         		row.createCell(8).setCellValue(departmentLevel1Description6);
         		row.createCell(9).setCellValue(departmentLevel2Id7);
         		row.createCell(10).setCellValue(departmentLevel2Description8);
         		row.createCell(11).setCellValue(departmentLevel3Id9);
         		row.createCell(12).setCellValue(departmentLevel3Description10);
         		row.createCell(13).setCellValue(departmentLevel4Id11);
         		row.createCell(14).setCellValue(departmentLevel4Description12);
         		row.createCell(15).setCellValue(departmentLevel5Id13);
          		row.createCell(16).setCellValue(departmentLevel5Description14);
          		row.createCell(17).setCellValue(departmentLevel6Id15);
         		row.createCell(18).setCellValue(departmentLevel6Description16);
         		row.createCell(19).setCellValue(activePfizer17);
         		row.createCell(20).setCellValue(four.get(ntid));
         		row.createCell(21).setCellValue(five.get(ntid));
                 	   
         		FileOutputStream oFile = new FileOutputStream(outputFile); 
         		newWorkbook.write(oFile);
         		
			 	
		 	costCenter1 = "";costCenterDescription2= ""; supervisorId3= ""; supervisorName4= ""; departmentLevel1Id5 = ""; departmentLevel1Description6 = "";  departmentLevel2Id7 = ""; departmentLevel2Description8 = ""; departmentLevel3Id9= "";  departmentLevel3Description10 = ""; departmentLevel4Id11= ""; departmentLevel4Description12=""; departmentLevel5Id13= ""; departmentLevel5Description14=""; departmentLevel6Id15 = ""; departmentLevel6Description16 = ""; activePfizer17="";
			 }
			 catch (Exception e) {

				  String blank = "N/A";
				    if(newSheet.getRow(fork) == null)
				    {
						System.out.println("null row at "+ntid);
						Row row = newSheet.createRow(fork);
						  fork++;
						
						row.createCell(0).setCellValue(input);
		         		row.createCell(1).setCellValue(two.get(ntid).toString());
		         		row.createCell(2).setCellValue(three.get(ntid).toString());
		         		row.createCell(3).setCellValue(blank);
		         		row.createCell(4).setCellValue(blank);
		         		row.createCell(5).setCellValue(blank);
		         		row.createCell(6).setCellValue(blank);
		         		row.createCell(7).setCellValue(blank);
		         		row.createCell(8).setCellValue(blank);
		         		row.createCell(9).setCellValue(blank);
		         		row.createCell(10).setCellValue(blank);
		         		row.createCell(11).setCellValue(blank);
		         		row.createCell(12).setCellValue(blank);
		         		row.createCell(13).setCellValue(blank);
		         		row.createCell(14).setCellValue(blank);
		         		row.createCell(15).setCellValue(blank);
		         		row.createCell(16).setCellValue(blank);
		         		row.createCell(17).setCellValue(blank);
		         		row.createCell(18).setCellValue(blank);
		         		row.createCell(19).setCellValue(blank);
		         		row.createCell(20).setCellValue(blank);
		         		row.createCell(21).setCellValue(five.get(ntid));

				    }
				  
			}	
			 
		 }
			    
	} 	
 	
 	
 	
		public ArrayList<ArrayList<String>> getLoginFromDB(Connection con, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six,ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine, ArrayList<String> ten, ArrayList<String> eleven,  ArrayList<String> twelve, ArrayList<String> thirteen, ArrayList<String> AllinOneUsername) throws SQLException
 		{
			
			ArrayList<String> PGS = new ArrayList<>();
			ArrayList<String> RND = new ArrayList<>();
			ArrayList<String> GIS = new ArrayList<>();
			ArrayList<String> RREMS = new ArrayList<>();
			ArrayList<String> FBO = new ArrayList<>();
			ArrayList<String> CPPM = new ArrayList<>();
			ArrayList<String> AIBI = new ArrayList<>();
			ArrayList<String> PROCUREMENT = new ArrayList<>();
			ArrayList<String> DSE = new ArrayList<>();
			ArrayList<String> ARCHITECTURE = new ArrayList<>();
			ArrayList<String> HRCFS = new ArrayList<>();
			ArrayList<String> GPD = new ArrayList<>();
			ArrayList<String> ECS = new ArrayList<>();

			
			ArrayList<String> UserNameFullList = new ArrayList<>();
			ArrayList<String> FullNameFullList = new ArrayList<>();
			ArrayList<String> EmailAddressFullList = new ArrayList<>();
		
			ArrayList<String> LoginFullList = new ArrayList<>();
			
			System.out.println("Database connection successful");
			String PGSquery = "'"+one.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String RNDquery = "'"+two.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String GISquery = "'"+three.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String RREMSquery = "'"+four.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String FBOquery = "'"+five.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String CPPMquery = "'"+six.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String AIBIquery = "'"+seven.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String PROCUREMENTquery = "'"+eight.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String DSEquery = "'"+nine.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String ARCHITECTUREquery = "'"+ten.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String HRCFSquery = "'"+eleven.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String GPDquery = "'"+twelve.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			String ECSquery = "'"+thirteen.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
						
			Statement s1 = con.createStatement();

			ResultSet PGSrs = s1.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+PGSquery.toString()+") GROUP BY u.lower_user_name;");
			while(PGSrs.next()){
				PGS.add(PGSrs.getString(2));
			}
			
			Statement s2 = con.createStatement();

			ResultSet RNDrs = s2.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+RNDquery.toString()+") GROUP BY u.lower_user_name;");
			while(RNDrs.next()){
				RND.add(RNDrs.getString(2));
			}
			
			Statement s3 = con.createStatement();
			ResultSet GISrs = s3.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+GISquery.toString()+") GROUP BY u.lower_user_name;");
			while(GISrs.next()){
				GIS.add(GISrs.getString(2));
			}
			
			Statement s4 = con.createStatement();
			ResultSet RREMSrs = s4.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+RREMSquery.toString()+") GROUP BY u.lower_user_name;");
			while(RREMSrs.next()){
				RREMS.add(RREMSrs.getString(2));
			}

			Statement s5 = con.createStatement();
			ResultSet FBOrs = s5.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+FBOquery.toString()+") GROUP BY u.lower_user_name;");
			while(FBOrs.next()){
				FBO.add(FBOrs.getString(2));
			}

			Statement s6 = con.createStatement();
			ResultSet CPPMrs = s6.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+CPPMquery.toString()+") GROUP BY u.lower_user_name;");
			while(CPPMrs.next()){
				CPPM.add(CPPMrs.getString(2));
			}

			Statement s7 = con.createStatement();
			ResultSet AIBIrs = s7.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+AIBIquery.toString()+") GROUP BY u.lower_user_name;");
			while(AIBIrs.next()){
				AIBI.add(AIBIrs.getString(2));
			}

			Statement s8 = con.createStatement();
			ResultSet PROCUREMENTrs = s8.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+PROCUREMENTquery.toString()+") GROUP BY u.lower_user_name;");
			while(PROCUREMENTrs.next()){
				PROCUREMENT.add(PROCUREMENTrs.getString(2));
			}

			Statement s9 = con.createStatement();
			ResultSet DSErs = s9.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+DSEquery.toString()+") GROUP BY u.lower_user_name;");
			while(DSErs.next()){
				DSE.add(DSErs.getString(2));
			}
			
			Statement s10 = con.createStatement();
			ResultSet ARCHITECTURErs = s10.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+ARCHITECTUREquery.toString()+") GROUP BY u.lower_user_name;");
			while(ARCHITECTURErs.next()){
				ARCHITECTURE.add(ARCHITECTURErs.getString(2));
			}
			
			Statement s11 = con.createStatement();
			ResultSet HRCFSrs = s11.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+HRCFSquery.toString()+") GROUP BY u.lower_user_name;");
			while(HRCFSrs.next()){
				HRCFS.add(HRCFSrs.getString(2));
			}
			
			Statement s12 = con.createStatement();
			ResultSet GPDrs = s12.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+GPDquery.toString()+") GROUP BY u.lower_user_name;");
			while(GPDrs.next()){
				GPD.add(GPDrs.getString(2));
			}

			Statement s13 = con.createStatement();
			ResultSet ECSrs = s13.executeQuery("SELECT u.lower_user_name, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+ECSquery.toString()+") GROUP BY u.lower_user_name;");
			while(ECSrs.next()){
				ECS.add(ECSrs.getString(2));
			}

			Collections.sort(AllinOneUsername);
			System.out.println("Size before remov dup "+AllinOneUsername.size());
			System.out.println(AllinOneUsername.toString());
			ArrayList<String> FinalAllUsernameList = (ArrayList<String>) AllinOneUsername.stream().distinct().collect(Collectors.toList());
			System.out.println("Removed duplicated "+FinalAllUsernameList.size());
			
			
			System.out.println("After REmoving duplicate "+FinalAllUsernameList);
			String FullQuery = "'"+FinalAllUsernameList.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			
			Statement s14 = con.createStatement();
			ResultSet Fullrs = s14.executeQuery("SELECT uuu.lower_user_name, uuu.display_name, uuu.email_address, uuu.active, uuu.R1 FROM (SELECT lower_user_name, display_name,email_address,active,MAX(DATEADD(second, cast(attribute_value AS BIGINT) / 1000, {d '1970-01-01' })) as R1 , ROW_NUMBER() OVER (partition by lower_user_name ORDER BY display_name) TEST FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID WHERE lower_user_name IN ("+FullQuery.toString()+") AND active = '1' GROUP BY lower_user_name, display_name, email_address, active) uuu Where uuu.TEST = 1;");
			while(Fullrs.next()){
				UserNameFullList.add(Fullrs.getString(1));
				FullNameFullList.add(Fullrs.getString(2));
				EmailAddressFullList.add(Fullrs.getString(3));
				
				LoginFullList.add(Fullrs.getString(5));

			}
			System.out.println("count of all users "+LoginFullList.size());

				
			
			ArrayList<ArrayList<String>> al = new ArrayList<>();
			al.add(PGS);
			al.add(RND);
			al.add(GIS);
			al.add(RREMS);
			al.add(FBO);
			al.add(CPPM);
			al.add(AIBI);
			al.add(PROCUREMENT);
			al.add(DSE);
			al.add(ARCHITECTURE);
			al.add(HRCFS);
			al.add(GPD);
			al.add(ECS);

//	
			al.add(UserNameFullList);
			al.add(FullNameFullList);
			al.add(EmailAddressFullList);
			al.add(LoginFullList);

			return al;
		}
		
		public ArrayList<ArrayList<Boolean>> getBooleanList(Connection con, ArrayList<String> AllinOneUsername) throws SQLException
		{
			ArrayList<ArrayList<Boolean>> alBoolean = new ArrayList<>();
			ArrayList<Boolean> FlagFullList = new ArrayList<>();
			
			Collections.sort(AllinOneUsername);
			System.out.println("Size before remov dup "+AllinOneUsername.size());
			System.out.println(AllinOneUsername.toString());
			ArrayList<String> FinalAllUsernameList = (ArrayList<String>) AllinOneUsername.stream().distinct().collect(Collectors.toList());
			System.out.println("Removed duplicated "+FinalAllUsernameList.size());
			
			
			System.out.println("After REmoving duplicate "+FinalAllUsernameList);
			String FullQuery = "'"+FinalAllUsernameList.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
			
			Statement s10 = con.createStatement();
			ResultSet Fullrs = s10.executeQuery("SELECT uuu.lower_user_name, uuu.display_name, uuu.email_address, uuu.active, uuu.R1 FROM (SELECT lower_user_name, display_name,email_address,active,MAX(DATEADD(second, cast(attribute_value AS BIGINT) / 1000, {d '1970-01-01' })) as R1 , ROW_NUMBER() OVER (partition by lower_user_name ORDER BY display_name) TEST FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID WHERE lower_user_name IN ("+FullQuery.toString()+") AND active = '1' GROUP BY lower_user_name, display_name, email_address, active) uuu Where uuu.TEST = 1;");
			while(Fullrs.next()){
				
// Old Query Before PGS_RND Segregation
//"SELECT u.lower_user_name, u.display_name, u.email_address, u.active, MAX(DATEADD(second, cast(attribute_value as bigint)/1000,{d '1970-01-01'})) FROM cwd_user u JOIN cwd_membership m ON u.id = m.child_id AND u.directory_id = m.directory_id LEFT OUTER JOIN (SELECT * FROM dbo.cwd_user_attributes ca WHERE attribute_name = 'login.lastLoginMillis') AS a ON a.user_id = u.ID JOIN cwd_directory d ON m.directory_id = d.id WHERE u.lower_user_name  IN("+FullQuery.toString()+") AND u.active = '1' GROUP BY u.lower_user_name, u.display_name, u.email_address, u.active;"				
				FlagFullList.add(Fullrs.getBoolean(4));
				
			}
			System.out.println("count of all flag "+FlagFullList.size());
			
			alBoolean.add(FlagFullList);
			
			return alBoolean;
			
		}
	
}

