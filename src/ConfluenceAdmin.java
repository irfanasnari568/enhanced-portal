

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Servlet implementation class ConfluenceAdmin_Report
 */
@WebServlet("/ConfluenceAdmin")
public class ConfluenceAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			//Connection Created with microsoft sql server
			//String url="jdbc:sqlserver://;serverName=AMRDRMW637;portNumber=2023;databaseName=CONFLUENCEDB_P";
			String url = "jdbc:sqlserver://AMRDRMW637:2023;databaseName=CONFLUENCEDB_P";
			System.out.println(url);
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection connection=DriverManager.getConnection(url, "CONFLUENCEDB_P_User", "1qazxsw@2019");
			System.out.println(connection);
		    Statement stmt = connection.createStatement();
		    Statement statement=connection.createStatement();
		    
		  //Creating Workbook/Excel using Apache POI
	         XSSFWorkbook workbook=new XSSFWorkbook();
	         
	         String filename = "Confluence_Admin_Report.xlsx";
		    	String outputFile= "/usr/local/Enhanced/reports/"+filename;
		     	//String outputFile = "D:\\Users\\"+filename;
	         
	       //Creating Cell Style applied in both sheets
	         XSSFCellStyle style=workbook.createCellStyle();
	         XSSFFont font=workbook.createFont();
	         font.setBold(true);
	         style.setFont(font);
		    
		      //Executing 1st query (Space_Admin Details)
	         String sql;
	         sql = "select con.Space_Name, con.Space_key,con.Space_Type,con.Admin_Type,CONCAT(con.Group_Admin,'',con.User_Admin) as Space_Admin, con.Full_Name, con.Email_Address,con.Business_Line from (select raj.Space_Name, raj.Space_key,raj.Space_Type,raj.Admin_Type,(CASE raj.Group_Admin WHEN 'NULL' THEN '' ELSE raj.Group_Admin END) as Group_Admin, (CASE raj.User_Admin WHEN 'NULL' THEN '' ELSE raj.User_Admin END) as User_Admin,raj.Full_Name,raj.Email_Address,(CASE raj.Admin_Type WHEN 'GROUP' THEN 'GROUP' ELSE raj.Business_Lines END) as Business_Line from (select distinct inst.Space_Name,inst.Space_key,inst.Space_Type,inst.Admin_Type,inst.Group_Admin,inst.User_Admin, inst.Full_Name, inst.Email_Address,cd.directory_name,(CASE cd.directory_name WHEN 'Confluence Internal Directory' THEN cd.directory_name WHEN 'LDAP server' THEN 'Inactive' WHEN 'Delegated LDAP Authentication' THEN 'Inactive' WHEN 'LDAP server-con' THEN 'GIS' WHEN 'LDAP server confprodrrems' THEN 'RREMS' WHEN 'LDAP server confprodfbo' THEN 'FBO' WHEN 'LDAP server confprodcppm' THEN 'CPPM' WHEN 'LDAP server confprodaibi' THEN 'AIBI' WHEN 'LDAP server confprodprocurement' THEN 'PROCUREMENT' WHEN 'LDAP server confproddse-commercial' THEN 'DSE-Commercial' WHEN 'LDAP server confprodarchitecture' THEN 'Digital Architecture' WHEN 'LDAP server of Confprodhrcfs' THEN 'HRCFS' WHEN 'LDAP server for confprodpgs' THEN 'PGS' WHEN 'LDAP server for confprodrnd' THEN 'RND' WHEN 'LDAP server of Confprodgpd' THEN 'GPD' WHEN 'LDAP server Confdevecs' THEN 'ECS' END) as Business_Lines , inst.date, ROW_NUMBER() over (partition by CONCAT(inst.User_Admin,',',inst.Space_Name) order by inst.date desc) as dev from (select distinct SPACENAME as Space_Name, SPACEKEY as Space_key, SPACETYPE as Space_Type,(CASE WHEN PERMGROUPNAME IS NULL THEN 'USER' ELSE 'GROUP' END)as Admin_Type,(CASE WHEN PERMGROUPNAME IS NULL THEN 'NULL' ELSE PERMGROUPNAME END) as Group_Admin,(CASE WHEN PERMUSERNAME IS NULL THEN 'NULL' ELSE username END)as User_Admin, (CASE WHEN PERMUSERNAME IS NULL THEN 'NULL' ELSE display_name END) as Full_Name, (CASE WHEN PERMUSERNAME IS NULL THEN 'NULL' ELSE email_address END) as Email_Address, w.directory_id as dirid, w.updated_date as date from dbo.SPACES s JOIN dbo.SPACEPERMISSIONS p ON s.SPACEID=p.SPACEID LEFT JOIN dbo.user_mapping u ON u.user_key=p.PERMUSERNAME LEFT JOIN dbo.cwd_user w ON w.user_name=u.username where p.PERMTYPE='SETSPACEPERMISSIONS')inst LEFT JOIN cwd_directory cd ON cd.id=inst.dirid and cd.directory_name in ('Confluence Internal Directory', 'LDAP server','Delegated LDAP Authentication','LDAP server-con','LDAP server confprodrrems', 'LDAP server confprodfbo', 'LDAP server confprodcppm', 'LDAP server confprodaibi', 'LDAP server confprodprocurement','LDAP server confproddse-commercial', 'LDAP server confprodarchitecture', 'LDAP server of Confprodhrcfs', 'LDAP server for confprodpgs','LDAP server for confprodrnd', 'LDAP server of Confprodgpd', 'LDAP server Confdevecs') ) raj where raj.dev=1)con";
	         ResultSet rs = stmt.executeQuery(sql);
	        
	         
	          //Creating Space_Admin sheet (Sheet-1)
	         XSSFSheet sheet= workbook.createSheet("Space_Admin");  
	         
	         //Creating 1st Row 
	         XSSFRow row=sheet.createRow((short)0);
	        
	         //Creating cells in (1st row-Header) and applying style
	         Cell cell0=row.createCell((short)0);
	         cell0.setCellValue("Space_Name");
	         cell0.setCellStyle(style);
	         Cell cell1=row.createCell((short)1);
	         cell1.setCellValue("Space_Key");
	         cell1.setCellStyle(style);
	         Cell cell2=row.createCell((short)2);
	         cell2.setCellValue("Space_Type");
	         cell2.setCellStyle(style);
	         Cell cell3=row.createCell((short)3);
	         cell3.setCellValue("Admin_Type");
	         cell3.setCellStyle(style);
	         Cell cell4=row.createCell((short)4);
	         cell4.setCellValue("Space_Admin");
	         cell4.setCellStyle(style);
	         Cell cell5=row.createCell((short)5);
	         cell5.setCellValue("Full_Name");
	         cell5.setCellStyle(style);
	         Cell cell6=row.createCell((short)6);
	         cell6.setCellValue("Email_Address");
	         cell6.setCellStyle(style);
	         Cell cell7=row.createCell((short)7);
	         cell7.setCellValue("Business_Line");
	         cell7.setCellStyle(style);
	         int i=1;
	        
	         while(rs.next()){
	             //Retrieve by column name
	             String Space_Name  = rs.getString("Space_Name");
	             String Space_Key  = rs.getString("Space_key");
	             String Space_Type = rs.getString("Space_Type");
	             String Admin_Type = rs.getString("Admin_Type");
	             String Space_Admin = rs.getString("Space_Admin");
	             String Full_Name=rs.getString("Full_Name");
	             String Email_Address=rs.getString("Email_Address");
	             String Business_Line=rs.getString("Business_Line");
 
	             //Writing rowwise value in Space_Admin sheet
	             XSSFRow row2=sheet.createRow((short)i);
	             row2.createCell((short)0).setCellValue(Space_Name);
	             row2.createCell((short)1).setCellValue(Space_Key);
	             row2.createCell((short)2).setCellValue(Space_Type);
	             row2.createCell((short)3).setCellValue(Admin_Type);
	             row2.createCell((short)4).setCellValue(Space_Admin);
	             row2.createCell((short)5).setCellValue(Full_Name);
	             row2.createCell((short)6).setCellValue(Email_Address);
	             row2.createCell((short)7).setCellValue(Business_Line);
	             i++;
	           
	          }
	         
	         //Configure column size in sheet-1
	         int CoumnNo=sheet.getRow(0).getLastCellNum();
	         for(int a=0;a<=CoumnNo;a++)
	         {
	         sheet.autoSizeColumn(a);
	         }
	         
	         //Executing 2nd query (Group_Members Details)
	         String sql2="select raj.Group_Name, raj.Group_User,raj.Full_Name,raj.Email_Address,raj.Business_Line from (select distinct inst.Group_Name,inst.Group_User,inst.Full_Name,inst.Email_Address,cd.directory_name, (CASE cd.directory_name WHEN 'Confluence Internal Directory' THEN cd.directory_name WHEN  'LDAP server' THEN 'Inactive' WHEN 'Delegated LDAP Authentication' THEN 'Inactive' WHEN 'LDAP server-con' THEN 'GIS' WHEN 'LDAP server confprodrrems' THEN 'RREMS' WHEN 'LDAP server confprodfbo' THEN 'FBO' WHEN 'LDAP server confprodcppm' THEN 'CPPM' WHEN 'LDAP server confprodaibi' THEN 'AIBI' WHEN 'LDAP server confprodprocurement' THEN 'PROCUREMENT' WHEN 'LDAP server confproddse-commercial' THEN 'DSE-Commercial' WHEN 'LDAP server confprodarchitecture' THEN 'Digital Architecture' WHEN 'LDAP server of Confprodhrcfs' THEN 'HRCFS' WHEN 'LDAP server for confprodpgs' THEN 'PGS' WHEN 'LDAP server for confprodrnd' THEN 'RND' WHEN 'LDAP server of Confprodgpd' THEN 'GPD' WHEN 'LDAP server Confdevecs' THEN 'ECS' END) as Business_Line , inst.date, ROW_NUMBER() over (partition by CONCAT(inst.Group_Name,',',inst.Group_User) order by inst.date desc) as dev from (select distinct PERMGROUPNAME as Group_Name, u.id as userid, user_name as Group_User, display_name as Full_Name, email_address as Email_Address,u.directory_id as dirid, u.updated_date as date  from dbo.SPACES s JOIN  dbo.SPACEPERMISSIONS p ON s.SPACEID=p.SPACEID JOIN dbo.cwd_group g ON  p.PERMGROUPNAME=g.group_name and p.PERMTYPE='SETSPACEPERMISSIONS' JOIN dbo.cwd_membership m ON m.parent_id=g.id JOIN dbo.cwd_user u ON u.id=m.child_user_id)inst LEFT JOIN cwd_directory cd ON cd.id=inst.dirid and cd.directory_name in ('Confluence Internal Directory', 'LDAP server','Delegated LDAP Authentication', 'LDAP server-con','LDAP server confprodrrems', 'LDAP server confprodfbo', 'LDAP server confprodcppm', 'LDAP server confprodaibi', 'LDAP server confprodprocurement', 'LDAP server confproddse-commercial', 'LDAP server confprodarchitecture', 'LDAP server of Confprodhrcfs', 'LDAP server for confprodpgs', 'LDAP server for confprodrnd', 'LDAP server of Confprodgpd', 'LDAP server Confdevecs') ) raj where raj.dev=1";
	         ResultSet rs2=statement.executeQuery(sql2);
	         
	         //Creating Group_Details Sheet (Sheet-2)
	         XSSFSheet sheet2= workbook.createSheet("Group_Details");
	         
	         //Creating 1st Row
	         XSSFRow rows=sheet2.createRow((short)0);
	         
	       //Creating cells (1st row/Header) and applying style
	         Cell cells0=rows.createCell((short)0);
	         cells0.setCellValue("Group_Name");
	         cells0.setCellStyle(style);
	         Cell cells1=rows.createCell((short)1);
	         cells1.setCellValue("Group_User");
	         cells1.setCellStyle(style);
	         Cell cells2=rows.createCell((short)2);
	         cells2.setCellValue("Full_Name");
	         cells2.setCellStyle(style);
	         Cell cells3=rows.createCell((short)3);
	         cells3.setCellValue("Email_Address");
	         cells3.setCellStyle(style);
	         Cell cells4=rows.createCell((short)4);
	         cells4.setCellValue("Business_Line");
	         cells4.setCellStyle(style);
	         int j=1;
	         while(rs2.next())
	         {
	        	 //Retrieving values by column_name
	        	 String Group_Name  = rs2.getString("Group_Name");
	             String Group_User  = rs2.getString("Group_User");
	             String Full_Name = rs2.getString("Full_Name");
	             String Email_Address=rs2.getString("Email_Address");
	             String Business_Line=rs2.getString("Business_Line");
	             
	             //Writing rowwise value in Group_Details sheet
	             XSSFRow rows2=sheet2.createRow((short)j);
	             rows2.createCell((short)0).setCellValue(Group_Name);
	             rows2.createCell((short)1).setCellValue(Group_User);
	             rows2.createCell((short)2).setCellValue(Full_Name);
	             rows2.createCell((short)3).setCellValue(Email_Address);
	             rows2.createCell((short)4).setCellValue(Business_Line);
	             j++;
	         }
	         
	       //Configure column size in sheet-2
	         int CoumnNos=sheet2.getRow(0).getLastCellNum();
	         for(int b=0;b<=CoumnNos;b++)
	         {
	         sheet2.autoSizeColumn(b);
	         }
	         
	        // Downloading excel-sheet in xlsx format
	         
	    	   
      		FileOutputStream oFile = new FileOutputStream(outputFile); 
      		workbook.write(oFile);
//	         response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//	         response.setHeader("Content-Disposition", "attachment;filename= ConfluenceAdmin_Report.xlsx");
//	         ServletOutputStream outStream = response.getOutputStream();
//	         
//	      // Write workbook to response.
//	         workbook.write(outStream); 
//	         outStream.close();
	         
	 		File file = new File(outputFile);

			if(file.exists())
			{
				//			Desktop desktop = Desktop.getDesktop();
				//			desktop.open(file);
				//			InputStream in = new URL(f).openStream();
				//			Files.copy(in, Paths.get(f));


				response.setContentType("application/vnd.ms-excel");   
				response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
				System.out.println("filename new  "+filename);
				
				BufferedInputStream in=null;
				ServletOutputStream outs=null;
				byte[] buffer = new byte[1024]; 
				
				int g = 0;
				in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
				outs=response.getOutputStream();
				while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
					outs.write(buffer, 0, g);  
				}  
				outs.flush();  
				System.out.println("Done !!");

				outs.close();
				in.close();
				
			}
			else
			{
				System.out.println("not found");
			}

	         
	         
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		
		
	}

	
	
	
}
