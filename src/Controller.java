

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String result = request.getParameter("name");
		
		System.out.println(result);
		
		
		if(result.equals("group"))
		{
			RequestDispatcher rd1 = request.getRequestDispatcher("JiraUserGroup");
			rd1.forward(request,response);
			return;
		}
		
		if(result.equals("jira"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("JiraController");
			rd.forward(request,response);
			return;
		}
		
		if (result.equals("conf"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("ConfluenceController");
			rd.forward(request,response);
			return;
		}
		
		if(result.equals("confgroup"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("ConfluenceUserGroup");
			rd.forward(request,response);
			return;
		}
		
		if(result.equals("project"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("ProjectList");
			rd.forward(request,response);
			return;
		}
		
		if(result.equals("confAdmin"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("ConfluenceAdmin");
			rd.forward(request,response);
			return;
		}
		
		if(result.equals("JSD"))
		{
			//RequestDispatcher rd = request.getRequestDispatcher("JSDController");
			RequestDispatcher rd = request.getRequestDispatcher("ServiceDeskController");
			rd.forward(request,response);
			return;
		}
		
		if(result.equals("jiraAdmin"))
		{
			RequestDispatcher rd = request.getRequestDispatcher("JiraAdmin");
			rd.forward(request,response);
			return;
		}
		
		
		
		
		
	}

}
