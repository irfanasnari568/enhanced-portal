

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@WebServlet("/ConfluenceController")
public class ConfluenceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	String userhome = System.getProperty("user.home");
	
	int startAt = 0;
	String login= "bc8c2e2b59c44e8497a279fa85848fb3";
	String password= "a03f507332514bddBf02288759961A1c";
	
	String Researchlogin = "SRVAMR-ATLRPT";
	String Researchpass = "Pfe63478";
	
	
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	String userNameDB = "CONFLUENCEDB_P_User";
	String passwordDB = "1qazxsw@2019";
	String url = "jdbc:sqlserver://AMRDRMW637:2023;databaseName=CONFLUENCEDB_P";

	XSSFWorkbook newWorkbook = new XSSFWorkbook();
	
  
	String authString = login + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	
	
	public ConfluenceController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		XSSFSheet PGSSheet = null, RNDSheet = null, FULLSheet = null, SUMMARYSHEET = null;
		XSSFSheet GISSheet = null, RREMSSheet = null, FBOSheet = null, CPPMSheet = null, AIBISheet = null, PROCUREMENTSheet = null, DSESheet = null, ARCHITECTURESheet = null, HRCFSSheet = null, GPDSheet  = null, ECSSheet = null;
		String PGSTEXT = "PGS-User List";
		String RNDTEXT = "RND-User List";
		String GISTEXT = "GIS-User List";
		String RREMSTEXT = "RREMS-User List";
		String FBOTEXT = "FBO-User List";
	    String CPPMTEXT = "CPPM-User List";
	    String AIBITEXT = "AIBI-User List";
	    String PROCUREMENTTEXT = "PROCUREMENT-USER List";
	    String DSETEXT = "DSE-COMMERCIAL-USER List";
	    String ARCHITECTURETEXT = "ARCHITECTURE-USER List";
	    String HRCFSTEXT = "HRCFS-User List";
	    String GPDTEXT = "GPD-User List";
	    String ECSTEXT = "ECS-User List";
	    String FULLTEXT = "FULL LIST";
	    String SUMMARYTEXT = "SUMMARY";
		
		ConfluenceController user = new ConfluenceController();
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}			
		String PGSGROUP= "confprodpgs", RNDGROUP = "confprodrnd" ,GISGROUP= "confprod", RREMSGROUP="confprodrrems", FBOGROUP="confprodfbo";
		String CPPMGROUP ="confprodcppm", AIBIGROUP="confprodaibi", PROCUREMENTGROUP="confprodprocurement" , DSEGROUP = "confproddse-commercial";
		String ARCHITECTUREGROUP= "confprodarchitecture", HRCFSGROUP = "confprodhrcfs", GPDGROUP = "Confprodgpd", ECSGROUP = "Confprodecs";
		
		int startAt = 0;
		int max = 200;
		ArrayList<ArrayList<String>> PGSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> PGSParent = new ArrayList<>();
				
		ArrayList<String> one = new ArrayList<>();
		ArrayList<String> two = new ArrayList<>();
		ArrayList<String> PGSLogin = new ArrayList<>();
		ArrayList<String> three = new ArrayList<>();
		  
		try {
			PGSParent = user.get_confluence_users(startAt, one, two, PGSContainer, PGSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 one = PGSParent.get(0);
		 two = PGSParent.get(1);
		
		while(one.size() >= max)
		{
			System.out.println("Increasing list "+one.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				PGSParent = user.get_confluence_users(startAt, one, two, PGSContainer, PGSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total PGS Users "+one.size());

		// PGS END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> RNDContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> RNDParent = new ArrayList<>();
				
		ArrayList<String> oneRnd = new ArrayList<>();
		ArrayList<String> twoRnd = new ArrayList<>();
		ArrayList<String> threeRnd = new ArrayList<>();
		ArrayList<String> RNDLogin = new ArrayList<>();
		  
		try {
			RNDParent = user.get_confluence_users(startAt, oneRnd, twoRnd, RNDContainer, RNDGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneRnd = RNDParent.get(0);
		 twoRnd = RNDParent.get(1);
		
		while(oneRnd.size() >= max)
		{
			System.out.println("Increasing list "+oneRnd.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				RNDParent = user.get_confluence_users(startAt, oneRnd, twoRnd, RNDContainer, RNDGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total GIS Users "+oneRnd.size());
		
		// RND END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> GISContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> GISParent = new ArrayList<>();
				
		ArrayList<String> oneGis = new ArrayList<>();
		ArrayList<String> twoGis = new ArrayList<>();
		ArrayList<String> threeGis = new ArrayList<>();
		ArrayList<String> GISLogin = new ArrayList<>();
		  
		try {
			GISParent = user.get_confluence_users(startAt, oneGis, twoGis, GISContainer, GISGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneGis = GISParent.get(0);
		 twoGis = GISParent.get(1);
		
		while(oneGis.size() >= max)
		{
			System.out.println("Increasing list "+oneGis.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				GISParent = user.get_confluence_users(startAt, oneGis, twoGis, GISContainer, GISGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total GIS Users "+oneGis.size());

		// GIS END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> RREMSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> RREMSParent = new ArrayList<>();
				
		ArrayList<String> oneRrems = new ArrayList<>();
		ArrayList<String> twoRrems = new ArrayList<>();
		ArrayList<String> threeRrems = new ArrayList<>();
		ArrayList<String> RREMSLogin = new ArrayList<>();
		  
		try {
			RREMSParent = user.get_confluence_users(startAt, oneRrems, twoRrems, RREMSContainer, RREMSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneRrems = RREMSParent.get(0);
		 twoRrems = RREMSParent.get(1);
		
		while(oneRrems.size() >= max)
		{
			System.out.println("Increasing list "+oneRrems.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				RREMSParent = user.get_confluence_users(startAt, oneRrems, twoRrems, RREMSContainer, RREMSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total RREMS Users "+oneRrems.size());

		// RREMS END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> FBOContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> FBOParent = new ArrayList<>();
				
		ArrayList<String> oneFbo = new ArrayList<>();
		ArrayList<String> twoFbo = new ArrayList<>();
		ArrayList<String> threeFbo = new ArrayList<>();
		ArrayList<String> FBOLogin = new ArrayList<>();
		  
		try {
			FBOParent = user.get_confluence_users(startAt, oneFbo, twoFbo, FBOContainer, FBOGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneFbo = FBOParent.get(0);
		 twoFbo = FBOParent.get(1);
		
		while(oneFbo.size() >= max)
		{
			System.out.println("Increasing list "+oneFbo.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				FBOParent = user.get_confluence_users(startAt, oneFbo, twoFbo, FBOContainer, FBOGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total FBO Users "+oneFbo.size());

		// FBO END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> CPPMContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> CPPMParent = new ArrayList<>();
				
		ArrayList<String> oneCppm = new ArrayList<>();
		ArrayList<String> twoCppm = new ArrayList<>();
		ArrayList<String> threeCppm = new ArrayList<>();
		ArrayList<String> CPPMLogin = new ArrayList<>();
		  
		try {
			CPPMParent = user.get_confluence_users(startAt, oneCppm, twoCppm, CPPMContainer, CPPMGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneCppm = CPPMParent.get(0);
		 twoCppm = CPPMParent.get(1);
		
		while(oneCppm.size() >= max)
		{
			System.out.println("Increasing list "+oneCppm.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				CPPMParent = user.get_confluence_users(startAt, oneCppm, twoCppm, CPPMContainer, CPPMGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total CPPM Users "+oneCppm.size());

		// CPPM END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> AIBIContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> AIBIParent = new ArrayList<>();
				
		ArrayList<String> oneAibi = new ArrayList<>();
		ArrayList<String> twoAibi = new ArrayList<>();
		ArrayList<String> threeAibi = new ArrayList<>();
		ArrayList<String> AIBILogin = new ArrayList<>();
		  
		try {
			AIBIParent = user.get_confluence_users(startAt, oneAibi , twoAibi , AIBIContainer, AIBIGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneAibi = AIBIParent.get(0);
		 twoAibi = AIBIParent.get(1);
		 System.out.println("AIBI user list "+oneAibi.toString());
		
		while(oneAibi.size() >= max)
		{
			System.out.println("Increasing list "+oneAibi.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				AIBIParent = user.get_confluence_users(startAt, oneAibi , twoAibi , AIBIContainer, AIBIGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total AIBI Users "+oneAibi.size());

		// AIBI END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> PROCUREMENTContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> PROCUREMENTParent = new ArrayList<>();
				
		ArrayList<String> oneProcurement = new ArrayList<>();
		ArrayList<String> twoProcurement = new ArrayList<>();
		ArrayList<String> threeProcurement = new ArrayList<>();
		ArrayList<String> PROCUREMENTLogin = new ArrayList<>();
		  
		try {
			PROCUREMENTParent = user.get_confluence_users(startAt, oneProcurement , twoProcurement , PROCUREMENTContainer, PROCUREMENTGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneProcurement = PROCUREMENTParent.get(0);
		 twoProcurement = PROCUREMENTParent.get(1);
		
		
		while(oneProcurement.size() >= max)
		{
			System.out.println("Increasing list "+oneProcurement.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				PROCUREMENTParent = user.get_confluence_users(startAt, oneProcurement , twoProcurement , PROCUREMENTContainer, PROCUREMENTGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total PROCUREMENT Users "+oneProcurement.size());

		// PROCUREMENT END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> DSEContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> DSEParent = new ArrayList<>();
				
		ArrayList<String> oneDse = new ArrayList<>();
		ArrayList<String> twoDse = new ArrayList<>();
		ArrayList<String> threeDse = new ArrayList<>();
		ArrayList<String> DSELogin = new ArrayList<>();
		  
		try {
			DSEParent = user.get_confluence_users(startAt, oneDse , twoDse , DSEContainer, DSEGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneDse = DSEParent.get(0);
		 twoDse = DSEParent.get(1);
		 System.out.println("DSE user list "+oneDse.toString());
		
		while(oneDse.size() >= max)
		{
			System.out.println("Increasing list "+oneDse.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				DSEParent = user.get_confluence_users(startAt, oneDse, twoDse, DSEContainer, DSEGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total DSE Users "+oneDse.size());

		// DSE END HERE

		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> ARCHITECTUREContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> ARCHITECTUREParent = new ArrayList<>();
				
		ArrayList<String> oneArchitecture = new ArrayList<>();
		ArrayList<String> twoArchitecture = new ArrayList<>();
		ArrayList<String> threeArchitecture = new ArrayList<>();
		ArrayList<String> ARCHITECTURELogin = new ArrayList<>();
		  
		try {
			ARCHITECTUREParent = user.get_confluence_users(startAt, oneArchitecture , twoArchitecture , ARCHITECTUREContainer, ARCHITECTUREGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneArchitecture = ARCHITECTUREParent.get(0);
		twoArchitecture = ARCHITECTUREParent.get(1);
		 System.out.println("ARCHITECTURE user list "+oneArchitecture.toString());
		
		while(oneArchitecture.size() >= max)
		{
			System.out.println("Increasing list "+oneArchitecture.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				ARCHITECTUREParent = user.get_confluence_users(startAt, oneArchitecture, twoArchitecture, ARCHITECTUREContainer, ARCHITECTUREGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total ARCHITECTURE Users "+oneArchitecture.size());

		// ARCHITECTURE END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> HRCFSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> HRCFSParent = new ArrayList<>();
				
		ArrayList<String> oneHrcfs = new ArrayList<>();
		ArrayList<String> twoHrcfs = new ArrayList<>();
		ArrayList<String> threeHrcfs = new ArrayList<>();
		ArrayList<String> HRCFSLogin = new ArrayList<>();
		  
		try {
			HRCFSParent = user.get_confluence_users(startAt, oneHrcfs , twoHrcfs , HRCFSContainer, HRCFSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneHrcfs = HRCFSParent.get(0);
		twoHrcfs = HRCFSParent.get(1);
		 System.out.println("HRCFS user list "+oneHrcfs.toString());
		
		while(oneHrcfs.size() >= max)
		{
			System.out.println("Increasing list "+oneHrcfs.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				HRCFSParent = user.get_confluence_users(startAt, oneHrcfs, twoHrcfs, HRCFSContainer, HRCFSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total HRCFS Users "+oneHrcfs.size());

		// HRCFS END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> GPDContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> GPDParent = new ArrayList<>();
				
		ArrayList<String> oneGpd = new ArrayList<>();
		ArrayList<String> twoGpd = new ArrayList<>();
		ArrayList<String> threeGpd = new ArrayList<>();
		ArrayList<String> GPDLogin = new ArrayList<>();
		  
		try {
			GPDParent = user.get_confluence_users(startAt, oneGpd, twoGpd, GPDContainer, GPDGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneGpd = GPDParent.get(0);
		twoGpd = GPDParent.get(1);
		 System.out.println("GPD user list "+oneGpd.toString());
		
		while(oneGpd.size() >= max)
		{
			System.out.println("Increasing list "+oneGpd.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				GPDParent = user.get_confluence_users(startAt, oneGpd, twoGpd, GPDContainer, GPDGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total GPD Users "+oneGpd.size());

		// GPD END HERE

		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> ECSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> ECSParent = new ArrayList<>();
				
		ArrayList<String> oneEcs = new ArrayList<>();
		ArrayList<String> twoEcs = new ArrayList<>();
		ArrayList<String> threeEcs = new ArrayList<>();
		ArrayList<String> ECSLogin = new ArrayList<>();
		  
		try {
			ECSParent = user.get_confluence_users(startAt, oneEcs, twoEcs, ECSContainer, ECSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneEcs = ECSParent.get(0);
		twoEcs = ECSParent.get(1);
		 System.out.println("ECS user list "+oneEcs.toString());
		
		while(oneEcs.size() >= max)
		{
			System.out.println("Increasing list "+oneEcs.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				ECSParent = user.get_confluence_users(startAt, oneEcs , twoEcs, ECSContainer, ECSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total ECS Users "+oneEcs.size());

		// ECS END HERE


		// Merging all usernames for Full List.
		
		ArrayList<String> AllinOneUsername = new ArrayList<String>();
		AllinOneUsername.addAll(one);
		AllinOneUsername.addAll(oneRnd);
		AllinOneUsername.addAll(oneGis);
		AllinOneUsername.addAll(oneRrems);
		AllinOneUsername.addAll(oneFbo);
		AllinOneUsername.addAll(oneCppm);
		AllinOneUsername.addAll(oneAibi);
		AllinOneUsername.addAll(oneProcurement);
		AllinOneUsername.addAll(oneDse);
		AllinOneUsername.addAll(oneArchitecture);
		AllinOneUsername.addAll(oneHrcfs);
		AllinOneUsername.addAll(oneGpd);
		AllinOneUsername.addAll(oneEcs);
		
		System.out.println(AllinOneUsername.toString());
		System.out.println(AllinOneUsername.size());
		

		ArrayList<String> oneFull = new ArrayList<>();
		ArrayList<String> twoFull = new ArrayList<>();
		ArrayList<String> threeFull = new ArrayList<>();
		ArrayList<String> FullLogin = new ArrayList<>();
		//Call for Active flag and Last Login Details from DB
		 ArrayList<ArrayList<String>> callForAll = new ArrayList<>();
		 Connection con =null;
	    	
	     	 try {
	    		con = DriverManager.getConnection(url, userNameDB, passwordDB);
	    	
	    		 callForAll = user.getLoginAndFlagFromDB(con, one, oneRnd, oneGis, oneRrems, oneFbo, oneCppm, oneAibi, oneProcurement, oneDse, oneArchitecture, oneHrcfs, oneGpd, oneEcs, AllinOneUsername);
	    		
	    		 three = callForAll.get(0);
	    		 PGSLogin = callForAll.get(1);
	    		 
	    		 threeRnd = callForAll.get(2);
	    		 RNDLogin = callForAll.get(3);
	    		 
	    		 threeGis = callForAll.get(4);
	    		 GISLogin = callForAll.get(5);
	    		 
	    		 threeRrems = callForAll.get(6);
	    		 RREMSLogin = callForAll.get(7);
	    		 
	    		 threeFbo = callForAll.get(8);
	    		 FBOLogin = callForAll.get(9);
	    		 
	    		 threeCppm = callForAll.get(10);
	    		 CPPMLogin = callForAll.get(11);
	    		 
	    		 threeAibi = callForAll.get(12);
	    		 AIBILogin = callForAll.get(13);

	    		 threeProcurement = callForAll.get(14);
	    		 PROCUREMENTLogin = callForAll.get(15);

	    		 threeDse = callForAll.get(16);
	    		 DSELogin = callForAll.get(17);
	    		 
	    		 threeArchitecture = callForAll.get(18);
	    		 ARCHITECTURELogin = callForAll.get(19);
	    		 
	    		 threeHrcfs = callForAll.get(20);
	    		 HRCFSLogin= callForAll.get(21);
	    		 
	    		 threeGpd = callForAll.get(22);
	    		 GPDLogin= callForAll.get(23);
	    		 
	    		 threeEcs = callForAll.get(24);
	    		 ECSLogin= callForAll.get(25);

	    		 // Full List call
	    		 oneFull = callForAll.get(26);
	    		 twoFull = callForAll.get(27);
	    		 threeFull = callForAll.get(28);
	    		 FullLogin = callForAll.get(29);
	

	    	} catch (SQLException e2) {
	    		// TODO Auto-generated catch block
	    		e2.printStackTrace();
	    	}
	     	 
	     	 System.out.println(one.toString());
	     	 System.out.println(two.toString());
	     	 System.out.println(three.toString());
	     	 System.out.println(PGSLogin.toString());
	     	user.writeSummary(SUMMARYSHEET,one, oneRnd, oneGis, oneRrems, oneFbo, oneCppm, oneAibi, oneProcurement, oneDse, oneArchitecture, oneHrcfs, oneGpd, oneEcs, SUMMARYTEXT);
	    	 

	     	try {
				user.WriteData(PGSSheet, one, two, three, PGSLogin, PGSTEXT);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	     	 
	     	try {
				user.WriteData(RNDSheet, oneRnd, twoRnd, threeRnd, RNDLogin, RNDTEXT);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

	     	
	     	try {
	     		user.WriteData(GISSheet, oneGis, twoGis, threeGis, GISLogin, GISTEXT);
	     	} catch (ParserConfigurationException e) {
	     		// TODO Auto-generated catch block
	     		e.printStackTrace();
	     	}
		
	     	try {
	     		user.WriteData(RREMSSheet, oneRrems, twoRrems, threeRrems, RREMSLogin, RREMSTEXT);
	     	} catch (ParserConfigurationException e) {
	     		// TODO Auto-generated catch block
	     		e.printStackTrace();
	     	}
	     	
	     	try {
	     		user.WriteData(FBOSheet, oneFbo, twoFbo, threeFbo, FBOLogin, FBOTEXT);
	     		user.WriteData(CPPMSheet, oneCppm, twoCppm, threeCppm, CPPMLogin, CPPMTEXT);
	     		user.WriteData(AIBISheet, oneAibi, twoAibi, threeAibi, AIBILogin, AIBITEXT);
	     		user.WriteData(PROCUREMENTSheet, oneProcurement, twoProcurement, threeProcurement, PROCUREMENTLogin, PROCUREMENTTEXT);
	     		user.WriteData(DSESheet, oneDse, twoDse, threeDse, DSELogin, DSETEXT);
	     		user.WriteData(ARCHITECTURESheet, oneArchitecture, twoArchitecture, threeArchitecture, ARCHITECTURELogin, ARCHITECTURETEXT);
	     		user.WriteData(HRCFSSheet, oneHrcfs, twoHrcfs, threeHrcfs, HRCFSLogin, HRCFSTEXT);
	     		user.WriteData(GPDSheet, oneGpd, twoGpd, threeGpd, GPDLogin, GPDTEXT);
	     		user.WriteData(ECSSheet, oneEcs, twoEcs, threeEcs, ECSLogin, ECSTEXT);

	     		System.out.println("username no-dup "+oneFull.size());
	     		user.WriteData(FULLSheet, oneFull, twoFull, threeFull, FullLogin, FULLTEXT);
	     	    

	     	} catch (ParserConfigurationException e) {
	     		// TODO Auto-generated catch block
	     		e.printStackTrace();
	     	}
	     	
	     	String filename = "Confluence_org_report.xlsx";
	    	String outputFile= "/usr/local/Enhanced/reports/"+filename;
	    // 	String outputFile = "D:\\Users\\"+filename;
	     	
//	     	String filename = "cond.xlsx";
	  		File file = new File(outputFile);

			if(file.exists())
			{
				//			Desktop desktop = Desktop.getDesktop();
				//			desktop.open(file);
				//			InputStream in = new URL(f).openStream();
				//			Files.copy(in, Paths.get(f));


				response.setContentType("application/vnd.ms-excel");   
				response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
				System.out.println("filename new  "+filename);
				
				BufferedInputStream in=null;
				ServletOutputStream outs=null;
				byte[] buffer = new byte[1024]; 
			
				
				try {
					int g = 0;
					in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
					outs=response.getOutputStream();
					while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
						outs.write(buffer, 0, g);  
					}  
					outs.flush();  

				} catch (IOException ioe) {
					ioe.printStackTrace(System.out);
				}

				System.out.println("Done !!");

				outs.close();
				in.close();
				
			}
			else
			{
				System.out.println("not found");
			}


	
	}
	
	public void writeSummary(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six, ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine, ArrayList<String> ten, ArrayList<String> eleven, ArrayList<String> twelve, ArrayList<String> thirteen, String text) throws IOException
	{
		//XSSFSheet newwSheet = newSheet;
		newSheet = newWorkbook.createSheet(text);
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		System.out.println("From summary tab");
		Row headerRow = newSheet.createRow(0);
		
			Cell headerCell1 = headerRow.createCell(0);	
			Cell headerCell2 = headerRow.createCell(1);	
			Cell headerCell3 = headerRow.createCell(2);	


			headerCell1.setCellValue("Business Line");	
			headerCell1.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(0);
			headerCell2.setCellValue("Count"); 
			headerCell2.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(1);
			headerCell3.setCellValue("($)Dollar"); 
			headerCell3.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(2);

			//
			Row row1 = newSheet.createRow(1);
			Cell PGS1 = row1.createCell(0);
			Cell PGS2 = row1.createCell(1);
			PGS1.setCellValue("PGS");
			PGS2.setCellValue(one.size());
			
			Row row2 = newSheet.createRow(2);
			Cell RND1 = row2.createCell(0);
			Cell RND2 = row2.createCell(1);
			RND1.setCellValue("RND");
			RND2.setCellValue(two.size());
			
			
			Row row3 = newSheet.createRow(3);
			Cell GIS1 = row3.createCell(0);
			Cell GIS2 = row3.createCell(1);
			GIS1.setCellValue("GIS");
			GIS2.setCellValue(three.size());
			
			Row row4 = newSheet.createRow(4);
			Cell RREMS1 = row4.createCell(0);
			Cell RREMS2 = row4.createCell(1);
			RREMS1.setCellValue("RREMS");
			RREMS2.setCellValue(four.size());
			
			Row row5 = newSheet.createRow(5);
			Cell FBO1 = row5.createCell(0);
			Cell FBO2 = row5.createCell(1);
			FBO1.setCellValue("FBO");
			FBO2.setCellValue(five.size());
			
			Row row6 = newSheet.createRow(6);
			Cell CPPM1 = row6.createCell(0);
			Cell CPPM2 = row6.createCell(1);
			CPPM1.setCellValue("CPPM");
			CPPM2.setCellValue(six.size());

			Row row7 = newSheet.createRow(7);
			Cell AIBI1 = row7.createCell(0);
			Cell AIBI2 = row7.createCell(1);
			AIBI1.setCellValue("AIBI");
			AIBI2.setCellValue(seven.size());
			
			Row row8 = newSheet.createRow(8);
			Cell PROCUREMENT1 = row8.createCell(0);
			Cell PROCUREMENT2 = row8.createCell(1);
			PROCUREMENT1.setCellValue("PROCUREMENT");
			PROCUREMENT2.setCellValue(eight.size());
			
			Row row9 = newSheet.createRow(9);
			Cell DSE1 = row9.createCell(0);
			Cell DSE2 = row9.createCell(1);
			DSE1.setCellValue("DSE-Comerrcial");
			DSE2.setCellValue(nine.size());
			
			Row row10 = newSheet.createRow(10);
			Cell ARCHITECTURE1 = row10.createCell(0);
			Cell ARCHITECTURE2 = row10.createCell(1);
			ARCHITECTURE1.setCellValue("ARCHITECTURE");
			ARCHITECTURE2.setCellValue(ten.size());
			
			Row row11 = newSheet.createRow(11);
			Cell HRCFS1= row11.createCell(0);
			Cell HRCFS2 = row11.createCell(1);
			HRCFS1.setCellValue("HRCFS");
			HRCFS2.setCellValue(eleven.size());

			Row row12 = newSheet.createRow(12);
			Cell GPD1= row12.createCell(0);
			Cell GPD2 = row12.createCell(1);
			GPD1.setCellValue("GPD");
			GPD2.setCellValue(twelve.size());

			Row row13 = newSheet.createRow(13);
			Cell ECS1= row13.createCell(0);
			Cell ECS2= row13.createCell(1);
			ECS1.setCellValue("ECS");
			ECS2.setCellValue(thirteen.size());

			System.out.println(one);
			
	}

	private ArrayList<ArrayList<String>> get_confluence_users(int startAt, ArrayList<String> one, ArrayList<String> two, ArrayList<ArrayList<String>> Container, String groupName) throws IOException, JSONException {
	
		

		String confgroupApi = "https://confluence.pfizer.com/rest/api/group/"+groupName+"/member?start="+startAt;
		URL groupUrl = new URL(confgroupApi);
		HttpURLConnection conn = null;
		TrustAllCertificates.install();
	  	conn = (HttpURLConnection) groupUrl.openConnection();
	  	String authString = login + ":" + password;
	  	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		
		conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
		String inputLine = "";
		BufferedReader in = null;
			System.out.println("Authentication process");
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		  	StringBuffer response1 = new StringBuffer();
		  while((inputLine = in.readLine()) != null)
		  {
			  response1.append(inputLine);
		  }
		in.close();
		
		
		 JSONObject json  = new JSONObject(response1.toString());
		 JSONArray result = json.getJSONArray("results");
	     
	     for(int i=0; i<result.length(); i++)
	     {
	    	
	    	 JSONObject result1 = result.getJSONObject(i);
	    	 one.add(result1.getString("username"));
	    	 two.add(result1.getString("displayName"));
	     }
	     
	     Container.add(one);
	     Container.add(two);
		return Container;
	}
	

	private ArrayList<ArrayList<String>> getLoginAndFlagFromDB(Connection con, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six, ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine,  ArrayList<String> ten, ArrayList<String> eleven, ArrayList<String> twelve, ArrayList<String> thirteen, ArrayList<String> AllinOne) throws SQLException {
		// TODO Auto-generated method stub
		
		ArrayList<String> PGSFlag = new ArrayList<>();
		ArrayList<String> PGSLogin = new ArrayList<>();
		
		ArrayList<String> RNDFlag = new ArrayList<>();
		ArrayList<String> RNDLogin = new ArrayList<>();
		
		ArrayList<String> GISFlag = new ArrayList<>();
		ArrayList<String> GISLogin = new ArrayList<>();
		
		ArrayList<String> RREMSFlag = new ArrayList<>();
		ArrayList<String> RREMSLogin = new ArrayList<>();
		
		ArrayList<String> FBOFlag = new ArrayList<>();
		ArrayList<String> FBOLogin = new ArrayList<>();

		ArrayList<String> CPPMFlag = new ArrayList<>();
		ArrayList<String> CPPMLogin = new ArrayList<>();

		ArrayList<String> AIBIFlag = new ArrayList<>();
		ArrayList<String> AIBILogin = new ArrayList<>();
		
		ArrayList<String> PROCUREMENTFlag = new ArrayList<>();
		ArrayList<String> PROCUREMENTLogin = new ArrayList<>();

		ArrayList<String> DSEFlag = new ArrayList<>();
		ArrayList<String> DSELogin = new ArrayList<>();
		
		ArrayList<String> ARCHITECTUREFlag = new ArrayList<>();
		ArrayList<String> ARCHITECTURELogin = new ArrayList<>();
		
		ArrayList<String> HRCFSFlag = new ArrayList<>();
		ArrayList<String> HRCFSLogin = new ArrayList<>();

		ArrayList<String> GPDFlag = new ArrayList<>();
		ArrayList<String> GPDLogin = new ArrayList<>();
		
		ArrayList<String> ECSFlag = new ArrayList<>();
		ArrayList<String> ECSLogin = new ArrayList<>();

		//For Full user list.
		ArrayList<String> FullUserName = new ArrayList<>();		
		ArrayList<String> FullName = new ArrayList<>();
		ArrayList<String> FullFlag = new ArrayList<>();
		ArrayList<String> FullLogin = new ArrayList<>();

		System.out.println("list size "+one.size());
		String PGSquery = "'"+one.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s1 = con.createStatement();
		ResultSet PGSrs = s1.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+PGSquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(PGSrs.next()){
			PGSFlag.add(PGSrs.getString(2));
			PGSLogin.add(PGSrs.getString(3));
		}
//	
		System.out.println("list size "+two.size());
		String RNDquery = "'"+two.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s2 = con.createStatement();
		ResultSet RNDrs = s2.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+RNDquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(RNDrs.next()){
			RNDFlag.add(RNDrs.getString(2));
			RNDLogin.add(RNDrs.getString(3));
		}
//	
		System.out.println("list size "+three.size());
		String GISquery = "'"+three.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s3 = con.createStatement();
		ResultSet GISrs = s3.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+GISquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(GISrs.next()){
			GISFlag.add(GISrs.getString(2));
			GISLogin.add(GISrs.getString(3));
		}
//		
		System.out.println("list size "+four.size());
		String RREMSquery = "'"+four.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s4 = con.createStatement();
		ResultSet RREMSrs = s4.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+RREMSquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(RREMSrs.next()){
			RREMSFlag.add(RREMSrs.getString(2));
			RREMSLogin.add(RREMSrs.getString(3));
		}
//		
		System.out.println("list size "+five.size());
		String FBOquery = "'"+five.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s5 = con.createStatement();
		ResultSet FBOrs = s5.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+FBOquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(FBOrs.next()){
			FBOFlag.add(FBOrs.getString(2));
			FBOLogin.add(FBOrs.getString(3));
		}
//
		String CPPMquery = "'"+six.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s6 = con.createStatement();
		ResultSet CPPMrs = s6.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+CPPMquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(CPPMrs.next()){
			CPPMFlag.add(CPPMrs.getString(2));
			CPPMLogin.add(CPPMrs.getString(3));
		}
//
		String AIBIquery = "'"+seven.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s7 = con.createStatement();
		ResultSet AIBIrs = s7.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+AIBIquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(AIBIrs.next()){
			AIBIFlag.add(AIBIrs.getString(2));
			AIBILogin.add(AIBIrs.getString(3));
		}
//
		String PROCUREMENTquery = "'"+eight.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s8 = con.createStatement();
		ResultSet PROCUREMENTrs = s8.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+PROCUREMENTquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(PROCUREMENTrs.next()){
			PROCUREMENTFlag.add(PROCUREMENTrs.getString(2));
			PROCUREMENTLogin.add(PROCUREMENTrs.getString(3));
		}
//
		String DSEquery = "'"+nine.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s9 = con.createStatement();
		ResultSet DSErs = s9.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+DSEquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(DSErs.next()){
			DSEFlag.add(DSErs.getString(2));
			DSELogin.add(DSErs.getString(3));
		}
		
//
		String ARCHITECTUREquery = "'"+ten.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s10 = con.createStatement();
		ResultSet ARCHITECTURErs = s10.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+ARCHITECTUREquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(ARCHITECTURErs.next()){
			ARCHITECTUREFlag.add(ARCHITECTURErs.getString(2));
			ARCHITECTURELogin.add(ARCHITECTURErs.getString(3));
		}
//
		
		String HRCFSquery = "'"+eleven.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s11 = con.createStatement();
		ResultSet HRCFSrs = s11.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+HRCFSquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(HRCFSrs.next()){
			HRCFSFlag.add(HRCFSrs.getString(2));
			HRCFSLogin.add(HRCFSrs.getString(3));
		}
//
		
		String GPDquery = "'"+twelve.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s12 = con.createStatement();
		ResultSet GPDrs = s12.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+GPDquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(GPDrs.next()){
			GPDFlag.add(GPDrs.getString(2));
			GPDLogin.add(GPDrs.getString(3));
		}
//
		String ECSquery = "'"+thirteen.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement s13 = con.createStatement();
		ResultSet ECSrs = s13.executeQuery("SELECT DISTINCT u.lower_user_name,u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+ECSquery.toString()+") GROUP BY u.lower_user_name ,u.active ORDER by u.lower_user_name;");
		while(ECSrs.next()){
			ECSFlag.add(ECSrs.getString(2));
			ECSLogin.add(ECSrs.getString(3));
		}

		
		// Full List
		Collections.sort(AllinOne);
		ArrayList<String> FinalAllUsernameList = (ArrayList<String>) AllinOne.stream().distinct().collect(Collectors.toList());
		System.out.println("Removed duplicated "+FinalAllUsernameList.size());
		
		
		System.out.println("After REmoving duplicate "+FinalAllUsernameList);
		String Fullquery = "'"+FinalAllUsernameList.toString().replace("[","").replace("]", "").replace(" ","").replace(",","','")+"'";
		
		Statement Fulls9 = con.createStatement();
		ResultSet Fullrs = Fulls9.executeQuery("SELECT DISTINCT u.lower_user_name, u.display_name, u.active,MAX(l.SUCCESSDATE)FROM cwd_user u JOIN cwd_membership m ON u.id = child_user_id JOIN cwd_group g ON m.parent_id = g.id JOIN cwd_directory d ON u.directory_id = d.id LEFT OUTER JOIN user_mapping map ON map.username = u.user_name LEFT OUTER JOIN logininfo l ON l.USERNAME = map.user_key WHERE u.lower_user_name IN("+Fullquery.toString()+") GROUP BY u.lower_user_name, u.display_name,u.active ORDER by u.lower_user_name;");
		while(Fullrs.next()){
			FullUserName.add(Fullrs.getString(1));
			FullName.add(Fullrs.getString(2));
			FullFlag.add(Fullrs.getString(3));
			FullLogin.add(Fullrs.getString(4));
		}
		
//		System.out.println("Out of class "+FullUserName.toString());
//		System.out.println("Out of class "+FullName.toString());
//		System.out.println("Out of class "+FullFlag.toString());
//		System.out.println("Out of class "+FullLogin.toString());

		ArrayList<ArrayList<String>> al = new ArrayList<>();
		al.add(PGSFlag);
		al.add(PGSLogin);
		
//RND
		al.add(RNDFlag);
		al.add(RNDLogin);
// GIS
		al.add(GISFlag);
		al.add(GISLogin);
// RREMS
		al.add(RREMSFlag);
		al.add(RREMSLogin);
//FBO
		al.add(FBOFlag);
		al.add(FBOLogin);
// CPPM		
		al.add(CPPMFlag);
		al.add(CPPMLogin);
//AIBI
		al.add(AIBIFlag);
		al.add(AIBILogin);
//PROCUREMENET
		al.add(PROCUREMENTFlag);
		al.add(PROCUREMENTLogin);
//DSE
		al.add(DSEFlag);
		al.add(DSELogin);
//ARCHITECTURE
		al.add(ARCHITECTUREFlag);
		al.add(ARCHITECTURELogin);
//HRCFS
		al.add(HRCFSFlag);
		al.add(HRCFSLogin);
//GPD
		al.add(GPDFlag);
		al.add(GPDLogin);	
//ECS
		al.add(ECSFlag);
		al.add(ECSLogin);	
		
//Full list
		al.add(FullUserName);
		al.add(FullName);
		al.add(FullFlag);
		al.add(FullLogin);
		
		return al;
	}

	public void WriteData(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, String PGSTEXT) throws IOException, ParserConfigurationException
 	{
		String filename = "Confluence_org_report.xlsx";
     	String outputFile= "/usr/local/Enhanced/reports/"+filename;
     	//String outputFile = "D:\\Users\\"+filename;
		newSheet = newWorkbook.createSheet(PGSTEXT);
 		DocumentBuilder db = dbf.newDocumentBuilder();

 		String  costCenter1=null, costCenterDescription2 = null, supervisorId3 = null, supervisorName4 = null, departmentLevel1Id5 = null, departmentLevel1Description6= null, departmentLevel2Id7 = null, departmentLevel2Description8 = null;
 		String departmentLevel3Id9 = null, departmentLevel3Description10 = null, departmentLevel4Id11 = null, departmentLevel4Description12 = null, departmentLevel5Id13 = null, departmentLevel5Description14 = null, departmentLevel6Id15 = null, departmentLevel6Description16 = null, activePfizer17 = null;
 		String EmailAddress= null;
 		System.out.println("Sheet Created");
		String Columns[] = new String[] {"Username", "Full Name", "Email Address", "Cost Center", "Cost Center Description", "Supervisor Id", "Supervisor Name", "Department Level1 Id","Department Level1 Description", "Department Level2 Id", "Department Level2 Description", "Department Level3 Id", "Department Level3 Description", "Department Level4 Id", "Department Level4 Description", "Department Level5 Id", "Department Level5 Description", "Department Level6 Id", "Department Level6 Description", "Active in Pfizer", "Active in Confluence", "Last Login"};     
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
			
		Row headerRow = newSheet.createRow(0);
			
		for(int columnI=0; columnI<Columns.length; columnI++)
		{ 
			Cell cell = headerRow.createCell(columnI);	
			cell.setCellValue(Columns[columnI]);	
			cell.setCellStyle(headerCellStyle); 
			newSheet.autoSizeColumn(columnI);	
		}
 		String input, url;
		URL urli;
		URLConnection conn; 
		Document doc = null;
		int fork = 1;
		
		 for(int ntid =0 ; ntid<one.size();ntid++)
		 {   
			input = one.get(ntid).toString();
			url = "http://researchservice.pfizer.com/ResearchServicePeople/person/ntid/";
			url = url.concat(input);
		    
//		    System.out.println(ntid+ "Running");
//		    System.out.println(input);
//		        
			Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {          
				return new PasswordAuthentication(Researchlogin, Researchpass.toCharArray());
			}
				});
			
			 try {
		    	urli = new URL(url);
		    	conn = urli.openConnection();
		    	conn.setReadTimeout(50000);
		    	conn.setRequestProperty("Accept-Encoding", "gzip");
		    	conn.setRequestProperty("Accept", "application/xml");
		        conn.setRequestProperty("Content-Type", "application/xml");
		        conn.setUseCaches(true);
		        
					try {
						doc = db.parse(conn.getInputStream());
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        
					Node pfizerActivenode = doc.getElementsByTagName("person").item(0);
			        NodeList newlist = pfizerActivenode.getChildNodes();
			       Node ank = null;
			        
			        
		   for(int k=0; k<newlist.getLength(); k++)
		     {
		     	ank = newlist.item(k);
				if(Objects.equals("activeIndicator", ank.getNodeName()))
		     	{
		     		activePfizer17 = ank.getChildNodes().item(0).getTextContent();
		     	}
				
				if(Objects.equals("emailAddress", ank.getNodeName()))
		     	{
					EmailAddress = ank.getChildNodes().item(0).getTextContent();
		     	}
		     }
		//	activePfizer17 = doc.getElementsByTagName("activeIndicator").item(0).getTextContent();


	        Node n = doc.getElementsByTagName("organization").item(0);
	        NodeList nl = n.getChildNodes();
	        Node an = null;    	
	   	         
	        for (int i=1; i < nl.getLength(); i++) {
	        	an = nl.item(i);
	        	
	            if(Objects.equals("costCenter", an.getNodeName())) 
	            {
	            	
	            	costCenter1 = an.getChildNodes().item(0).getTextContent();
	            	//System.out.println(costCenter1);
	            }
	            
	            if(Objects.equals("costCenterDescription", an.getNodeName())) 
	            {        	
	            	costCenterDescription2 = an.getChildNodes().item(0).getTextContent();			
	            }
	            
	            if(Objects.equals("supervisorId", an.getNodeName())) 
	            {            		
	            	supervisorId3 = an.getChildNodes().item(0).getTextContent(); 					
	            }
	            
	            if(Objects.equals("supervisorName", an.getNodeName())) 
	            {            	
	            	supervisorName4 = an.getChildNodes().item(0).getTextContent();					
	            }
	            
	            if(Objects.equals("departmentLevel1Id", an.getNodeName())) 
	            {			
	            	departmentLevel1Id5 = an.getChildNodes().item(0).getTextContent();				
	            }            
	            
	            if(Objects.equals("departmentLevel1Description", an.getNodeName())) 
	            {	
	            	departmentLevel1Description6 = an.getChildNodes().item(0).getTextContent();		
	            }
	            
	            if(Objects.equals("departmentLevel2Id", an.getNodeName())) 
	            { 
	            	departmentLevel2Id7 = an.getChildNodes().item(0).getTextContent();				
	            }
	            
	            if(Objects.equals("departmentLevel2Description", an.getNodeName())) 
	            {
	            	departmentLevel2Description8 = an.getChildNodes().item(0).getTextContent();		
	            }
	            
	            if(Objects.equals("departmentLevel3Id", an.getNodeName()))
	            {			
	            	departmentLevel3Id9 = an.getChildNodes().item(0).getTextContent();
	            }    
	            
	            if(Objects.equals("departmentLevel3Description", an.getNodeName())) 
	            {
	            	departmentLevel3Description10 = an.getChildNodes().item(0).getTextContent();	
	            }
	            
	            if(Objects.equals("departmentLevel4Id", an.getNodeName()))
	            {            
	            	departmentLevel4Id11 = an.getChildNodes().item(0).getTextContent();				
	            }
	            
	            if(Objects.equals("departmentLevel4Description", an.getNodeName())) 
	            {	
	            	departmentLevel4Description12 = an.getChildNodes().item(0).getTextContent();	
	            }
	            
	            if(Objects.equals("departmentLevel5Id", an.getNodeName())) 
	            {        
	            	departmentLevel5Id13 = an.getChildNodes().item(0).getTextContent();				
	            }

	            if(Objects.equals("departmentLevel5Description", an.getNodeName())) 
	            {
	            	departmentLevel5Description14 = an.getChildNodes().item(0).getTextContent();	
	            }
	            if(Objects.equals("departmentLevel6Id", an.getNodeName())) 
	            {
	            	departmentLevel6Id15 = an.getChildNodes().item(0).getTextContent();				
	            }
	            
	            if(Objects.equals("departmentLevel6Description", an.getNodeName())) 
	            {
	            	departmentLevel6Description16 = an.getChildNodes().item(0).getTextContent();				
	            }
	            	     	
	    }
	        Row row = newSheet.createRow(fork);
	        fork++;
        	row.createCell(0).setCellValue(input);
        	row.createCell(1).setCellValue(two.get(ntid).toString());
        	row.createCell(2).setCellValue(EmailAddress);	
     		row.createCell(3).setCellValue(costCenter1);
     		row.createCell(4).setCellValue(costCenterDescription2);
     		row.createCell(5).setCellValue(supervisorId3);
     		row.createCell(6).setCellValue(supervisorName4);
     		row.createCell(7).setCellValue(departmentLevel1Id5);
     		row.createCell(8).setCellValue(departmentLevel1Description6);
     		row.createCell(9).setCellValue(departmentLevel2Id7);
     		row.createCell(10).setCellValue(departmentLevel2Description8);
     		row.createCell(11).setCellValue(departmentLevel3Id9);
     		row.createCell(12).setCellValue(departmentLevel3Description10);
     		row.createCell(13).setCellValue(departmentLevel4Id11);
     		row.createCell(14).setCellValue(departmentLevel4Description12);
     		row.createCell(15).setCellValue(departmentLevel5Id13);
      		row.createCell(16).setCellValue(departmentLevel5Description14);
      		row.createCell(17).setCellValue(departmentLevel6Id15);
     		row.createCell(18).setCellValue(departmentLevel6Description16);
     		row.createCell(19).setCellValue(activePfizer17);
     		row.createCell(20).setCellValue(three.get(ntid));
     		row.createCell(21).setCellValue(four.get(ntid));
             	   
     		FileOutputStream oFile = new FileOutputStream(outputFile); 
     		newWorkbook.write(oFile);
     		
		 	
	 	costCenter1 = "";costCenterDescription2= ""; supervisorId3= ""; supervisorName4= ""; departmentLevel1Id5 = ""; departmentLevel1Description6 = "";  departmentLevel2Id7 = ""; departmentLevel2Description8 = ""; departmentLevel3Id9= "";  departmentLevel3Description10 = ""; departmentLevel4Id11= ""; departmentLevel4Description12=""; departmentLevel5Id13= ""; departmentLevel5Description14=""; departmentLevel6Id15 = ""; departmentLevel6Description16 = ""; activePfizer17="";
		 }
		 catch (Exception e) {

			  String blank = "N/A";
			    if(newSheet.getRow(fork) == null)
			    {
			    	
					System.out.println("null row at "+ntid);
					Row row = newSheet.createRow(fork);
					  fork++;
					
					row.createCell(0).setCellValue(input);
	         		row.createCell(1).setCellValue(two.get(ntid).toString());
	         		row.createCell(2).setCellValue(blank);
	         		row.createCell(3).setCellValue(blank);
	         		row.createCell(4).setCellValue(blank);
	         		row.createCell(5).setCellValue(blank);
	         		row.createCell(6).setCellValue(blank);
	         		row.createCell(7).setCellValue(blank);
	         		row.createCell(8).setCellValue(blank);
	         		row.createCell(9).setCellValue(blank);
	         		row.createCell(10).setCellValue(blank);
	         		row.createCell(11).setCellValue(blank);
	         		row.createCell(12).setCellValue(blank);
	         		row.createCell(13).setCellValue(blank);
	         		row.createCell(14).setCellValue(blank);
	         		row.createCell(15).setCellValue(blank);
	         		row.createCell(16).setCellValue(blank);
	         		row.createCell(17).setCellValue(blank);
	         		row.createCell(18).setCellValue(blank);
	         		row.createCell(19).setCellValue(blank);
	         		row.createCell(20).setCellValue(blank);
	         		row.createCell(21).setCellValue(four.get(ntid));

			    }
			  
		}	
		 
		 }

			 
		
		
		
		       
 	}


}
