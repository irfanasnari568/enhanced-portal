

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ConfluenceUserGroup
 */
@WebServlet("/ConfluenceUserGroup")
public class ConfluenceUserGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public ConfluenceUserGroup() {
        super();
        // TODO Auto-generated constructor stub
    }

	String userhome = System.getProperty("user.home");
	
	

	String login= "bc8c2e2b59c44e8497a279fa85848fb3";
	String password= "a03f507332514bddBf02288759961A1c";
	
	XSSFWorkbook newWorkbook = new XSSFWorkbook();
	
  
	String authString = login + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ConfluenceUserGroup confluence = new ConfluenceUserGroup();
		XSSFSheet PGSSheet = null,  RNDSheet = null, SUMMARYSHEET = null;
		XSSFSheet GISSheet = null, RREMSSheet = null, FBOSheet = null, CPPMSheet = null, AIBISheet = null, PROCUREMENTSheet = null, DSESheet = null, ARCHITECTURESheet = null, HRCFSSheet = null, GPDSheet = null, ECSSheet = null ;
		String PGSTEXT = "PGS-User List";
		String RNDTEXT = "RND-User List";
		String GISTEXT = "GIS-User List";
		String RREMSTEXT = "RREMS-User List";
		String FBOTEXT = "FBO-User List";
	    String CPPMTEXT = "CPPM-User List";
	    String AIBITEXT = "AIBI-User List";
	    String PROCUREMENTTEXT = "PROCUREMENT-USER List";
	    String DSETEXT = "DSE-COMMERCIAL-USER List";
	    String ARCHITECTURETEXT = "ARCHITECTURE-User List";
	    String HRCFSTEXT = "HRCFS-User List";
	    String GPDTEXT = "GPD-User List";
	    String ECSTEXT = "ECS-User List";
	    String SUMMARYTEXT = "SUMMARY";
		
	    String PGSGROUP= "confprodpgs", RNDGROUP = "confprodrnd" ,GISGROUP= "confprod", RREMSGROUP="confprodrrems", FBOGROUP="confprodfbo";
		String CPPMGROUP ="confprodcppm", AIBIGROUP="confprodaibi", PROCUREMENTGROUP="confprodprocurement" , DSEGROUP = "confproddse-commercial";
		String ARCHITECTUREGROUP = "confprodarchitecture", HRCFSGROUP = "confprodhrcfs", GPDGROUP = "Confprodgpd", ECSGROUP = "Confprodecs";
		
		int startAt = 0;
		int max = 200;
		ArrayList<ArrayList<String>> PGSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> PGSParent = new ArrayList<>();
				
		ArrayList<String> one = new ArrayList<>();
		ArrayList<String> two = new ArrayList<>();
		
		  
		try {
			PGSParent = confluence.get_confluence_users(startAt, one, two, PGSContainer, PGSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 one = PGSParent.get(0);
		 two = PGSParent.get(1);
		
		while(one.size() >= max)
		{
			System.out.println("Increasing list "+one.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				PGSParent = confluence.get_confluence_users(startAt, one, two, PGSContainer, PGSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total PGS Users "+one.size());

		// PGS END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> RNDContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> RNDParent = new ArrayList<>();
				
		ArrayList<String> oneRnd = new ArrayList<>();
		ArrayList<String> twoRnd = new ArrayList<>();
		
		  
		try {
			RNDParent = confluence.get_confluence_users(startAt, oneRnd, twoRnd, RNDContainer, RNDGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneRnd = RNDParent.get(0);
		 twoRnd = RNDParent.get(1);
		
		while(oneRnd.size() >= max)
		{
			System.out.println("Increasing list "+oneRnd.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				RNDParent = confluence.get_confluence_users(startAt, oneRnd, twoRnd, RNDContainer, RNDGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total RND Users "+oneRnd.size());

		// RND END HERE
		
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> GISContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> GISParent = new ArrayList<>();
				
		ArrayList<String> oneGis = new ArrayList<>();
		ArrayList<String> twoGis = new ArrayList<>();
		
		  
		try {
			GISParent = confluence.get_confluence_users(startAt, oneGis, twoGis, GISContainer, GISGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneGis = GISParent.get(0);
		 twoGis = GISParent.get(1);
		
		while(oneGis.size() >= max)
		{
			System.out.println("Increasing list "+oneGis.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				GISParent = confluence.get_confluence_users(startAt, oneGis, twoGis, GISContainer, GISGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total GIS Users "+oneGis.size());

		// GIS END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> RREMSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> RREMSParent = new ArrayList<>();
				
		ArrayList<String> oneRrems = new ArrayList<>();
		ArrayList<String> twoRrems = new ArrayList<>();
		
		  
		try {
			RREMSParent = confluence.get_confluence_users(startAt, oneRrems, twoRrems, RREMSContainer, RREMSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneRrems= RREMSParent.get(0);
		 twoRrems = RREMSParent.get(1);
		
		while(oneRrems.size() >= max)
		{
			System.out.println("Increasing list "+oneRrems.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				RREMSParent = confluence.get_confluence_users(startAt, oneRrems, twoRrems, RREMSContainer, RREMSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total RREMS Users "+oneRrems.size());

		// RREMS END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> FBOContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> FBOParent = new ArrayList<>();
				
		ArrayList<String> oneFbo = new ArrayList<>();
		ArrayList<String> twoFbo = new ArrayList<>();
		
		  
		try {
			FBOParent = confluence.get_confluence_users(startAt, oneFbo, twoFbo, FBOContainer, FBOGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneFbo = FBOParent.get(0);
		 twoFbo = FBOParent.get(1);
		
		while(oneFbo.size() >= max)
		{
			System.out.println("Increasing list "+oneFbo.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				FBOParent = confluence.get_confluence_users(startAt, oneFbo, twoFbo, FBOContainer, FBOGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total FBO Users "+oneFbo.size());

		// FBO END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> CPPMContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> CPPMParent = new ArrayList<>();
				
		ArrayList<String> oneCppm = new ArrayList<>();
		ArrayList<String> twoCppm = new ArrayList<>();
		
		  
		try {
			CPPMParent = confluence.get_confluence_users(startAt, oneCppm, twoCppm, CPPMContainer, CPPMGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneCppm = CPPMParent.get(0);
		 twoCppm = CPPMParent.get(1);
		
		while(oneCppm.size() >= max)
		{
			System.out.println("Increasing list "+oneCppm.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				CPPMParent = confluence.get_confluence_users(startAt, oneCppm, twoCppm, CPPMContainer, CPPMGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total CPPM Users "+oneCppm.size());

		// CPPM END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> AIBIContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> AIBIParent = new ArrayList<>();
				
		ArrayList<String> oneAibi = new ArrayList<>();
		ArrayList<String> twoAibi = new ArrayList<>();
		
		  
		try {
			AIBIParent = confluence.get_confluence_users(startAt, oneAibi, twoAibi, AIBIContainer, AIBIGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneAibi = AIBIParent.get(0);
		 twoAibi = AIBIParent.get(1);
		
		while(oneAibi.size() >= max)
		{
			System.out.println("Increasing list "+oneAibi.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				AIBIParent = confluence.get_confluence_users(startAt, oneAibi, twoAibi, AIBIContainer, AIBIGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total AIBI Users "+oneAibi.size());

		// AIBI END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> PROCUREMENTContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> PROCUREMENTParent = new ArrayList<>();
				
		ArrayList<String> oneProcurement = new ArrayList<>();
		ArrayList<String> twoProcurement = new ArrayList<>();
		
		  
		try {
			PROCUREMENTParent = confluence.get_confluence_users(startAt, oneProcurement, twoProcurement, PROCUREMENTContainer, PROCUREMENTGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneProcurement = PROCUREMENTParent.get(0);
		 twoProcurement = PROCUREMENTParent.get(1);
		
		while(oneProcurement.size() >= max)
		{
			System.out.println("Increasing list "+oneProcurement.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				PROCUREMENTParent = confluence.get_confluence_users(startAt, oneProcurement, twoProcurement, PROCUREMENTContainer, PROCUREMENTGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total PROCUREMENT Users "+oneProcurement.size());

		// PROCUREMENT END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> DSEContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> DSEParent = new ArrayList<>();
				
		ArrayList<String> oneDse = new ArrayList<>();
		ArrayList<String> twoDse = new ArrayList<>();
		
		  
		try {
			DSEParent = confluence.get_confluence_users(startAt, oneDse, twoDse, DSEContainer, DSEGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 oneDse = DSEParent.get(0);
		 twoDse = DSEParent.get(1);
		
		while(oneDse.size() >= max)
		{
			System.out.println("Increasing list "+oneDse.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				DSEParent = confluence.get_confluence_users(startAt, oneDse, twoDse, DSEContainer, DSEGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total DSE Users "+oneDse.size());

		// DSE END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> ARCHITECTUREContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> ARCHITECTUREParent = new ArrayList<>();
				
		ArrayList<String> oneArchitecture = new ArrayList<>();
		ArrayList<String> twoArchitecture = new ArrayList<>();
		
		  
		try {
			ARCHITECTUREParent = confluence.get_confluence_users(startAt, oneArchitecture, twoArchitecture, ARCHITECTUREContainer, ARCHITECTUREGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneArchitecture = ARCHITECTUREParent.get(0);
		twoArchitecture = ARCHITECTUREParent.get(1);
		
		while(oneArchitecture.size() >= max)
		{
			System.out.println("Increasing list "+oneArchitecture.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				ARCHITECTUREParent = confluence.get_confluence_users(startAt, oneArchitecture, twoArchitecture, ARCHITECTUREContainer, ARCHITECTUREGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total ARCHITECTURE Users "+oneArchitecture.size());

		// ARCHITECTURE END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> HRCFSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> HRCFSParent = new ArrayList<>();
				
		ArrayList<String> oneHrcfs = new ArrayList<>();
		ArrayList<String> twoHrcfs = new ArrayList<>();
		
		  
		try {
			HRCFSParent = confluence.get_confluence_users(startAt, oneHrcfs, twoHrcfs, HRCFSContainer, HRCFSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneHrcfs = HRCFSParent.get(0);
		twoHrcfs = HRCFSParent.get(1);
		
		while(oneHrcfs.size() >= max)
		{
			System.out.println("Increasing list "+oneHrcfs.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				HRCFSParent = confluence.get_confluence_users(startAt, oneHrcfs, twoHrcfs, HRCFSContainer, HRCFSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total HRCFS Users "+oneHrcfs.size());

		// HRCFS END HERE

		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> GPDContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> GPDParent = new ArrayList<>();
				
		ArrayList<String> oneGpd = new ArrayList<>();
		ArrayList<String> twoGpd = new ArrayList<>();
		
		  
		try {
			GPDParent = confluence.get_confluence_users(startAt, oneGpd, twoGpd, GPDContainer, GPDGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneGpd = GPDParent.get(0);
		twoGpd = GPDParent.get(1);
		
		while(oneGpd.size() >= max)
		{
			System.out.println("Increasing list "+oneGpd.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				GPDParent = confluence.get_confluence_users(startAt, oneGpd, twoGpd, GPDContainer, GPDGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total GPD Users "+oneGpd.size());

		// GPD END HERE
		
		startAt = 0;
		max = 200;
		ArrayList<ArrayList<String>> ECSContainer = new ArrayList<>();
		ArrayList<ArrayList<String>> ECSParent = new ArrayList<>();
				
		ArrayList<String> oneEcs = new ArrayList<>();
		ArrayList<String> twoEcs = new ArrayList<>();
		
		  
		try {
			ECSParent = confluence.get_confluence_users(startAt, oneEcs, twoEcs, ECSContainer, ECSGROUP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		oneEcs = ECSParent.get(0);
		twoEcs = ECSParent.get(1);
		
		while(oneEcs.size() >= max)
		{
			System.out.println("Increasing list "+oneEcs.size());
			startAt= startAt+200;
			max = max + 200;
			try {
				ECSParent = confluence.get_confluence_users(startAt, oneEcs, twoEcs, ECSContainer, ECSGROUP);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total ECS Users "+oneEcs.size());

		// ECS END HERE
		
		confluence.writeSummary(SUMMARYSHEET, one, oneRnd, oneGis, oneRrems, oneFbo, oneCppm, oneAibi, oneProcurement, oneDse, oneArchitecture, oneHrcfs, oneGpd, oneEcs, SUMMARYTEXT);
		
		confluence.writeData(PGSSheet, one, two, PGSTEXT);
		confluence.writeData(RNDSheet, oneRnd, twoRnd, RNDTEXT);
		confluence.writeData(GISSheet, oneGis, twoGis, GISTEXT);
		confluence.writeData(RREMSSheet, oneRrems, twoRrems, RREMSTEXT);
		confluence.writeData(FBOSheet, oneFbo, twoFbo, FBOTEXT);
		confluence.writeData(CPPMSheet, oneCppm, twoCppm, CPPMTEXT);
		confluence.writeData(AIBISheet, oneAibi, twoAibi, AIBITEXT);
		confluence.writeData(PROCUREMENTSheet, oneProcurement, twoProcurement, PROCUREMENTTEXT);
		confluence.writeData(DSESheet, oneDse, twoDse, DSETEXT);
		confluence.writeData(ARCHITECTURESheet, oneArchitecture, twoArchitecture, ARCHITECTURETEXT);
		try {
		confluence.writeData(HRCFSSheet, oneHrcfs, twoHrcfs, HRCFSTEXT);
		confluence.writeData(GPDSheet, oneGpd, twoGpd, GPDTEXT);
		confluence.writeData(ECSSheet, oneEcs, twoEcs, ECSTEXT);
		}catch (Exception e) {
			// TODO: handle exception
		}

		String filename = "Confluence_user_report.xlsx";
		String outputFile= "/usr/local/Enhanced/reports/"+filename;
  		//String outputFile= "D:\\Users\\"+filename;
  		
  		File file = new File(outputFile);

		if(file.exists())
		{
			response.setContentType("application/vnd.ms-excel");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
			System.out.println("filename new  "+filename);
			
			BufferedInputStream in=null;
			ServletOutputStream outs=null;
			byte[] buffer = new byte[1024]; 
		
			
			try {
				int g = 0;
				in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
				outs=response.getOutputStream();
				while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
					outs.write(buffer, 0, g);  
				}  
				outs.flush();  

			} catch (IOException ioe) {
				ioe.printStackTrace(System.out);
			}

			System.out.println("Done !!");

			outs.close();
			in.close();
			
		}
		else
		{
			System.out.println("not found");
		}
	}
	
	

	public void writeSummary(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, ArrayList<String> three, ArrayList<String> four, ArrayList<String> five, ArrayList<String> six, ArrayList<String> seven, ArrayList<String> eight, ArrayList<String> nine, ArrayList<String> ten, ArrayList<String> eleven, ArrayList<String> twelve, ArrayList<String> thirteen, String text) throws IOException
	{
		//XSSFSheet newwSheet = newSheet;
		newSheet = newWorkbook.createSheet(text);
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		System.out.println("From summary tab");
		Row headerRow = newSheet.createRow(0);
		
			Cell headerCell1 = headerRow.createCell(0);	
			Cell headerCell2 = headerRow.createCell(1);	
			Cell headerCell3 = headerRow.createCell(2);	


			headerCell1.setCellValue("Business Line");	
			headerCell1.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(0);
			headerCell2.setCellValue("Count"); 
			headerCell2.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(1);
			headerCell3.setCellValue("($)Dollar"); 
			headerCell3.setCellStyle(headerCellStyle);
			newSheet.autoSizeColumn(2);

			//
			Row row1 = newSheet.createRow(1);
			Cell PGS1 = row1.createCell(0);
			Cell PGS2 = row1.createCell(1);
			PGS1.setCellValue("PGS");
			PGS2.setCellValue(one.size());
			
			Row row2 = newSheet.createRow(2);
			Cell RND1 = row2.createCell(0);
			Cell RND2 = row2.createCell(1);
			RND1.setCellValue("RND");
			RND2.setCellValue(two.size());

			
			Row row3 = newSheet.createRow(3);
			Cell GIS1 = row3.createCell(0);
			Cell GIS2 = row3.createCell(1);
			GIS1.setCellValue("GIS");
			GIS2.setCellValue(three.size());
			
			Row row4 = newSheet.createRow(4);
			Cell RREMS1 = row4.createCell(0);
			Cell RREMS2 = row4.createCell(1);
			RREMS1.setCellValue("RREMS");
			RREMS2.setCellValue(four.size());
			
			Row row5 = newSheet.createRow(5);
			Cell FBO1 = row5.createCell(0);
			Cell FBO2 = row5.createCell(1);
			FBO1.setCellValue("FBO");
			FBO2.setCellValue(five.size());
			
			Row row6 = newSheet.createRow(6);
			Cell CPPM1 = row6.createCell(0);
			Cell CPPM2 = row6.createCell(1);
			CPPM1.setCellValue("CPPM");
			CPPM2.setCellValue(six.size());

			Row row7 = newSheet.createRow(7);
			Cell AIBI1 = row7.createCell(0);
			Cell AIBI2 = row7.createCell(1);
			AIBI1.setCellValue("AIBI");
			AIBI2.setCellValue(seven.size());
			
			Row row8 = newSheet.createRow(8);
			Cell PROCUREMENT1 = row8.createCell(0);
			Cell PROCUREMENT2 = row8.createCell(1);
			PROCUREMENT1.setCellValue("PROCUREMENT");
			PROCUREMENT2.setCellValue(eight.size());
			
			Row row9 = newSheet.createRow(9);
			Cell DSE1 = row9.createCell(0);
			Cell DSE2 = row9.createCell(1);
			DSE1.setCellValue("DSE-Comerrcial");
			DSE2.setCellValue(nine.size());
			
			Row row10 = newSheet.createRow(10);
			Cell ARCHITECTURE1 = row10.createCell(0);
			Cell ARCHITECTURE2 = row10.createCell(1);
			ARCHITECTURE1.setCellValue("ARCHITECTURE");
			ARCHITECTURE2.setCellValue(ten.size());
			
			Row row11 = newSheet.createRow(11);
			Cell HRCFS1 = row11.createCell(0);
			Cell HRCFS2 = row11.createCell(1);
			HRCFS1.setCellValue("HRCFS");
			HRCFS2.setCellValue(eleven.size());
			
			Row row12 = newSheet.createRow(12);
			Cell GPD1= row12.createCell(0);
			Cell GPD2 = row12.createCell(1);
			GPD1.setCellValue("GPD");
			GPD2.setCellValue(twelve.size());

			Row row13 = newSheet.createRow(13);
			Cell ECS1= row13.createCell(0);
			Cell ECS2= row13.createCell(1);
			ECS1.setCellValue("ECS");
			ECS2.setCellValue(thirteen.size());
			
			System.out.println(one);
			
	}
	
	private ArrayList<ArrayList<String>> get_confluence_users(int startAt, ArrayList<String> one, ArrayList<String> two, ArrayList<ArrayList<String>> Container, String groupName) throws IOException, JSONException {
	
		String confgroupApi = "https://confluence.pfizer.com/rest/api/group/"+groupName+"/member?start="+startAt;
		URL groupUrl = new URL(confgroupApi);
		HttpURLConnection conn = null;
		TrustAllCertificates.install();
	  	conn = (HttpURLConnection) groupUrl.openConnection();
	  	String authString = login + ":" + password;
	  	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		
		conn.setRequestProperty ("Authorization", "Basic " + authStringEnc);        	    	  
		String inputLine = "";
		BufferedReader in = null;
			System.out.println("Authentication process");
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		  	StringBuffer response1 = new StringBuffer();
		  while((inputLine = in.readLine()) != null)
		  {
			  response1.append(inputLine);
		  }
		in.close();
		
		
		 JSONObject json  = new JSONObject(response1.toString());
		 JSONArray result = json.getJSONArray("results");
	     
	     for(int i=0; i<result.length(); i++)
	     {
	    	
	    	 JSONObject result1 = result.getJSONObject(i);
	    	 one.add(result1.getString("username"));
	    	 two.add(result1.getString("displayName"));
	     }
	     
	     Container.add(one);
	     Container.add(two);
		return Container;
	}

	public void writeData(XSSFSheet newSheet, ArrayList<String> one, ArrayList<String> two, String TEXT) throws IOException
	{
		String filename = "Confluence_user_report.xlsx";
  		String outputFile= "/usr/local/Enhanced/reports/"+filename;
  	//	String outputFile= "D:\\Users\\"+filename;
  		
		newSheet = newWorkbook.createSheet(TEXT);
		
		System.out.println("Sheet Created");
		String Columns[] = new String[] {"Username", "Full Name"};     
		Font headerFont = newWorkbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 15);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = newWorkbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
			
		Row headerRow = newSheet.createRow(0);
			
		for(int columnI=0; columnI<Columns.length; columnI++)
		{ 
			Cell cell = headerRow.createCell(columnI);	
			cell.setCellValue(Columns[columnI]);	
			cell.setCellStyle(headerCellStyle); 
			newSheet.autoSizeColumn(columnI);	
		}
		int fork = 1;
		for(int i=0; i<one.size(); i++)
		{
			System.out.println("Running "+i);
			System.out.println(one.get(i));
			Row row = newSheet.createRow(fork);
	        fork++;
	        row.createCell(0).setCellValue(one.get(i));
	        row.createCell(1).setCellValue(two.get(i));
	      
	        FileOutputStream oFile = new FileOutputStream(outputFile); 
     		newWorkbook.write(oFile);
	}

	}

}
