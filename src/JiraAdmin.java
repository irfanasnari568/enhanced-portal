

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//Servlet Mapping
@WebServlet("/JiraAdmin")
public class JiraAdmin extends HttpServlet
{
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		try
		{
			//Connection Created with microsoft sql server
			//String url="jdbc:sqlserver://;serverName=amrvsp000005556;portNumber=2023;databaseName=jiradevdb";
			String url="jdbc:sqlserver://;serverName=AMRDRMW638;portNumber=2023;databaseName=JIRADB_P";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//Connection connection=DriverManager.getConnection(url, "jiradevdb_User", "b1%o2%j1%o2%");
			Connection connection=DriverManager.getConnection(url, "JIRADB_P_User", "2wsxcde@2019");
			
		    Statement stmt = connection.createStatement();
		    Statement statement=connection.createStatement();
		    
		  //Creating Workbook/Excel using Apache POI
	         XSSFWorkbook workbook=new XSSFWorkbook();
	         
	         String filename = "Jira_Admin_Report.xlsx";
	 	     	String outputFile= "/usr/local/Enhanced/reports/"+filename;
	 	 	 //String outputFile= "D:\\Users\\"+filename;
	         
	       //Creating Cell Style applied in both sheets
	         XSSFCellStyle style=workbook.createCellStyle();
	         XSSFFont font=workbook.createFont();
	         font.setBold(true);
	         style.setFont(font);
		    
		      //Executing 1st query (Project_Admin Details)
	         String sql;
	         sql = "select  cast.Project_Name, cast.Project_Key, cast.Project_Type, cast.Project_Lead, cast.Admin_Type, cast.Project_Admin,cast.Full_Name, cast.Full_Name, cast.Email_Address,(CASE cast.Business_L WHEN 'Delegated' THEN 'Inactive' WHEN 'NULL' THEN 'Inactive' ELSE cast.Business_L END) as Business_Line from (select distinct tests.Project_Name,tests.Project_Key,tests.Project_Type,ap.lower_user_name as Project_Lead, tests.Admin_Type, tests.Project_Admin, tests.Full_Name, tests.Email_Address, (CASE tests.Admin_Type WHEN  'GROUP' THEN 'Group' ELSE tests.Business_Line END) As Business_L from(select test.Project_Name, test.Project_Key,test.Project_Type, test.Project_Lead, test.Admin_Type,(CASE test.Admin_Type WHEN 'GROUP' THEN test.Project_Admin ELSE test.username END) as Project_Admin , (CASE test.Admin_Type WHEN 'GROUP' THEN'NULL' ELSE test.Full_Name END) as Full_Name,(CASE test.Admin_Type WHEN 'GROUP' THEN 'NULL' ELSE test.Email_Address END) as Email_Address, test.Business_Line from (select distinct inn.Project_Name,inn.Project_Key,inn.Project_Type,Project_Lead,inn.Admin_Type,inn.Project_Admin,inn.username,inn.Full_Name, inn.Email_Address, parent_name as Businessline, (CASE parent_name WHEN 'jiraprod' THEN 'GIS' WHEN 'jiraprodpgs' THEN 'PGS' WHEN 'jiraprodrnd' THEN 'RND' WHEN 'jiraprodaibi' THEN 'AIBI' WHEN 'jiraprodfbo' THEN 'FBO' WHEN 'jiraproddse-commercial' THEN 'DSE-Commercial' WHEN 'jiraprodarchitecture' THEN 'Digital Architecture' WHEN 'jiraprodecs' THEN 'ECS' WHEN 'jiraprodhrcfs' THEN 'HRCFS' WHEN 'jiraprodaudit' THEN 'AUDIT' WHEN 'jiraprodgpd' THEN 'GPD' WHEN 'jiraprodprocurement' THEN 'Procurement' WHEN 'jiraprodcppm' THEN 'CPPM' WHEN 'jiraprodrrems' THEN 'RREMS' ELSE CASE inn.dir WHEN 1 THEN 'Internal Directory' WHEN 10000 THEN 'Delegated' ELSE 'NULL' END END) as Business_Line,ROW_NUMBER() over (partition by inn.Testss order by parent_name) inst from(select distinct pname as Project_Name,pkey as Project_Key,LEAD as Project_Lead,PROJECTTYPE as Project_Type,(CASE ROLETYPE WHEN 'atlassian-group-role-actor' THEN 'GROUP' ELSE 'USER' END)as Admin_Type,ROLETYPEPARAMETER as Project_Admin, k.lower_user_name as username ,display_name as Full_Name,email_address as Email_Address,CONCAT(pname, ',' ,ROLETYPEPARAMETER) as Testss, directory_id as dir from dbo.project p JOIN dbo.projectroleactor a ON p.ID=a.PID JOIN dbo.projectrole r ON r.ID=a.PROJECTROLEID LEFT JOIN dbo.app_user k ON k.user_key= a.ROLETYPEPARAMETER LEFT JOIN dbo.cwd_user u ON u.user_name=k.lower_user_name and u.active=1 where  a.ROLETYPEPARAMETER NOT IN ('NULL')and r.ID=10002 ) inn LEFT JOIN cwd_membership m ON m.child_name=inn.username  and m.parent_name IN ('jiraprodrnd', 'jiraprod','jiraprodpgs','jiraprodaibi', 'jiraprodfbo','jiraprodrrems', 'jiraprodarchitecture', 'jiraprodaudit', 'jiraprodcppm', 'jiraproddse-commercial', 'jiraprodecs', 'jiraprodgpd', 'jiraprodhrcfs', 'jiraprodprocurement')) test where test.inst=1 )tests JOIN app_user ap ON ap.user_key=tests.Project_Lead LEFT JOIN cwd_user cu ON cu.user_name=ap.lower_user_name)cast";
	         ResultSet rs = stmt.executeQuery(sql);
	        
	         
	          //Creating Project_Admin sheet (Sheet-1)
	         XSSFSheet sheet= workbook.createSheet("Project_Admin");  
	         
	         //Creating 1st Row 
	         XSSFRow row=sheet.createRow((short)0);
	        
	         //Creating cells in (1st row-Header) and applying style
	         Cell cell0=row.createCell((short)0);
	         cell0.setCellValue("Project_Name");
	         cell0.setCellStyle(style);
	         Cell cell1=row.createCell((short)1);
	         cell1.setCellValue("Project_Key");
	         cell1.setCellStyle(style);
	         Cell cell2=row.createCell((short)2);
	         cell2.setCellValue("Project_Type");
	         cell2.setCellStyle(style);
	         Cell cell3=row.createCell((short)3);
	         cell3.setCellValue("Project_Lead");
	         cell3.setCellStyle(style);
	         Cell cell4=row.createCell((short)4);
	         cell4.setCellValue("Admin_Type");
	         cell4.setCellStyle(style);
	         Cell cell5=row.createCell((short)5);
	         cell5.setCellValue("Project_Admin");
	         cell5.setCellStyle(style);
	         Cell cell6=row.createCell((short)6);
	         cell6.setCellValue("Full_Name");
	         cell6.setCellStyle(style);
	         Cell cell7=row.createCell((short)7);
	         cell7.setCellValue("Email_Address");
	         cell7.setCellStyle(style);
	         Cell cell8=row.createCell((short)8);
	         cell8.setCellValue("Business_Line");
	         cell8.setCellStyle(style);
	         int i=1;
	        
	         while(rs.next()){
	             //Retrieve by column name
	             String Project_Name  = rs.getString("Project_Name");
	             String Project_Key  = rs.getString("Project_key");
	             String Project_Lead = rs.getString("Project_Lead");
	             String Project_Type = rs.getString("Project_Type");
	             String Admin_Type = rs.getString("Admin_Type");
	             String Project_Admin = rs.getString("Project_Admin");
	             String Full_Name=rs.getString("Full_Name");
	             String Email_Address=rs.getString("Email_Address");
	             String Business_Line = rs.getString("Business_Line");
 
	             //Writing rowwise value in Project_Admin sheet
	             XSSFRow row2=sheet.createRow((short)i);
	             row2.createCell((short)0).setCellValue(Project_Name);
	             row2.createCell((short)1).setCellValue(Project_Key);
	             row2.createCell((short)2).setCellValue(Project_Type);
	             row2.createCell((short)3).setCellValue(Project_Lead);
	             row2.createCell((short)4).setCellValue(Admin_Type);
	             row2.createCell((short)5).setCellValue(Project_Admin);
	             row2.createCell((short)6).setCellValue(Full_Name);
	             row2.createCell((short)7).setCellValue(Email_Address);
	             row2.createCell((short)8).setCellValue(Business_Line);
	             i++;
	           
	          }
	         
	         //Configure column size in sheet-1
	         int CoumnNo=sheet.getRow(0).getLastCellNum();
	         for(int a=0;a<=CoumnNo;a++)
	         {
	         sheet.autoSizeColumn(a);
	         }
	         
	         //Executing 2nd query (Group_Members Details)
	         String sql2="select * from(select test.Group_Name, test.NTID,test.Full_Name, test.Email_Address, (CASE test.Business_Line WHEN 'Delegated' THEN 'Inactive' ELSE test.Business_Line END) as Business_Line from (select distinct inn.Group_Name,inn.NTID,inn.Full_Name,inn.Email_Address, parent_name as Businessline, (CASE parent_name WHEN 'jiraprod' THEN 'GIS' WHEN 'jiraprodpgs' THEN 'PGS' WHEN 'jiraprodrnd' THEN 'RND' WHEN 'jiraprodaibi' THEN 'AIBI' WHEN 'jiraprodfbo' THEN 'FBO' WHEN 'jiraproddse-commercial' THEN 'DSE-Commercial' WHEN 'jiraprodarchitecture' THEN 'Digital Architecture' WHEN 'jiraprodecs' THEN 'ECS' WHEN 'jiraprodhrcfs' THEN 'HRCFS' WHEN 'jiraprodaudit' THEN 'AUDIT' WHEN 'jiraprodgpd' THEN 'GPD' WHEN 'jiraprodprocurement' THEN 'Procurement' WHEN 'jiraprodcppm' THEN 'CPPM' WHEN 'jiraprodrrems' THEN 'RREMS'  ELSE CASE inn.dir WHEN 1 THEN 'Internal Directory' WHEN 10000 THEN 'Delegated' ELSE 'NULL' END END) as Business_Line, ROW_NUMBER() over (partition by inn.Testss order by parent_name) inst from(select distinct group_name as Group_Name, child_name as NTID, display_name as Full_Name, email_address as Email_Address ,CONCAT(group_name, ',' ,child_name) as Testss, u.directory_id as dir from dbo.cwd_group g JOIN dbo.projectroleactor a ON g.group_name=a.ROLETYPEPARAMETER and a.PROJECTROLEID=10002 JOIN dbo.cwd_membership m ON m.parent_id=g.ID JOIN dbo.cwd_user u ON u.ID=m.child_id where a.ROLETYPEPARAMETER NOT IN ('jira-software-users','PGS-jira-software-user') and g.active=1) inn LEFT JOIN cwd_membership m ON m.child_name=inn.NTID  and m.parent_name IN ('jiraprodrnd', 'jiraprod','jiraprodpgs','jiraprodaibi', 'jiraprodfbo','jiraprodrrems', 'jiraprodarchitecture', 'jiraprodaudit', 'jiraprodcppm','jiraproddse-commercial', 'jiraprodecs', 'jiraprodgpd', 'jiraprodhrcfs', 'jiraprodprocurement')) test where test.inst=1)tests where tests.Business_Line!='NULL'";
	         ResultSet rs2=statement.executeQuery(sql2);
	         
	         //Creating Group_Details Sheet (Sheet-2)
	         XSSFSheet sheet2= workbook.createSheet("Group_Details");
	         
	         //Creating 1st Row
	         XSSFRow rows=sheet2.createRow((short)0);
	         
	       //Crseating cells (1st row/Header) and applying style
	         Cell cells0=rows.createCell((short)0);
	         cells0.setCellValue("Group_Name");
	         cells0.setCellStyle(style);
	         Cell cells1=rows.createCell((short)1);
	         cells1.setCellValue("NTID");
	         cells1.setCellStyle(style);
	         Cell cells2=rows.createCell((short)2);
	         cells2.setCellValue("Full_Name");
	         cells2.setCellStyle(style);
	         Cell cells3=rows.createCell((short)3);
	         cells3.setCellValue("Email_Address");
	         cells3.setCellStyle(style);
	         Cell cells4=rows.createCell((short)4);
	         cells4.setCellValue("Business_Line");
	         cells4.setCellStyle(style);
	         int j=1;
	         while(rs2.next())
	         {
	        	 //Retrieving values by column_name
	        	 String Group_Name  = rs2.getString("Group_Name");
	             String NTID  = rs2.getString("NTID");
	             String Full_Name = rs2.getString("Full_Name");
	             String Email_Address=rs2.getString("Email_Address");
	             String Business_Line=rs2.getString("Business_Line");
	             
	             //Writing rowwise value in Group_Details sheet
	             XSSFRow rows2=sheet2.createRow((short)j);
	             rows2.createCell((short)0).setCellValue(Group_Name);
	             rows2.createCell((short)1).setCellValue(NTID);
	             rows2.createCell((short)2).setCellValue(Full_Name);
	             rows2.createCell((short)3).setCellValue(Email_Address);
	             rows2.createCell((short)4).setCellValue(Business_Line);
	             j++;
	         }
	         
	       //Configure column size in sheet-2
	         int CoumnNos=sheet2.getRow(0).getLastCellNum();
	         for(int b=0;b<=CoumnNos;b++)
	         {
	         sheet2.autoSizeColumn(b);
	         }
	         
//	        // Downloading excel-sheet in xlsx format
	         
	         
		     	FileOutputStream oFile = new FileOutputStream(outputFile); 
	     		workbook.write(oFile);
	     		
	     		File file = new File(outputFile);

	    		if(file.exists())
	    		{
	    			//			Desktop desktop = Desktop.getDesktop();
	    			//			desktop.open(file);
	    			//			InputStream in = new URL(f).openStream();
	    			//			Files.copy(in, Paths.get(f));


	    			response.setContentType("application/vnd.ms-excel");   
	    			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
	    			System.out.println("filename new  "+filename);
	    			
	    			BufferedInputStream in=null;
	    			ServletOutputStream outs=null;
	    			byte[] buffer = new byte[1024]; 
	    			
	    			int g = 0;
    				in = new BufferedInputStream(new FileInputStream(new File (outputFile)));  
    				outs=response.getOutputStream();
    				while ((g = in.read(buffer, 0, buffer.length)) != -1) {  
    					outs.write(buffer, 0, g);  
    				}  
    				outs.flush();  
    				
    				System.out.println("Done !!");

        			outs.close();
        			in.close();
        			
        		}
        		else
        		{
        			System.out.println("not found");
        		}
//	         response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//	         response.setHeader("Content-Disposition", "attachment;filename= JiraAdmin_Report.xlsx");
//	         ServletOutputStream outStream = response.getOutputStream();
	         
	      // Write workbook to response.
//	         workbook.write(outStream); 
//	         outStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	

}
